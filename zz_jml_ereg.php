<?php
	include("zz_koneksi_db.php");
	$ostr = "";
	$thn = date("Y");
	$bln = date("m");
	settype($bln, "int");
	
	function get_bln($a) {
		if( $a== 1 ) { return "Jan"; }
		if( $a== 2 ) { return "Feb"; }
		if( $a== 3 ) { return "Mar"; }
		if( $a== 4 ) { return "Apr"; }
		if( $a== 5 ) { return "Mei"; }
		if( $a== 6 ) { return "Jun"; }
		if( $a== 7 ) { return "Jul"; }
		if( $a== 8 ) { return "Agt"; }
		if( $a== 9 ) { return "Sep"; }
		if( $a== 10 ) { return "Okt"; }
		if( $a== 11 ) { return "Nov"; }
		if( $a== 12 ) { return "Des"; }
	}

	
	for( $iiy = 1; $iiy <= $bln; $iiy++ ) {
		$hsl1 = mysqli_query($conn, "select count(id) from tbl_reg_ranperda where sproses='3' and month(tgl_skreg)='$iiy' and year(tgl_skreg)='$thn'");
		if( mysqli_num_rows($hsl1) == 0 ) {
			$jml = 0;
		} else {
			$B1 = mysqli_fetch_array($hsl1);
			if( $B1[0] == "" || !isset($B1[0]) ) { $jml = 0; } else { $jml = $B1[0]; }
		}
		$atext = "Jumlah Register terdata = ".$jml;
		$nbl = get_bln($iiy);
		if( $ostr == "" ) { $dum = ""; } else { $dum = ", "; }
		$dum .= "{\"perda\": ".json_encode($nbl).",\n";
		$dum .= "\"atext\": ".json_encode($atext).",\n";
		$dum .= "\"jml\": ".json_encode($jml)."\n";
		$dum .= "}";
		$ostr .= $dum;
	}
	echo "[".$ostr."]";
?>