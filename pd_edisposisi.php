<?php include_once("zz_koneksi_db.php"); ?>
<?php include ("zz_generate_menu.php"); ?>
<?php
	session_start();
	require('calendar/tc_calendar.php');
	$host  = $_SERVER['HTTP_HOST'];
	$uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
	$ip1 = $_SERVER['HTTP_X_FORWARDED_FOR'];
	$ip2 = $_SERVER['REMOTE_ADDR'];
	$waktu = date("Y-m-d H:i:s");
	$extra = "pd_login.php";
	if( $_SESSION["sws_id"] == "" || !isset($_SESSION["sws_id"]) || $_SESSION["sws_id"] == 0 ) {
		echo "<script>window.location.href=\"pd_login.php\";</script>";
		//header("Location: http://$host$uri/$extra");
		exit;
	}
	if( $_SESSION["sws_tingkat"] == 99 || $_SESSION["sws_tingkat"] == 1 || $_SESSION["sws_tingkat"] == 2 ) {
	} else {
		echo "<script>window.location.href=\"pd_login.php\";</script>";
		//header("Location: http://$host$uri/$extra");
		exit;
	}
	$anid = $_POST["id"];
	$hsl = mysqli_query($conn, "select * from tbl_reg_ranperda where id='$anid'");
	if( mysqli_num_rows($hsl) == 0 ) {
		echo "<script>window.location.href=\"pd_login.php\";</script>";
		//header("Location: http://$host$uri/$extra");
		exit;
	}
	$B = mysqli_fetch_array($hsl);
	$token = $B["utoken"];
	$judul = $B["judul"];
	$ringkasan = str_replace (array("\r\n", "\n", "\r"), '<br>', $B["ringkasan"]);
	$wkirim = get_waktu($B["wkirim"]);
	$hsl1 = mysqli_query($conn, "select nama from tbl_prov where kode='".$B["prov"]."'");
	$B1 = mysqli_fetch_array($hsl1);
	$wilayah = $B1[0];
	if( $B["kabkota"] != 0 ) {
		$hsl1 = mysqli_query($conn, "select nama from tbl_kab where kode_kab='".$B["kabkota"]."'");
		$B1 = mysqli_fetch_array($hsl1);
		$wilayah .= " / ".$B1[0];
	}
	if( $B["nfile_asli"] == "" || !isset($B["nfile_asli"]) ) { $ada1 = 0; } else { $ada1 = 1; $nf1 = $B["nfile_asli"]; }
	if( $B["kepdprd_asli"] == "" || !isset($B["kepdprd_asli"]) ) { $ada2 = 0; } else { $ada2 = 1; $nf2 = $B["kepdprd_asli"]; }
	if( $B["sp_asli"] == "" || !isset($B["sp_asli"]) ) { $ada3 = 0; } else { $ada3 = 1; $nf3 = $B["sp_asli"]; }
	if( $B["kepmen_asli"] == "" || !isset($B["kepmen_asli"]) ) { $ada4 = 0; } else { $ada4 = 1; $nf4 = $B["kepmen_asli"]; }
	
	if( $ada1 == 1 ) {
		$branperda = "<td colspan=\"2\" onClick=\"unduh(1,'$anid', '1');\" onMouseOver=\"this.style.cursor='pointer';\"><u>".$nf1."</u></td>";
	} else {
		$branperda = "<td colspan=\"2\">Berkas tidak ada</td>";
	}
	if( $ada2 == 1 ) {
		$bdprd = "<td colspan=\"2\" onClick=\"unduh(2,'$anid', '1');\" onMouseOver=\"this.style.cursor='pointer';\"><u>".$nf2."</u></td>";
	} else {
		$bdprd = "<td colspan=\"2\">Berkas belum ada</td>";
	}
	if( $ada3 == 1 ) {
		$bsp = "<td colspan=\"2\" onClick=\"unduh(3,'$anid', '1');\" onMouseOver=\"this.style.cursor='pointer';\"><u>".$nf3."</u></td>";
	} else {
		$bsp = "<td colspan=\"2\">Berkas belum ada</td>";
	}
	if( $ada4 == 1 ) {
		$bkm = "<td colspan=\"2\" onClick=\"unduh(4,'$anid', '1');\" onMouseOver=\"this.style.cursor='pointer';\"><u>".$nf4."</u></td>";
	} else {
		$bkm = "<td colspan=\"2\">Berkas tidak ada</td>";
	}
	
	if( $B["jenis"] == 1 ) { $jeval = "Evaluasi"; } else { $jeval = "Non evaluasi"; }
	
	
	
	function get_waktu($a) {
		$dum = explode(" ",$a);
		$dum1 = explode("-", $dum[0]);
		$ostr = $dum1[2];
		if( $dum1[1] == "01" ) { $ostr .= " Januari ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "02" ) { $ostr .= " Februari ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "03" ) { $ostr .= " Maret ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "04" ) { $ostr .= " April ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "05" ) { $ostr .= " Mei ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "06" ) { $ostr .= " Juni ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "07" ) { $ostr .= " Juli ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "08" ) { $ostr .= " Agustus ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "09" ) { $ostr .= " September ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "10" ) { $ostr .= " Oktober ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "11" ) { $ostr .= " November ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "12" ) { $ostr .= " Desember ".$dum1[0]." ".$dum[1]; }
		
		$tnow = time();
		$tunduh = strtotime($a);
		$time = $tnow - $tunduh;
		$time = ($time < 1) ? 1 : $time;
		$tokens = array(
			31536000 => 'tahun',
			2592000  => 'bulan',
			604800   => 'minggu',
			86400    => 'hari',
			3600     => 'jam',
			60		 => 'menit',
			1		 => 'detik');
			
		foreach($tokens as $unit => $text ) {
			if( $time < $unit ) continue;
			$nou = floor($time / $unit);
			$et = $nou." ".$text;
			break;
		}
		return $ostr." (".$et." yang lalu)";
	}
?>


<!DOCTYPE HTML>
<html>

<head>
  <title>.:: ePERDA - Kementerian Dalam Negeri ::.</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>

<script language="javascript">
function unduh(b, a, c) {
	document.form_unduh.id.value = a;
	document.form_unduh.jenis.value = b;
	document.form_unduh.tabel.value = c;
	document.form_unduh.submit();
}


function checkit() {
	var ans; 
	ans=window.confirm('Yakin akan memasukkan tanggal disposisi?');
	return ans;
}

</script>  
  
</head>

<body>
  <div id="main">
    <header>
      <?php generate_logo(); ?>
      <?php generate_menu(5); ?>
    </header>
    <div id="site_content">
      <div class="gallery"><?php generate_gallery($conn, 950, 150); ?></div>
      <div class="content">
        <h1>Nomor Register Rancangan PERDA</h1>
        <table width="100%" border="0" cellspacing="0" cellpadding="3" style="border-top:2px solid #000000; font-size:80%" >
          <tr>
            <td colspan="3"><h2><?php echo $judul; ?></h2></td>
          </tr>
          <tr>
            <td>Provinsi / KabKota</td>
            <td width="76%" colspan="2"><?php echo $wilayah; ?></td>
          </tr>
          <tr valign="top">
            <td>Jenis Ranperda</td>
            <td colspan="2"><?php echo $jeval; ?></td>
          </tr>
          <tr valign="top">
            <td>Klasifikasi</td>
            <td colspan="2"><?php echo $B["klasifikasi"]; ?></td>
          </tr>
          <tr valign="top">
            <td width="24%">Ringkasan</td>
            <td colspan="2"><?php echo $ringkasan; ?></td>
          </tr>
          <tr>
            <td>Berkas Ranperda</td>
            <?php echo $branperda; ?>
          </tr>
          <tr>
            <td>Berkas Keputusan DPRD</td>
            <?php echo $bdprd; ?>
          </tr>
          <tr>
            <td>Berkas Surat Permohonan</td>
            <?php echo $bsp; ?>
          </tr>
        </table>
        <form action="pd_edisposisi_confirm.php" method="post" enctype="multipart/form-data" name="form1" onSubmit="return checkit();">
          <table width="100%" border="0" cellspacing="0" cellpadding="3" style="border-top:2px solid #000000; font-size:80%">
            <tr valign="top">
              <td width="20%">Tgl Disposisi</td>
              <td width="4%">&nbsp;</td>
              <td width="76%">
<?php
		$dnow = date("Y-m-d");
		$tl = explode("-",$dnow);
	  $myCalendar = new tc_calendar("tgl_disposisi", true, false);
	  $myCalendar->setPicture("calendar/images/iconCalendar.gif");
	  $myCalendar->setDate($tl[2],$tl[1],$tl[0]);
	  $myCalendar->setPath("calendar/");
	  $myCalendar->setYearSelect(1930, date('Y'));
	  $myCalendar->dateAllow('1930-01-01', date('Y-m-d',mktime(0, 0, 0, 31, 12, date("Y"))));
	  // VERY IMPORTANT: next line REQUIRED only for localized version!
	  $myCalendar->setDateFormat(str_replace("%","",str_replace("B","F",str_replace("d","j",L_CAL_FORMAT))));
	  $myCalendar->setAlignment('left', 'bottom');
	  //$myCalendar->autoSubmit(true, "form1");
	  $myCalendar->writeScript();        
?>              
              <input name="id" type="hidden" id="id" value="<?php echo $anid; ?>"></td>
            </tr>
            <tr>
              <td colspan="3">&nbsp;</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td><input type="submit" name="button" id="button" value="Proses"></td>
            </tr>
          </table>
        </form>
        <br>
        <p>&nbsp;</p>
      </div>
    </div>
    <?php generate_footer(); ?>
  </div>
  <p>&nbsp;</p>
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript" src="js/image_fade.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
    });
  </script>
  
<form action="pd_eunduh_01.php" method="post" name="form_unduh">
<input name="id" type="hidden" value="" />
<input name="jenis" type="hidden" value="" />
<input name="tabel" type="hidden" value="" />
</form>
<form action="pd_ehistory.php" method="post" name="form_hist">
<input name="id" type="hidden" value="" />
<input name="token" type="hidden" value="" />
</form>
  
</body>
</html>
