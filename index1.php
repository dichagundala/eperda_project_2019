<?php include_once("zz_koneksi_db.php"); ?>
<?php include ("zz_generate_menu.php"); ?>
<!DOCTYPE HTML>
<html>

<head>
  <title>.:: ePERDA - Kementerian Dalam Negeri ::.</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="box/RoundedCorners.css" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>
  <script type="text/javascript" src="js/laststatus.js"></script>
  <script type="text/javascript" src="http://www.amcharts.com/lib/3/amcharts.js"></script>
<script type="text/javascript" src="http://www.amcharts.com/lib/3/serial.js"></script>
<script type="text/javascript" src="http://www.amcharts.com/lib/3/themes/none.js"></script>
<script src="http://www.amcharts.com/lib/3/plugins/dataloader/dataloader.min.js"></script>
  <script>
  	update_status_1();
	update_status_2();
	update_status_3();
	update_status_4();
	check_online();
	
	var myVar1 = setInterval(update_status_1, 300000);
	var myVar2 = setInterval(update_status_2, 300000);
	var myVar3 = setInterval(update_status_3, 300000);
	var myVar4 = setInterval(update_status_4, 300000);
	var myVar5 = setInterval(check_online, 20000);
	
  </script>
  <style type="text/css">
        .boxcontenttext { margin-top:0px; padding: 2px; font-size: 20pt; font-weight: bold; color: #C71585; text-align: left;}      
        #divDialog {font-size: 10pt; }
        #divDialog label {font-weight: bold; display: block; margin-top: 10px; }
        #divDialog input[type='text'] { display: block; width: 400px; margin-bottom: 5px;}         
  </style>
  
</head>

<body>
  <div id="main">
    <header>
      <?php generate_logo(); ?>
      <?php generate_menu(1); ?>
    </header>
    <div id="site_content">
      <div class="gallery"><?php generate_gallery($conn, 950, 150); ?></div>
      <div id="sidebar_container">
<?php
	$hsl = mysqli_query($conn, "select * from berita ORDER BY id DESC LIMIT 1");
	if( mysqli_num_rows($hsl) != 0 ) {
		$B = mysqli_fetch_array($hsl);
      	generate_berita($B);
	}
?>
	<div class="sidebar" align="center"><table width="100%" border="0" cellspacing="0" cellpadding="3">
  <tr>
    <td><a href="live_chat.php"><img src="images/livechat.png" width="180" height="130"></a></td>
  </tr>
  <tr>
    <td id="jml_online"></td>
  </tr>
</table>
</div>
    <div class="sidebar" id="laststatus"></div>
<?php	
	generate_tautan($conn); 
?>
      </div>
      <div class="content">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="25%"><div class="roundbox boxshadow" style="width: 161px; border: solid 2px 	#C71585">              
            <div class="gridheaderleft roundbox-top">Wilayah I</div>
            <div class="boxcontenttext roundbox-bottom" style="background: khaki;" id="laststatus1"></div>
        </div></td>
    <td width="25%"><div class="roundbox boxshadow" style="width: 161px; border: solid 2px #C71585">              
            <div class="gridheaderleft roundbox-top">Wilayah II</div>
            <div class="boxcontenttext roundbox-bottom" style="background: khaki;" id="laststatus2"></div>
        </div></td>
    <td width="25%"><div class="roundbox boxshadow" style="width: 161px; border: solid 2px #C71585">              
            <div class="gridheaderleft roundbox-top">Wilayah III</div>
            <div class="boxcontenttext roundbox-bottom" style="background: khaki;" id="laststatus3"></div>
        </div></td>
    <td width="25%"><div class="roundbox boxshadow" style="width: 161px; border: solid 2px #C71585">              
            <div class="gridheaderleft roundbox-top">Wilayah IV</div>
            <div class="boxcontenttext roundbox-bottom" style="background: khaki;" id="laststatus4"></div>
        </div></td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="3">
  <tr>
    <td><div id="chart1div" style="width: 100%; height: 200px;"></div></td>
  </tr>
</table>

      <hr>
        <h1>Selamat Datang</h1>
        <p><br>
          <span style="text-align: justify"><strong>Aplikasi Online Register dan Fasilitasi Rancangan Peraturan Daerah (ePERDA)</strong> adalah aplikasi yang dikembangkan oleh Direktorat Produk Hukum Daerah - Direktorat Jenderal Otonomi Daerah - Kementerian Dalam Negeri Republik Indonesia yang bertujuan untuk mempercepat proses register dan fasilitasi dari Rancangan Peraturan Daerah.<br>
          <br>
Alasan dan latar belakang dibangunnya aplikasi ePERDA ini melihat bahwa sampai saat ini pencantuman dan pendefinisian Nomor Register Peraturan Daerah masih bersifat manual yang mengakibatkan kurang optimalnya tujuan pencapaian tertib administrasi.<br><br>

Di sisi lain, selama sistem masih berjalan secara manual, Kementerian Dalam Negeri selalu menghadapi kendala dalam melakukan fasilitasi terhadap suatu Rancangan Peraturan Daerah, dimana dalam tahap fasilitasi tersebut Kementerian Dalam Negeri dapat menjaga agar Rancangan Peraturan Daerah yang diajukan dapat <em>in-line</em> dengan peraturan perundang-undangan yang lebih tinggi dan juga tanpa berbenturan dengan norma dan kepentingan umum dengan tujuan akhir bahwa Rancangan Peraturan Daerah tersebut dapat diimplementasikan untuk mencapai kesejahteraan umum.<br><br>

Akhir kata, semoga aplikasi ePERDA dapat memberikan kontribusi yang signifikan untuk peningkatan kinerja dan aparatur Pemerintah, khususnya di lingkungan Direktorat Produk Hukum Daerah - Direktorat Jenderal Otonomi Daerah - Kementerian Dalam Negeri Republik Indonesia, dan juga dapat meningkatkan akuntabilitas kinerja.<br><br>
</span></p>
        <table width="100%" border="0" cellspacing="0" cellpadding="3">
          <tr>
            <td width="50%">&nbsp;</td>
            <td width="50%"><span style="text-align: justify">Jakarta, Mei 2016<br>
            <br>
Direktorat Jenderal Otonomi Daerah<br>
Kementerian Dalam Negeri Republik Indonesia<br>
<br>
<br>
            </span>DR. Sumarsono, MDM</td>
          </tr>
        </table>
        <p><span style="text-align: justify"><br>
  <br>
        </span><br>
  <br>
          <br>
          <br>
          <br>
        </p>
      </div>
    </div>
    <?php generate_footer(); ?>
  </div>
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript" src="js/image_fade.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
    });
	function update_status_online() {
		$.get("zz_update_chat.php", function(data) {
		});
	}
	update_status_online();
	var myVar6 = setInterval(update_status_online, 20000);
	
	
	AmCharts.loadJSON = function(url) {
	if (window.XMLHttpRequest) {
    // IE7+, Firefox, Chrome, Opera, Safari
    	var request = new XMLHttpRequest();
  	} else {
    // code for IE6, IE5
    	var request = new ActiveXObject('Microsoft.XMLHTTP');
  }
  request.open('GET', url, false);
  request.send();
  // parse adn return the output
  return eval(request.responseText);
  
};

var chartData1 = AmCharts.loadJSON('zz_jml_perda.php');
var chart1 = AmCharts.makeChart("chart1div", {
	"type": "serial",
	"titles": [{
		"text" : "Jumlah PERDA yang terdata pada ePERDA",
		"size": 10
	}],
	"dataProvider": chartData1,
    "pathToImages": "http://cdn.amcharts.com/lib/3/images/",
    "categoryField": "prov",
	"categoryAxis": { 
		"gridPosition": "start",
		"fontSize": 8,
		"labelRotation": 45 },
	"valueAxis": {
		"labelsEnabled": true
	},
	"chartCursor": {},
	"trendLines": [],
    "graphs": [ {
      "valueField": "jml",
	  "fillAlphas": 0.8,
	  "fontSize": 8,
	  "labelText": "[[jml]]",
	  "fillColor": "#8d0404",
	  "lineColor": "#8d0404",
	  "type": "column",
      "lineThickness ": 4
    }]
  } );

  </script>
</body>
</html>
