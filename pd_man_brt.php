<?php include_once("zz_koneksi_db.php"); ?>
<?php include ("zz_generate_menu.php"); ?>
<?php
	$host  = $_SERVER['HTTP_HOST'];
	$uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
	$ip1 = $_SERVER['HTTP_X_FORWARDED_FOR'];
	$ip2 = $_SERVER['REMOTE_ADDR'];
	$waktu = date("Y-m-d H:i:s");
	if( $_SESSION["sws_id"] == "" || !isset($_SESSION["sws_id"]) || $_SESSION["sws_id"] == 0 ) {
		$extra = "pd_login.php";
		echo "<script>window.location.href=\"pd_login.php\";</script>";
		//header("Location: http://$host$uri/$extra");
		exit;
	}
	if( $_SESSION["sws_tingkat"] != 99 ) {
		$extra = "pd_login.php";
		echo "<script>window.location.href=\"pd_login.php\";</script>";
		//header("Location: http://$host$uri/$extra");
		exit;
	}
	
	$targetpage = "pd_man_brt.php"; 	
	$limit = 5;
	$stages = 3;
	$jenis = 0;
	if( $_GET["page"] == "" || !isset($_GET["page"]) ) {
		//dari combo box
		if( $_POST["jenis"] == "" || !isset($_POST["jenis"]) ) {
			//awal dipanggil
			$jenis = 0;
			$page = 1;
		} else {
			$jenis = $_POST["jenis"];
			$page = 1;
		}
	} else {
		$jenis = $_GET["jenis"];
		$page = $_GET['page'];
	}
	if( $jenis == 0 ) { $tamb = ""; }
	if( $jenis == 1 ) { $tamb = " WHERE ok=1 and hapus=0"; }
	if( $jenis == 2 ) { $tamb = " WHERE ok=0 and hapus=0"; }
	if( $jenis == 3 ) { $tamb = " WHERE hapus=1"; }
	$query = "SELECT COUNT(id) FROM berita $tamb";
	$B = mysqli_fetch_array(mysqli_query($conn, $query));
	$total_pages = $B[0];
	
	if($page) { $start = ($page - 1) * $limit;  } else { $start = 0; }
	$hsl = mysqli_query($conn, "select * from berita $tamb ORDER BY id DESC LIMIT $start, $limit");
	$query1 = "SELECT * FROM berita $tamb ORDER BY id DESC LIMIT $start, $limit";
	$result = mysqli_query($conn, $query1);
	
	if ($page == 0){$page = 1;}
	$prev = $page - 1;	
	$next = $page + 1;							
	$lastpage = ceil($total_pages/$limit);		
	$LastPagem1 = $lastpage - 1;
	$paginate = '';
	
	if($lastpage > 1) {	
		$paginate .= "<div class='paginate'>";
		if ($page > 1){
			$paginate.= "&nbsp;&nbsp;<a href='$targetpage?page=$prev'>Halaman Sebelumnya</a>&nbsp;&nbsp;";
		} else {
			$paginate.= "&nbsp;&nbsp;<span class='disabled'>Halaman Sebelumnya&nbsp;&nbsp;</span>";	
		}
		if ($lastpage < 7 + ($stages * 2)) {	
			for ($counter = 1; $counter <= $lastpage; $counter++) {
				if ($counter == $page){
					$paginate.= "<span class='current'>&nbsp;&nbsp;$counter&nbsp;&nbsp;</span>";
				}else{
					$paginate.= "&nbsp;&nbsp;<a href='$targetpage?page=$counter'>$counter</a>&nbsp;&nbsp;";
				}					
			}
		} elseif($lastpage > 5 + ($stages * 2))	{
			if($page < 1 + ($stages * 2)) {
				for ($counter = 1; $counter < 4 + ($stages * 2); $counter++) {
					if ($counter == $page) {
						$paginate.= "<span class='current'>&nbsp;&nbsp;$counter&nbsp;&nbsp;</span>";
					}else{
						$paginate.= "&nbsp;&nbsp;<a href='$targetpage?page=$counter'>$counter</a>&nbsp;&nbsp;";
					}					
				}
				$paginate.= "...";
				$paginate.= "&nbsp;&nbsp;<a href='$targetpage?page=$LastPagem1'>$LastPagem1</a>&nbsp;&nbsp;";
				$paginate.= "&nbsp;&nbsp;<a href='$targetpage?page=$lastpage'>$lastpage</a>&nbsp;&nbsp;";		
			} elseif($lastpage - ($stages * 2) > $page && $page > ($stages * 2)) {
				$paginate.= "&nbsp;&nbsp;<a href='$targetpage?page=1'>1</a>&nbsp;&nbsp;";
				$paginate.= "&nbsp;&nbsp;<a href='$targetpage?page=2'>2</a>&nbsp;&nbsp;";
				$paginate.= "...";
				for ($counter = $page - $stages; $counter <= $page + $stages; $counter++) {
					if ($counter == $page){
						$paginate.= "<span class='current'>&nbsp;&nbsp;$counter&nbsp;&nbsp;</span>";
					}else{
						$paginate.= "&nbsp;&nbsp;<a href='$targetpage?page=$counter'>$counter</a>&nbsp;&nbsp;";
					}					
				}
				$paginate.= "...";
				$paginate.= "&nbsp;&nbsp;<a href='$targetpage?page=$LastPagem1'>$LastPagem1</a>&nbsp;&nbsp;";
				$paginate.= "&nbsp;&nbsp;<a href='$targetpage?page=$lastpage'>$lastpage</a>&nbsp;&nbsp;";		
			} else {
				$paginate.= "&nbsp;&nbsp;<a href='$targetpage?page=1'>1</a>&nbsp;&nbsp;";
				$paginate.= "&nbsp;&nbsp;<a href='$targetpage?page=2'>2</a>&nbsp;&nbsp;";
				$paginate.= "...";
				for ($counter = $lastpage - (2 + ($stages * 2)); $counter <= $lastpage; $counter++) {
					if ($counter == $page){
						$paginate.= "<span class='current'>&nbsp;&nbsp;$counter&nbsp;&nbsp;</span>";
					}else{
						$paginate.= "&nbsp;&nbsp;<a href='$targetpage?page=$counter'>$counter</a>&nbsp;&nbsp;";
					}					
				}
			}
		}
					
		if ($page < $counter - 1){ 
			$paginate.= "&nbsp;&nbsp;<a href='$targetpage?page=$next'>Halaman Berikutnya</a>&nbsp;&nbsp;";
		}else{
			$paginate.= "<span class='disabled'>&nbsp;&nbsp;Halaman Berikutnya&nbsp;&nbsp;</span>";
		}
			
		$paginate.= "</div>";		
	}	
?>

<!DOCTYPE HTML>
<html>

<head>
  <title>.:: ePERDA - Kementerian Dalam Negeri ::.</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>
  
<script language="javascript">

function go_action(a, b) {
	document.form_action.id.value = a;
	document.form_action.aksi.value = b;
	document.form_action.submit();
}

function go_preview(a) {
	document.form_preview.id.value = a;
	document.form_preview.submit();
}

function go_ganti() {
	document.form_nav.jenis.value = document.form1.jenis.value;
	document.form_nav.submit();
}
</script>  
</head>

<body>
  <div id="main">
    <header>
      <?php generate_logo(); ?>
      <?php generate_menu(1); ?>
    </header>
    <div id="site_content">
      <div class="gallery"><?php generate_gallery($conn, 950, 150); ?></div>
      <div class="content_webgis">
        <h1>Pengelolaan Berita</h1>
        <table width="800" height="" border="0" align="center" cellpadding="2" cellspacing="2" class="sws_table">
          <tr>
            <td width="762" align="center" class="style5">&nbsp;</td>
          </tr>
          <tr>
            <td align="center" class="style5"><table width="100%" border="0" cellspacing="0" cellpadding="2">
              <tr>
                <td width="72%">&nbsp;&nbsp;Ditemukan
<?php 
	echo $total_pages.' Berita';
 	echo $paginate;
?></td>
                <td width="28%" align="right"><form id="form1" name="form1" method="post" action="">
                  <select name="jenis" class="style5" id="jenis" onchange="go_ganti();">
                    <option value="0" <?php if( $jenis == 0 ) { ?> selected <?php } ?>>Seluruh Berita</option>
                    <option value="1" <?php if( $jenis == 1 ) { ?> selected <?php } ?>>Berita yang dipublikasi</option>
                    <option value="3" <?php if( $jenis == 3 ) { ?> selected <?php } ?>>Berita yang disembunyikan</option>
                  </select>
                </form></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td align="left" class="style5">&nbsp;</td>
          </tr>
          <?php
	while( $B = mysqli_fetch_array($hsl) ) { 
		//echo "ID --> ".$B[0]."<br>";
		$id = $B[0];
		$bok = $B["ok"];
		$bhapus = $B["hapus"];
		if( $bok == 1 ) {
			if( $bhapus == 0 ) {
				//bolehnya cuma hapus
				$atd = "<td width=\"50%\" align=\"center\" onclick=\"go_action('$id',3);\">Sembunyikan</td>";
			} else {
				//bolehnya cuma di-undelete
				$atd = "<td width=\"50%\" align=\"center\" onclick=\"go_action('$id',2);\">Publikasi lagi</td>";
			}
		}
		if( $bok == 0 ) {
			// bolehnya cuma disetujui
			$atd = "<td width=\"50%\" align=\"center\" onclick=\"go_action('$id',1);\">Setujui</td>";
		}
		
?>
          <tr>
            <td align="left" class="style5"><h3><span><?php echo $B["judul"]; ?></span></h3>
              <p><?php echo sws_potong($B["isi"],150); ?></p></td>
          </tr>
          <tr>
            <td align="left" class="style5"><table width="100%" border="0" cellspacing="0" cellpadding="2">
              <tr>
                <td width="59%"  onclick="go_preview('<?php echo $id; ?>');">Dikirim oleh <?php echo $B["oleh"]; ?> pada <?php echo sws_get_tgl($B["tgl"]); ?></td>
                <td width="41%"><table width="100%" border="0" cellspacing="0" cellpadding="2">
                  <tr>
                    <td width="50%" align="center" onclick="go_preview('<?php echo $id; ?>');">Preview</td>
                    <?php echo $atd; ?> </tr>
                </table></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td align="left" class="style5"><hr /></td>
          </tr>
          <?php
	}
?>
        </table>
        <p><br>
        </p>
        <p><br>
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
        </p>      
      
      
        <p><br>
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
        </p>


	 </div><br><br><br><br>
    </div>
    <?php generate_footer(); ?>
  </div>
  <p>&nbsp;</p>
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript" src="js/image_fade.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
    });
  </script>

<form action="pd_man_brt_edit.php" method="post" name="form_action">
<input name="id" type="hidden" value="" />
<input name="aksi" type="hidden" value="" />
</form>
<form action="pd_man_brt_prev.php" method="post" name="form_preview" target="_blank">
<input name="id" type="hidden" value="" />
</form>
<form action="pd_man_brt.php" method="post" name="form_nav">
<input name="jenis" type="hidden" value="" />
</form>    
  
</body>
</html>
