<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<style type="text/css">
.sws_fnt_med {
	font-family: Verdana, Geneva, sans-serif;
	font-size: large;
}
.sws_fnt_biasa {
	font-family: Verdana, Geneva, sans-serif;
}
</style>
</head>

<body>
<p class="sws_fnt_med"><strong>Permohonan Nomor Register PERDA</strong></p>
<p>&nbsp;</p>
<p class="sws_fnt_biasa">Jakarta, sws_tanggal<br />
  Kepada Yth<br />
  Bapak/Ibu <strong>sws_okirim</strong><br />
  Di tempat</p>
<p class="sws_fnt_biasa">Dengan hormat</p>
<p class="sws_fnt_biasa">Bersama ini diberitahukan bahwa pengajuan Rancangan PERDA yang Bapak/Ibu  ajukan pada tanggal sws_tgl tentang <br />
  <br />
  <strong>&quot;sws_judul&quot;</strong><br />
  <br />
  Telah kami proses dan kami  registerkan dengan Nomor Register <strong>sws_oreg</strong> pada tanggal <strong>sws_tgl</strong>. Salinan Surat terlampir.</p>
<p class="sws_fnt_biasa">Demikian disampaikan. Atas perhatian dan kerjasamanya diucapkan terima  kasih.</p>
<p>&nbsp;</p>
<p class="sws_fnt_biasa">Hormat kami</p>
<p>&nbsp;</p>
<p class="sws_fnt_biasa">Pengelola ePERDA Kementerian Dalam Negeri</p>
</body>
</html>