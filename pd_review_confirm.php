<?php include_once("zz_koneksi_db.php"); ?>
<?php
	require 'PHPMailer/PHPMailerAutoload.php';
	session_start();
	$host  = $_SERVER['HTTP_HOST'];
	$uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
	$ip1 = $_SERVER['HTTP_X_FORWARDED_FOR'];
	$ip2 = $_SERVER['REMOTE_ADDR'];
	$waktu = date("Y-m-d H:i:s");
	$extra = "pd_login.php";
	if( $_SESSION["sws_id"] == "" || !isset($_SESSION["sws_id"]) || $_SESSION["sws_id"] == 0 ) {
		header("Location: http://$host$uri/$extra");
		exit;
	}
	if( $_SESSION["sws_tingkat"] == 99 || $_SESSION["sws_tingkat"] == 1 || $_SESSION["sws_tingkat"] == 2 ) {
	} else {
		header("Location: http://$host$uri/$extra");
		exit;
	}
	
	function get_waktu($a) {
		$dum = explode(" ",$a);
		$dum1 = explode("-", $dum[0]);
		$ostr = $dum1[2];
		$dum[1] = "";
		if( $dum1[1] == "01" ) { $ostr .= " Januari ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "02" ) { $ostr .= " Februari ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "03" ) { $ostr .= " Maret ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "04" ) { $ostr .= " April ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "05" ) { $ostr .= " Mei ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "06" ) { $ostr .= " Juni ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "07" ) { $ostr .= " Juli ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "08" ) { $ostr .= " Agustus ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "09" ) { $ostr .= " September ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "10" ) { $ostr .= " Oktober ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "11" ) { $ostr .= " November ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "12" ) { $ostr .= " Desember ".$dum1[0]." ".$dum[1]; }
		
		return $ostr;
	}
	
	function uniqueFilename($strExt) {
		$arrIp = explode('.', $_SERVER['REMOTE_ADDR']);
		list($usec, $sec) = explode(' ', microtime());
		$usec = (integer) ($usec * 65536);
		$sec = ((integer) $sec) & 0xFFFF;
		$strUid = sprintf("%08x-%04x-%04x", ($arrIp[0] << 24) | ($arrIp[1] << 16) | ($arrIp[2] << 8) | $arrIp[3], $sec, $usec);
		// tack on the extension and return the filename
		return $strUid . $strExt;
	}
	
	
	
	$anid = $_POST["id"];
	$token = $_POST["token"];
	$sproses = $_POST["sproses"];
	$wkepmen = $_POST["wkepmen"];
	$lanjut = true;
	if( $sproses == 3 ) {
		//disetujui
		if( $_POST["nkepmen"] == "" || !isset($_POST["nkepmen"]) ) {
			$lanjut = false;
		} else {
			$nk = addslashes($_POST["nkepmen"]);
		}
		if( empty($_FILES['bkepmen']) ) {
			$lanjut = false;
		} else {
			$file_a = $_FILES['bkepmen']['name'];
			$dum = explode(".",$file_a);
			if( count($dum) > 1 ) { $file_db = uniqueFilename(".".$dum[count($dum)-1]); } else { $file_db = uniqueFilename(""); }
			$uploaddir = "upload/".$file_db;
			if(move_uploaded_file($_FILES['bkepmen']['tmp_name'], $uploaddir)) {
			} else {
				$lanjut = false;
			}
		}
	}
	
	if( empty($_FILES['fasdirjen']) ) {
		$adafb1 = 0;
		$fba1 = "";
		$fbdb1 = "";
	} else {
		$adafb1 = 1;
		$fba1 = $_FILES['fasdirjen']['name'];
		$dum = explode(".",$fba1);
		if( count($dum) > 1 ) { $fbdb1 = uniqueFilename(".".$dum[count($dum)-1]); } else { $fbdb1 = uniqueFilename(""); }
		$uploaddir = "upload/".$fbdb1;
		if(move_uploaded_file($_FILES['fasdirjen']['tmp_name'], $uploaddir)) {
		} else {
			$adafb1 = 0;
			$fba1 = "";
			$fbdb1 = "";
		}
	}
	
	if( empty($_FILES['bpend']) ) {
		$adafb2 = 0;
		$fba2 = "";
		$fbdb2 = "";
	} else {
		$adafb2 = 1;
		$fba2 = $_FILES['bpend']['name'];
		$dum = explode(".",$fba2);
		if( count($dum) > 1 ) { $fbdb2 = uniqueFilename(".".$dum[count($dum)-1]); } else { $fbdb2 = uniqueFilename(""); }
		$uploaddir = "upload/".$fbdb2;
		if(move_uploaded_file($_FILES['bpend']['tmp_name'], $uploaddir)) {
		} else {
			$adafb2 = 0;
			$fba2 = "";
			$fbdb2 = "";
		}
	}
	
	$hsl = mysqli_query($conn, "select * from tbl_ranperda where id='$anid'");
	if( mysqli_num_rows($hsl) == 0 ) { $lanjut = false; }
	else {
		$B = mysqli_fetch_array($hsl);
		$email = $B["kirimke"];
		$noreg = $B["cnoreg"];
		$okirim = $B["okirim_nama"];
		$judul = $B["judul"];
		$tgl_kirim = get_waktu($B["wkirim"]);
	}
	
	$rev = addslashes($_POST["elm1"]);
	$waktu = date("Y-m-d H:i:s");
	$revnama = addslashes($_SESSION["sws_nama_pengguna"]);
	$revid = $_SESSION["sws_id"];
	
	$tgl_now = get_waktu($waktu);
	
	if( $lanjut ) {
		if( $sproses == 3 ) {
			//update dulu file file terakhir ke tbl_ranperda
			$hsl = mysqli_query($conn, "update tbl_ranperda SET sproses='$sproses' where id='$anid'");
			$astr = "insert into tbl_ranperda_fix select null, judul, nfile_asli, nfile_db, ringkasan, wkirim, prov, kabkota, okirim_nama, okirim_id, utoken, kirimke, cnoreg, tkt, sproses, ada_kepdprd, kepdprd_asli, kepdprd_db, ada_sp, sp_asli, sp_db, '1' as ada_kepmen, '$nk' as nokepmen, '$file_a' as kepmen_asli, '$file_db' as kepmen_db, '$wkepmen' as wkepmen, '$waktu' as wapprove from tbl_ranperda where id='$anid'";
			//echo $astr;
			$hsl = mysqli_query($conn, $astr);
			
			$fixid = mysqli_insert_id($conn);
			$hsl = mysqli_query($conn, "update tbl_ranperda set acuan='$fixid' where id='$anid'");
			$dum = explode("-", $wkepmen);
			$athn = $dum[0];
			$hsl = mysqli_query($conn, "insert into tbl_produk_hukum select null, nokepmen, '$athn' as thn, judul, ringkasan, wkepmen, 1, nfile_asli, nfile_db, '10' as tingkat, prov, kabkota from tbl_ranperda_fix where id='$fixid'");
		} else {
			$hsl = mysqli_query($conn, "update tbl_ranperda SET sproses='$sproses' where id='$anid'");
		}
		$astr = "insert into tbl_review VALUES (null, '$token', '$rev', '$waktu', '$revid', '$revnama', '$sproses', '$email',0,'','',0,'','',0,'','', '$adafb1', '$fba1', '$fbdb1', '$adafb2', '$fba2', '$fbdb2')";
		//echo $astr;
		$hsl = mysqli_query($conn, $astr);
		
		//kirim email
		$mail = new PHPMailer();
		$mail->isSMTP();
		$mail->SMTPDebug = 0;
		$mail->Debugoutput = 'html';
		$mail->Host = 'mail.eperda-chat.com';
		$mail->Port = 587;
		$mail->SMTPSecure = 'tls';
		$mail->SMTPAuth = true;
		$mail->Username = "admin@eperda-chat.com";
		$mail->Password = "ePERDAindoglobal2017";
		$mail->setFrom('admin@eperda-chat.com', 'Webadmin ePERDA Ditjen Otonomi Daerah');
		$mail->addReplyTo('admin@eperda-chat.com', 'Webadmin ePERDA Ditjen Otonomi Daerah');
		$mail->addAddress($email, $nama);
		$mail->Subject = 'Feedback Pengajuan Rancangan PERDA';
		
		if( $sproses == 2 ) {
		//butuh feedback
			$isian = "<html>    <head>  <meta http-equiv=Content-Type content=\"text/html; charset=windows-1252\">  <meta name=Generator content=\"Microsoft Word 15 (filtered)\">  <style>  <!--   /* Font Definitions */   @font-face  	{font-family:\"Cambria Math\";  	panose-1:2 4 5 3 5 4 6 3 2 4;}  @font-face  	{font-family:Calibri;  	panose-1:2 15 5 2 2 2 4 3 2 4;}   /* Style Definitions */   p.MsoNormal, li.MsoNormal, div.MsoNormal  	{margin-top:0in;  	margin-right:0in;  	margin-bottom:8.0pt;  	margin-left:0in;  	line-height:107%;  	font-size:11.0pt;  	font-family:\"Calibri\",sans-serif;}  .MsoChpDefault  	{font-family:\"Calibri\",sans-serif;}  .MsoPapDefault  	{margin-bottom:8.0pt;  	line-height:107%;}  @page WordSection1  	{size:8.5in 11.0in;  	margin:1.0in 1.0in 1.0in 1.0in;}  div.WordSection1  	{page:WordSection1;}  -->  </style>    </head>    <body lang=EN-US>    <div class=WordSection1>    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:  normal'><b><span style='font-size:18.0pt'>Status Pengajuan Rancangan PERDA</span></b></p>    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:  normal'>&nbsp;</p>    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:  normal'>&nbsp;</p>    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:  normal'>Jakarta, ".$tgl_now."</p>    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:  normal'>Kepada Yth</p>    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:  normal'>Bapak/Ibu ".$okirim."</p>    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:  normal'>Di tempat</p>    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:  normal'>&nbsp;</p>    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:  normal'>Dengan hormat</p>    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:  normal'>&nbsp;</p>    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:  normal'>Bersama ini diberitahukan bahwa pengajuan Rancangan PERDA yang  Bapak/Ibu ajukan pada tanggal ".$tgl_kirim." dengan judul ".$judul." telah kami  telaah dengan hasil sebagai berikut:</p>    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:  normal'>&nbsp;</p>    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:  normal'>".$rev."</p>    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:  normal'>&nbsp;</p>    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:  normal'>Untuk itu silakan Bapak/Ibu lakukan revisi dan kemudian dikirimkan  kembali kepada Kami.</p>    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:  normal'>&nbsp;</p>    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:  normal'>Demikian disampaikan. Atas perhatian dan kerjasamanya diucapkan terima  kasih.</p>    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:  normal'>&nbsp;</p>    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:  normal'>&nbsp;</p>    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:  normal'>&nbsp;</p>    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:  normal'>Hormat kami</p>    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:  normal'>&nbsp;</p>    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:  normal'>&nbsp;</p>    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:  normal'>&nbsp;</p>    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:  normal'>Pengelola ePERDA Kementerian Dalam Negeri</p>    </div>    </body>    </html>  ";
		} else {
		//disetujui
			$isian = "<html>    <head>  <meta http-equiv=Content-Type content=\"text/html; charset=windows-1252\">  <meta name=Generator content=\"Microsoft Word 15 (filtered)\">  <style>  <!--   /* Font Definitions */   @font-face  	{font-family:\"Cambria Math\";  	panose-1:2 4 5 3 5 4 6 3 2 4;}  @font-face  	{font-family:Calibri;  	panose-1:2 15 5 2 2 2 4 3 2 4;}   /* Style Definitions */   p.MsoNormal, li.MsoNormal, div.MsoNormal  	{margin-top:0in;  	margin-right:0in;  	margin-bottom:8.0pt;  	margin-left:0in;  	line-height:107%;  	font-size:11.0pt;  	font-family:\"Calibri\",sans-serif;}  .MsoChpDefault  	{font-family:\"Calibri\",sans-serif;}  .MsoPapDefault  	{margin-bottom:8.0pt;  	line-height:107%;}  @page WordSection1  	{size:8.5in 11.0in;  	margin:1.0in 1.0in 1.0in 1.0in;}  div.WordSection1  	{page:WordSection1;}  -->  </style>    </head>    <body lang=EN-US>    <div class=WordSection1>    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:  normal'><b><span style='font-size:18.0pt'>Status Pengajuan Rancangan PERDA</span></b></p>    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:  normal'>&nbsp;</p>    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:  normal'>&nbsp;</p>    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:  normal'>Jakarta, ".$tgl_now."</p>    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:  normal'>Kepada Yth</p>    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:  normal'>Bapak/Ibu ".$okirim."</p>    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:  normal'>Di tempat</p>    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:  normal'>&nbsp;</p>    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:  normal'>Dengan hormat</p>    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:  normal'>&nbsp;</p>    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:  normal'>Bersama ini diberitahukan bahwa pengajuan Rancangan PERDA yang  Bapak/Ibu ajukan pada tanggal ".$tgl_kirim." dengan judul ".$judul." telah kami  telaah, telah kami setujui dan kami registrasikan dengan nomor Register  ".$noreg."</p>    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:  normal'>&nbsp;</p>    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:  normal'>Demikian disampaikan. Atas perhatian dan kerjasamanya diucapkan terima  kasih.</p>    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:  normal'>&nbsp;</p>    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:  normal'>&nbsp;</p>    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:  normal'>&nbsp;</p>    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:  normal'>Hormat kami</p>    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:  normal'>&nbsp;</p>    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:  normal'>&nbsp;</p>    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:  normal'>&nbsp;</p>    <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:  normal'>Pengelola ePERDA Kementerian Dalam Negeri</p>    </div>    </body>    </html>  ";
		}
		if( $adafb1 == 1 ) { $mail->AddAttachment('upload/'.$fbdb1); }
		if( $adafb2 == 1 ) { $mail->AddAttachment('upload/'.$fbdb2); }
		$mail->msgHTML($isian, dirname(__FILE__));
		if (!$mail->send()) { $sk =  $mail->ErrorInfo; } else { $sk = 1; }
	}
?>
<form action="pd_dreg.php" method="post" name="form1"></form>
<script>document.form1.submit();</script>
