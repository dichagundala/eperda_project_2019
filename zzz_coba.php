<?php include_once("zz_koneksi_db.php"); ?>
<?php include ("zz_generate_menu.php"); ?>
<!DOCTYPE HTML>
<html>

<head>
  <title>.:: ePERDA - Kementerian Dalam Negeri ::.</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>
</head>

<body>
  <div id="main">
    <header>
	<div id="logo">
        <div id="logo_text">
          <div id="Shape3">
          	<img src="images/logo.png" align="right">
            <div id="Shape4" align="right">
                Direktorat Produk Hukum Daerah<br>
                Direktorat Jenderal Otonomi Daerah<br>
            </div>
          </div>
          <h1><a href="index.php">e<span class="font_merah"><strong>PERDA</strong></span></a></h1>
          <h2>Kementerian Dalam Negeri</h2>
        </div>
      </div>
	  <?php generate_menu(1); ?>
      <div id="txt_register" align="right">Sudah terdaftar? <a href="pd_login.php"><span class="font_merah">Klik disini</span></a> | <a href="daftar.php">Daftar</a></div>
    </header>
	
    <div id="site_content">
      <div class="gallery"><?php generate_gallery($conn, 950, 150); ?></div>
      <div id="sidebar_container">
<?php
	$hsl = mysqli_query($conn, "select * from berita ORDER BY id DESC LIMIT 1");
	$B = mysqli_fetch_array($hsl);
?>	      
      	<?php generate_berita($B); ?>
		<?php generate_tautan($conn); ?>
      </div>
      <div class="content">
        <h1>Selamat Datang</h1>
        <p><br>
          <span style="text-align: justify"><strong>Aplikasi Online Register dan pemantauan Rancangan Peraturan Daerah (ePERDA)</strong> adalah sistem yang dikembangkan oleh Direktorat Produk Hukum Daerah - Direktorat Jenderal Otonomi Daerah - Kementerian Dalam Negeri Republik Indonesia yang bertujuan untuk mempercepat proses Register dan fasilitasi dari Rancangan Peraturan Daerah.<br>
          <br>
Alasan dan latar belakang dibangunnya aplikasi ePERDA ini melihat bahwa sampai saat ini pencantuman dan pendefinisian Nomor Register Peraturan Daerah masih bersifat manual yang mengakibatkan kurang optimalnya tujuan pencapaian tertib administrasi.<br><br>

Di sisi lain, selama sistem masih berjalan secara manual, Kementerian Dalam Negeri selalu menghadapi kendala dalam melakukan fasilitasi terhadap suatu Rancangan Peraturan Daerah, dimana dalam tahap fasilitasi tersebut Kementerian Dalam Negeri dapat menjaga agar Rancangan Peraturan Daerah yang diajukan dapat <em>in-line</em> dengan peraturan perundang-undangan yang lebih tinggi dan juga tanpa berbenturan dengan norma dan kepentingan umum dengan tujuan akhir bahwa Rancangan Peraturan Daerah tersebut dapat diimplementasikan untuk mencapai kesejahteraan umum.<br><br>

Akhir kata, semoga aplikasi ePERDA dapat memberikan kontribusi yang signifikan untuk peningkatan kinerja dan aparatur Pemerintah, khususnya di lingkungan Direktorat Produk Hukum Daerah - Direktorat Jenderal Otonomi Daerah - Kementerian Dalam Negeri Republik Indonesia, dan juga dapat meningkatkan akuntabilitas kinerja.<br><br><br>

Jakarta, 2016<br>
Direktur Produk Hukum Daerah<br>
Direktorat Jenderal Otonomi Daerah<br>
Kementerian Dalam Negeri Republik Indonesia<br><br><br>

DR. Kurniasih SH., MSi.

</span><br>
        <br>
        <br>
        <br>
        </p>
</div>
    </div>
    <?php generate_footer(); ?>
  </div>
  <p>&nbsp;</p>
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript" src="js/image_fade.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
    });
  </script>
</body>
</html>
