<?php include_once("zz_koneksi_db.php"); ?>
<?php include ("zz_generate_menu.php"); ?>
<?php
	session_start();
	$host  = $_SERVER['HTTP_HOST'];
	$uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
	$ip1 = $_SERVER['HTTP_X_FORWARDED_FOR'];
	$ip2 = $_SERVER['REMOTE_ADDR'];
	$waktu = date("Y-m-d H:i:s");
	$extra = "pd_login.php";
	if( $_SESSION["sws_id"] == "" || !isset($_SESSION["sws_id"]) || $_SESSION["sws_id"] == 0 ) {
		echo "<script>window.location.href=\"pd_login.php\";</script>";
		//header("Location: http://$host$uri/$extra");
		exit;
	}
	if( $_SESSION["sws_tingkat"] == 99 || $_SESSION["sws_tingkat"] == 1 || $_SESSION["sws_tingkat"] == 2 ) {
	} else {
		echo "<script>window.location.href=\"pd_login.php\";</script>";
		//header("Location: http://$host$uri/$extra");
		exit;
	}
	if( $_POST["proses"] ) {
		$a = $_POST["a"];
		$b = $_POST["b"];
		$c = $_POST["c"];
		$d = $_POST["d"];
	} else {
		$a = 0;
		$b = 0;
		$c = 0;
		$d = 1;
	}
	
	if( $b == 0 ) { $ostr = " ORDER BY id"; }
	if( $b == 1 ) { $ostr = " ORDER BY nomer"; }
	if( $b == 2 ) { $ostr = " ORDER BY judul"; }
	if( $b == 3 ) { $ostr = " ORDER BY waktu"; }
	if( $b == 4 ) { $ostr = " ORDER BY tingkat"; }
	if( $b == 5 ) { $ostr = " ORDER BY status"; }
	if( $b == 6 ) { $ostr = " ORDER BY prov"; }
	if( $c == 0 ) { $ostr .= " DESC"; } else { $ostr .= " ASC"; }
	
	$hsl = mysqli_query($conn, "select count(id) from tbl_perda_batal");
	if( mysqli_num_rows($hsl) == 0 ) { $jumlah_record = 0; }
	else {
		$B = mysqli_fetch_array($hsl);
		if( $B[0] == "" || !isset($B[0]) ) { $jumlah_record = 0; } else { $jumlah_record = $B[0]; }
	}
	$perhal = 10;
	$maxhal = floor($jumlah_record / $perhal);
	if( $maxhal * $perhal != $jumlah_record ) { $maxhal += 1; }
	$str_limit = " LIMIT ".(($d - 1)*$perhal).", ".$perhal;
	$fixquery = "select * from tbl_perda_batal $ostr $str_limit";
	//echo "FIXQUERY --> ".$fixquery;
	
	
	function get_waktu($a) {
		$dum = explode(" ",$a);
		$dum1 = explode("-", $dum[0]);
		$ostr = $dum1[2];
		if( $dum1[1] == "01" ) { $ostr .= " Januari ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "02" ) { $ostr .= " Februari ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "03" ) { $ostr .= " Maret ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "04" ) { $ostr .= " April ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "05" ) { $ostr .= " Mei ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "06" ) { $ostr .= " Juni ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "07" ) { $ostr .= " Juli ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "08" ) { $ostr .= " Agustus ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "09" ) { $ostr .= " September ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "10" ) { $ostr .= " Oktober ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "11" ) { $ostr .= " November ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "12" ) { $ostr .= " Desember ".$dum1[0]." ".$dum[1]; }

		return $ostr;
	}
?>


<!DOCTYPE HTML>
<html>

<head>
  <title>.:: ePERDA - Kementerian Dalam Negeri ::.</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>

<script language="javascript">

function rubah_data() {
	document.form_tampil.b.value = document.form1.b.value;
	document.form_tampil.c.value = document.form1.c.value;
	document.form_tampil.d.value = document.form1.d.value;
	document.form_tampil.submit();
}

function go_edit(a) {
	document.form_edit.id.value = a;
	document.form_edit.submit();
}

function unduh(a, b) {
	document.form_unduh.id.value = a;
	document.form_unduh.jenis.value = b;
	document.form_unduh.submit();
}

function review(a) {
	document.form_review.id.value = a;
	document.form_review.submit();
}

function go_hapus(a) {
	var ans; 
	ans=window.confirm('Yakin akan menon-aktifkan pengguna ini?');
	if( ans == true ) {
		document.form_hapus.id.value = a;
		document.form_hapus.submit();
	}
}



</script>  
  
</head>

<body>
  <div id="main">
    <header>
      <?php generate_logo(); ?>
      <?php generate_menu(5); ?>
    </header>
    <div id="site_content">
      <div class="gallery"><?php generate_gallery($conn, 950, 150); ?></div>
      <div class="content_webgis_up">
        <h1>Daftar PERDA yang Dibatalkan</h1>
<?php
	if( $jumlah_record == 0 ) {
?>		     
	<strong><br>
<br>
        TIDAK ADA DATA</strong><br>
        <br>
        <br>
<br>
        <br>
        <br>
<br><br><br><br>
<?php
	} else {
?>		
        <br>
        <br>
        <form name="form1" method="post" action="">
          <table width="100%" border="0" cellspacing="0" cellpadding="3">
            <tr>
              <td width="44%">Ditemukan <?php echo number_format($jumlah_record); ?> PERDA yang Dibatalkan</td>
              <td width="41%" align="right">Urut berdasar
                <select name="b" id="b" onChange="rubah_data();">
                  <option value="0" <?php if( $b == 0 ) { ?> selected <?php } ?>>ID</option>
                  <option value="1" <?php if( $b == 1 ) { ?> selected <?php } ?>>Nomer PERDA</option>
                  <option value="2" <?php if( $b == 2 ) { ?> selected <?php } ?>>Judul Perda</option>
                  <option value="3" <?php if( $b == 3 ) { ?> selected <?php } ?>>Waktu Pembatalan</option>
                  <option value="4" <?php if( $b == 4 ) { ?> selected <?php } ?>>Tingkat Wilayah</option>
                  <option value="5" <?php if( $b == 5 ) { ?> selected <?php } ?>>Status Pembatalan</option>
                  <option value="6" <?php if( $b == 6 ) { ?> selected <?php } ?>>Provinsi</option>
                  
              </select>
                <select name="c" id="c" onChange="rubah_data();">
                  <option value="0" <?php if( $c == 0 ) { ?> selected <?php } ?>>Menurun</option>
                  <option value="1" <?php if( $c == 1 ) { ?> selected <?php } ?>>Meningkat</option>
              </select></td>
              <td width="15%" align="right">Ke Halaman 
                <select name="d" id="d" onChange="rubah_data();">
<?php
		for( $iix = 1; $iix <= $maxhal; $iix++ ) {
?>
					<option value="<?php echo $iix; ?>" <?php if( $iix == $d ) { ?> selected <?php } ?>><?php echo $iix; ?></option>
<?php
		}
?>                
              </select></td>
            </tr>
          </table>
        </form>
        <br><br>
<?php
		$hsl = mysqli_query($conn, $fixquery);
		while( $B = mysqli_fetch_array($hsl) ) {
			$anid = $B["id"];
			$hsl1 = mysqli_query($conn, "select nama from tbl_prov where kode='".$B["prov"]."'");
			$B1 = mysqli_fetch_array($hsl1);
			$wilayah = $B1[0];
			if( $B["kabkota"] != 0 ) {
				$hsl1 = mysqli_query($conn, "select nama from tbl_kab where kode_kab='".$B["kabkota"]."'");
				$B1 = mysqli_fetch_array($hsl1);
				$wilayah .= " / ".$B1[0];
			}
			if( $B["sproses"] == 1 ) { $astat = "Dibatalkan oleh Kementerian Dalam Negeri"; }
			if( $B["sproses"] == 2 ) { $astat = "Dibatalkan oleh Pemerintah Daerah Tingkat Provinsi"; }
			if( $B["sproses"] == 3 ) { $astat = "Dibatalkan oleh Pemerintah Daerah Tingkat Kabupaten/Kota"; }
			$ringkasan = str_replace (array("\r\n", "\n", "\r"), '<br>', $B["ringkasan"]);
			$alasan = str_replace (array("\r\n", "\n", "\r"), '<br>', $B["alasan"]);
			if( $B["ada"] == 1 ) {
				$tdperda = "<td width=\"240\" onClick=\"unduh('$anid', 1);\" onMouseOver=\"this.style.cursor='pointer';\" style=\"font-size:x-small\" align=\"right\"><u>Unduh Perda yang dibatalkan</u></td>";
			} else {
				$tdperda = "<td width=\"240\">&nbsp;</td>";
			}
			if( $B["adask"] == 1 ) {
				$tdsk = "<td width=\"261\" onClick=\"unduh('$anid;', 2);\" onMouseOver=\"this.style.cursor='pointer';\" style=\"font-size:x-small\" align=\"right\"><u>Unduh Kepmen Pembatalan Perda</u></td>";
			} else {
				$tdsk = "<td width=\"261\">&nbsp;</td>";
			}
			
?>
 <table width="100%" border="0" cellspacing="0" cellpadding="3">
          <tr>
            <td><table class="sws_table" width="100%" border="0" cellspacing="0" cellpadding="10" style="border-bottom:medium solid #000000">
              <tr valign="top">
                <td width="192">Nomer PERDA</td>
                <td width="2">&nbsp;</td>
                <td colspan="3"><strong><?php echo $B["nomer"]; ?></strong></td>
              </tr>
              <tr valign="top">
                <td width="192">Judul</td>
                <td width="2">&nbsp;</td>
                <td colspan="3"><strong><?php echo $B["judul"]; ?></strong></td>
              </tr>
              <tr valign="top">
                <td>Ringkasan</td>
                <td>&nbsp;</td>
                <td colspan="3"><?php echo $ringkasan; ?></td>
              </tr>
              <tr valign="top">
                <td>Waktu Pembatalan</td>
                <td>&nbsp;</td>
                <td colspan="3"><?php echo get_waktu($B["waktu"]); ?></td>
              </tr>
              <tr valign="top">
                <td>Alasan Pembatalan</td>
                <td>&nbsp;</td>
                <td colspan="3"><?php echo $alasan; ?></td>
              </tr>
              <tr valign="top">
                <td>Provinsi / Kabupaten/Kota</td>
                <td>&nbsp;</td>
                <td colspan="3"><?php echo $wilayah; ?></td>
              </tr>
              <tr valign="top">
                <td>Status</td>
                <td>&nbsp;</td>
                <td colspan="3"><?php echo $astat; ?></td>
              </tr>
              <tr valign="top">
                <td>Nomor SK</td>
                <td>&nbsp;</td>
                <td colspan="3"><?php echo $B["nosk"]; ?></td>
              </tr>
              <tr valign="top">
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td width="147">&nbsp;</td>
                <?php echo $tdperda; ?><?php echo $tdsk; ?>
              </tr>
            </table></td>
          </tr>
        </table>              
<?php
		}
?>

        <p>&nbsp;</p>
<?php
	}
?>
        
      </div>
    </div>
    <?php generate_footer(); ?>
  </div>
  <p>&nbsp;</p>
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript" src="js/image_fade.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
    });
  </script>
  
<form action="pd_dbatal.php" method="post" name="form_tampil">
<input name="a" type="hidden" value="<?php echo $a; ?>" />
<input name="b" type="hidden" value="" />
<input name="c" type="hidden" value="" />
<input name="d" type="hidden" value="" />
<input name="proses" type="hidden" value="1" />
</form>
<form action="pd_review.php" method="post" name="form_review">
<input name="id" type="hidden" value="" />
</form>
<form action="pd_unduh_batal.php" method="post" name="form_unduh">
<input name="id" type="hidden" value="" />
<input name="jenis" type="hidden" value="" />
</form>  
  
</body>
</html>
