<?php
	include("zz_koneksi_db.php");
	
	session_start();
	$host  = $_SERVER['HTTP_HOST'];
	$uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
	$ip1 = $_SERVER['HTTP_X_FORWARDED_FOR'];
	$ip2 = $_SERVER['REMOTE_ADDR'];
	$waktu = date("Y-m-d H:i:s");
	$extra = "pd_login.php";
	if( $_SESSION["sws_id"] == "" || !isset($_SESSION["sws_id"]) || $_SESSION["sws_id"] == 0 ) {
		echo "<script>window.location.href=\"pd_login.php\";</script>";
		//header("Location: http://$host$uri/$extra");
		exit;
	}
	if( $_SESSION["sws_tingkat"] == 99 || $_SESSION["sws_tingkat"] == 1 || $_SESSION["sws_tingkat"] == 2 ) {
	} else {
		echo "<script>window.location.href=\"pd_login.php\";</script>";
		//header("Location: http://$host$uri/$extra");
		exit;
	}
	
	function uniqueFilename($strExt) {
		$arrIp = explode('.', $_SERVER['REMOTE_ADDR']);
		list($usec, $sec) = explode(' ', microtime());
		$usec = (integer) ($usec * 65536);
		$sec = ((integer) $sec) & 0xFFFF;
		$strUid = sprintf("%08x-%04x-%04x", ($arrIp[0] << 24) | ($arrIp[1] << 16) | ($arrIp[2] << 8) | $arrIp[3], $sec, $usec);
		// tack on the extension and return the filename
		return $strUid . $strExt;
	}
	
	$judul = addslashes($_POST["judul"]);
	$nr = addslashes($_POST["nomer"]);
	$thn = addslashes($_POST["tahun"]);
	$prov = $_POST["prov"];
	$ringkasan = addslashes($_POST["ringkasan"]);
	$waktu = $_POST["waktu"];
	$alasan = addslashes($_POST["alasan"]);
	$nosk = addslashes($_POST["skep"]);
	$nomer = $nr." TAHUN ".$thn;
	if( empty($_FILES['berkas']) ) {
		$ab = 0;
		$fname = "";
		$fname_db = "";
	} else {
		$ab = 1;
		$fname = $_FILES['berkas']['name'];
		$dum = explode(".",$fname);
		if( count($dum) > 1 ) {
			$fname_db = uniqueFilename(".".$dum[count($dum)-1]);
		} else {
			$fname_db = uniqueFilename("");
		}
		$uploaddir = "pembatalan/".$fname_db;
		if(move_uploaded_file($_FILES['berkas']['tmp_name'], $uploaddir)) {
		} else {
			$ab = 0;
			$fname = "";
			$fname_db = "";
		}
	}
	
	if( empty($_FILES['bskep']) ) {
		$adask = 0;
		$ska = "";
		$skdb = "";
	} else {
		$adask = 1;
		$ska = $_FILES['bskep']['name'];
		$dum = explode(".",$ska);
		if( count($dum) > 1 ) {
			$skdb = uniqueFilename(".".$dum[count($dum)-1]);
		} else {
			$skdb = uniqueFilename("");
		}
		$uploaddir = "pembatalan/".$skdb;
		if(move_uploaded_file($_FILES['bskep']['tmp_name'], $uploaddir)) {
		} else {
			$adask = 0;
			$ska = "";
			$skdb = "";
		}
	}
	
	if( $_POST["kab"] == 0 || !isset($_POST["kab"]) ) {
		$tingkat = 1;
		$kab = 0;
	} else {
		$tingkat = 2;
		$kab = $_POST["kab"];
	}
	$sproses = $_POST["sbatal"];
	$wproses = date("Y-m-d H:i:s");
	$opid = $_SESSION["sws_id"];
	$opnama = addslashes($_SESSION["sws_nama_pengguna"]);
	mysqli_query($conn, "insert into tbl_perda_batal VALUES(null, '$nomer', '$judul', '$ringkasan', '$waktu', '$alasan', '$tingkat', '$prov', '$kab', '$sproses', '$ab', '$fname', '$fname_db', '$wproses', '$opid', '$opnama', '$adask', '$ska', '$skdb', '$nosk', '10', '$nr', '$thn');");
	
?>
<form action="pd_ebatal.php" method="post" name="form1"></form>
<script>document.form1.submit();</script>