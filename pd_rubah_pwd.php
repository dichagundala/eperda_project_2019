<?php include_once("zz_koneksi_db.php"); ?>
<?php include ("zz_generate_menu.php"); ?>
<?php
	session_start();
	$host  = $_SERVER['HTTP_HOST'];
	$uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
	$ip1 = $_SERVER['HTTP_X_FORWARDED_FOR'];
	$ip2 = $_SERVER['REMOTE_ADDR'];
	$waktu = date("Y-m-d H:i:s");
	$extra = "pd_login.php";
	if( $_SESSION["sws_id"] == "" || !isset($_SESSION["sws_id"]) || $_SESSION["sws_id"] == 0 ) {
		echo "<script>window.location.href=\"pd_login.php\";</script>";
		//header("Location: http://$host$uri/$extra");
		exit;
	}
	$ket = "";
	$hsl = mysqli_query($conn, "select * from tbl_pengguna where id='".$_SESSION["sws_id"]."'");
	if( mysqli_num_rows($hsl) == 0 ) {
		echo "<script>window.location.href=\"pd_login.php\";</script>";
		//header("Location: http://$host$uri/$extra");
		exit;
	}
	if( $_POST["button"] ) {
		$anid = $_POST["id"]; 
		if( $_POST["nlengkap"] == "" || !isset($_POST["nlengkap"]) ) {
			$ket = "Perubahan profil pengguna tidak dapat dilakukan. Harap mengisi nama lengkap."; 
		} else {
			$nl = addslashes($_POST["nlengkap"]);
		}
		if( $_POST["cbpwd"] ) {
			//ganti pwd
			if( $_POST["pwdlama"] == "" || !isset($_POST["pwdlama"]) ) { $pwdlama = ""; } else { $pwdlama = $_POST["pwdlama"]; }
			if( $_POST["pwdbaru1"] == "" || !isset($_POST["pwdbaru1"]) ) { $pwdbaru1 = ""; } else { $pwdbaru1 = $_POST["pwdbaru1"]; }
			if( $_POST["pwdbaru2"] == "" || !isset($_POST["pwdbaru2"]) ) { $pwdbaru2 = ""; } else { $pwdbaru2 = $_POST["pwdbaru2"]; }
			$hsl = mysqli_query($conn, "select * from tbl_pengguna where id='$id' and pwd=password('$pwdlama')");
			if( mysqli_num_rows($hsl) == 0 ) {
				$ket = "Perubahan profil pengguna tidak dapat dilakukan. Kata sandi lama tidak dapat diverifikasi.";
			} else {
				if( $pwdbaru1 != $pwdbaru2 ) {
					$ket = "Perubahan profil pengguna tidak dapat dilakukan. Kata sandi baru tidak dapat dikonfirmasi.";
				} else {
					mysqli_query($conn, "update tbl_pengguna SET nlengkap='$nl', pwd=password('$pwdbaru1') where id='$id'");
					$ket = "Perubahan profil berhasil dilakukan. Perubahan akan berefek ketika masuk kembali ke halaman terbatas";
				}
			}
		} else {
			mysqli_query($conn, "update tbl_pengguna SET nlengkap='$nl' where id='$id'");
			$ket = "Perubahan profil berhasil dilakukan. Perubahan akan berefek ketika masuk kembali ke halaman terbatas";
		}
	}
	
	$hsl = mysqli_query($conn, "select * from tbl_pengguna where id='".$_SESSION["sws_id"]."'");	
	$B = mysqli_fetch_array($hsl);
	$id = $B[0];
	$nlog = $B[1];
	$nlengkap = $B[2];
	$tingkat = $B[4];
	$prov = $B[5];
	$kab = $B[6];
	switch ( $tingkat ) {
		case 1: $tk = "Pengguna dengan otoritas Supervisor"; break;
		case 2: 
			$hsl = mysqli_query($conn, "select nama from tbl_provinsi where kode='$prov'");
			$B = mysqli_fetch_array($hsl);
			$tk = "Pengguna dengan otoritas petugas Provinsi ".$B[0];
			break;
		case 3:
			$hsl = mysqli_query($conn, "select nama from tbl_kab where kode_kab='$kab'");
			$B = mysqli_fetch_array($hsl);
			$tk = "Pengguna dengan otoritas petugas Kabupaten/Kota ".$B[0];
			$hsl = mysqli_query($conn, "select nama from tbl_prov where kode ='$prov'");
			$B = mysqli_fetch_array($hsl);
			$tk .= " - Provinsi ".$B[0];
			break;
		case 99:
			$tk = "Pengguna dengan otoritas Administrator"; break;
		default:
			echo "<script>window.location.href=\"pd_login.php\";</script>";
			//header("Location: http://$host$uri/$extra");
			exit;
	}

?>


<!DOCTYPE HTML>
<html>

<head>
  <title>.:: ePERDA - Kementerian Dalam Negeri ::.</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>
</head>

<body>
  <div id="main">
    <header>
      <?php generate_logo(); ?>
      <?php generate_menu(5); ?>
    </header>
    <div id="site_content">
      <div class="gallery"><?php generate_gallery($conn, 950, 150); ?></div>
      <div class="content_webgis">
        <h1>Perubahan Profil Pengguna</h1>
        <form name="form1" method="post" action="">
          <table width="95%" border="0" cellspacing="0" cellpadding="3">
            <tr>
              <td width="23%">Nama Login</td>
              <td width="2%">:</td>
              <td width="75%"><?php echo $nlog; ?><input name="id" type="hidden" value="<?php echo $id; ?>"></td>
            </tr>
            <tr>
              <td>Nama Lengkap</td>
              <td>:</td>
              <td><input name="nlengkap" type="text" id="nlengkap" value="<?php echo $nlengkap; ?>"></td>
            </tr>
            <tr>
              <td>Otoritas</td>
              <td>:</td>
              <td><?php echo $tk; ?></td>
            </tr>
            <tr>
              <td align="right"><input name="cbpwd" type="checkbox" id="cbpwd" value="1"></td>
              <td>&nbsp;</td>
              <td>Merubah Kata Sandi</td>
            </tr>
            <tr>
              <td>Kata Sandi Lama</td>
              <td>:</td>
              <td><input type="password" name="pwd_lama" id="pwd_lama"></td>
            </tr>
            <tr>
              <td>Kata Sandi Baru</td>
              <td>:</td>
              <td><input type="password" name="pwd_baru" id="pwd_baru"></td>
            </tr>
            <tr>
              <td>Konfirmasi Kata Sandi Baru</td>
              <td>&nbsp;</td>
              <td><input type="password" name="pwd_baru1" id="pwd_baru1"></td>
            </tr>
            <tr>
              <td colspan="3" align="center"><br>
                <br>
                <span style="color: #F00; font-weight: bold;"><?php echo $ket; ?></span><br>
              <br>
              <input type="submit" name="button" id="button" value="Proses"></td>
            </tr>
          </table>
        </form>
        <p>&nbsp;</p>
      </div>
    </div>
    <?php generate_footer(); ?>
  </div>
  <p>&nbsp;</p>
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript" src="js/image_fade.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
    });
  </script>
</body>
</html>
