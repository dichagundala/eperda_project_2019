<?php include_once("zz_koneksi_db.php"); ?>
<?php include ("zz_generate_menu.php"); ?>
<?php
	require 'PHPMailer/PHPMailerAutoload.php';
	session_start();
	$host  = $_SERVER['HTTP_HOST'];
	$uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
	$ip1 = $_SERVER['HTTP_X_FORWARDED_FOR'];
	$ip2 = $_SERVER['REMOTE_ADDR'];
	$waktu = date("Y-m-d H:i:s");
	$extra = "pd_login.php";
	if( $_SESSION["sws_id"] == "" || !isset($_SESSION["sws_id"]) || $_SESSION["sws_id"] == 0 ) {
		header("Location: http://$host$uri/$extra");
		exit;
	}
	if( $_SESSION["sws_tingkat"] == 99 || $_SESSION["sws_tingkat"] == 1 || $_SESSION["sws_tingkat"] == 2 ) {
	} else {
		header("Location: http://$host$uri/$extra");
		exit;
	}
	
	function get_waktu($a) {
		$dum = explode(" ",$a);
		$dum1 = explode("-", $dum[0]);
		$ostr = $dum1[2];
		$dum[1] = "";
		if( $dum1[1] == "01" ) { $ostr .= " Januari ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "02" ) { $ostr .= " Februari ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "03" ) { $ostr .= " Maret ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "04" ) { $ostr .= " April ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "05" ) { $ostr .= " Mei ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "06" ) { $ostr .= " Juni ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "07" ) { $ostr .= " Juli ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "08" ) { $ostr .= " Agustus ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "09" ) { $ostr .= " September ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "10" ) { $ostr .= " Oktober ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "11" ) { $ostr .= " November ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "12" ) { $ostr .= " Desember ".$dum1[0]." ".$dum[1]; }
		
		return $ostr;
	}
	
	function uniqueFilename($strExt) {
		$arrIp = explode('.', $_SERVER['REMOTE_ADDR']);
		list($usec, $sec) = explode(' ', microtime());
		$usec = (integer) ($usec * 65536);
		$sec = ((integer) $sec) & 0xFFFF;
		$strUid = sprintf("%08x-%04x-%04x", ($arrIp[0] << 24) | ($arrIp[1] << 16) | ($arrIp[2] << 8) | $arrIp[3], $sec, $usec);
		// tack on the extension and return the filename
		return $strUid . $strExt;
	}
	
	
	
	$anid = $_POST["id"];
	
	$hsl = mysqli_query($conn, "select * from tbl_reg_ranperda where id='$anid'");
	if( mysqli_num_rows($hsl) == 0 ) {
		header("Location: http://$host$uri/$extra");
	} else {
		$B = mysqli_fetch_array($hsl);
		$judul = $B["judul"];
		if( $B["jenis"] == 1 ) { $jeval = "Evaluasi"; } else { $jeval = "Non Evaluasi"; }
		$nosk = $B["no_skreg"];
		$thsk = $B["tahun"];
		$tglsk = get_waktu($B["tgl_skreg"]);
		$tglkel = get_waktu($B["tgl_keluar"]);
		if( $B["ada_skreg"] == 1 ) { $adafb1 = 1; $skdb = $B["skreg_db"]; } else { $adafb1 = 0; $skdb = ""; }
		if( $B["ada_btamb"] == 1 ) { $adafb2 = 1; $tamb = $B["btamb_db"]; } else { $adafb2 = 0; $tamb = ""; }
 		$email = $B["kirimke"];
		$noreg = $B["noreg"];
		$klasifikasi = $B["klasifikasi"];
		$okirim = $B["okirim_nama"];
		$tgl_aju = get_waktu($B["wkirim"]);
		$pengantar = $B["pengantar"];
		$ringkasan = str_replace (array("\r\n", "\n", "\r"), '<br>', $B["ringkasan"]);
	}
	$tnow = get_waktu(date("Y-m-d H:i:s"));
	
	//sekarang kirim email
	$mail = new PHPMailer();
	$mail->isSMTP();
	$mail->SMTPDebug = 0;
	$mail->Debugoutput = 'html';
	$mail->Host = 'mail.eperda-chat.com';
	$mail->Port = 587;
	$mail->SMTPSecure = 'tls';
	$mail->SMTPAuth = true;
	$mail->Username = "admin@eperda-chat.com";
	$mail->Password = "ePERDAindoglobal2017";
	$mail->setFrom('admin@eperda-chat.com', 'Webadmin ePERDA Ditjen Otonomi Daerah');
	$mail->addReplyTo('admin@eperda-chat.com', 'Webadmin ePERDA Ditjen Otonomi Daerah');
	$mail->addAddress($email, $nama);
	$mail->Subject = 'Register Rancangan PERDA';
	$isian = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\">
<head>
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
<title>Untitled Document</title>
<style type=\"text/css\">
.sws_fnt_med {
	font-family: Verdana, Geneva, sans-serif;
	font-size: large;
}
.sws_fnt_biasa {
	font-family: Verdana, Geneva, sans-serif;
}
</style>
</head>

<body>
<p class=\"sws_fnt_med\"><strong>Permohonan Nomor Register PERDA</strong></p>
<p>&nbsp;</p>
<p class=\"sws_fnt_biasa\">Jakarta, $tnow<br />
  Kepada Yth<br />
  Bapak/Ibu <strong>$okirim</strong><br />
  Di tempat</p><p>&nbsp;</p><p>&nbsp;</p>
<p class=\"sws_fnt_biasa\">Dengan hormat</p>
<p class=\"sws_fnt_biasa\">Bersama ini diberitahukan bahwa pengajuan Rancangan PERDA yang Bapak/Ibu ajukan pada tanggal $tgl_aju tentang <br />
  <br />
  <strong>&quot;$judul&quot;</strong><br />
  <br />
  Telah kami proses dan kami  registerkan dengan Nomor Register <strong>$noreg</strong> pada tanggal <strong>$tglsk</strong>. Salinan Surat terlampir.</p>
<p class=\"sws_fnt_biasa\">Demikian disampaikan. Atas perhatian dan kerjasamanya diucapkan terima  kasih.</p>
<p>&nbsp;</p>
<p class=\"sws_fnt_biasa\">Hormat kami</p>
<p>&nbsp;</p>
<p class=\"sws_fnt_biasa\">Pengelola ePERDA Kementerian Dalam Negeri</p>
</body>
</html>";
	if( $adafb1 == 1 ) { $mail->AddAttachment('upload/'.$skdb); }
	if( $adafb2 == 1 ) { $mail->AddAttachment('upload/'.$tamb); }

	$mail->msgHTML($isian, dirname(__FILE__));
	if (!$mail->send()) { $sk =  $mail->ErrorInfo; } else { $sk = 1; }
	
?>
<!DOCTYPE HTML>
<html>

<head>
  <title>.:: ePERDA - Kementerian Dalam Negeri ::.</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>
  
</head>

<body>
  <div id="main">
    <header>
	  <?php generate_logo(); ?>
      <?php generate_menu(7); ?>
    </header>
    <div id="site_content">
      <div class="gallery"><?php generate_gallery($conn, 950, 150); ?></div>
      <div class="content">
        <h1>Register Rancangan Perda</h1>
        <p>Register Rancangan Perda dengan informasi di bawah ini sudah selesai dikeluarkan. Sebuah email sudah dilayangkan ke pemohon dengan alamat: <?php echo $email; ?><br>
          <br>
        </p>
        <table width="100%" border="0" cellspacing="0" cellpadding="3">
          <tr valign="top">
            <td width="35%">Judul Ranperda</td>
            <td width="65%"><?php echo $judul; ?></td>
          </tr>
          <tr valign="top">
            <td>Jenis</td>
            <td><?php echo $jeval; ?></td>
          </tr>
          <tr valign="top">
            <td>Klasifikasi</td>
            <td><?php echo $klasifikasi; ?></td>
          </tr>
          <tr valign="top">
            <td>No. Register</td>
            <td><?php echo $noreg; ?></td>
          </tr>
          <tr valign="top">
            <td>No. SK Register</td>
            <td><?php echo $nosk; ?></td>
          </tr>
          <tr valign="top">
            <td>Tgl SK</td>
            <td><?php echo $tglsk; ?></td>
          </tr>
          <tr valign="top">
            <td>Tgl SK Keluar</td>
            <td><?php echo $tglkel; ?></td>
          </tr>
          <tr valign="top">
            <td>Dikirim ke</td>
            <td><?php echo $okirim; ?></td>
          </tr>
        </table>
        <p>Sebuah email berisi informasi ini juga sudah dilayangkan ke alamat <?php echo $email; ?>.<br>
          <br>
        .</p>
        <p><br>
        <br><br><br><br>
      </div>
    </div>
    <?php generate_footer(); ?>
  </div>
  <p>&nbsp;</p>
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript" src="js/image_fade.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
    });
  </script>
</body>
</html>
