<?php include_once("zz_koneksi_db.php"); ?>
<?php include ("zz_generate_menu.php"); ?>
<?php
	session_start();
	$host  = $_SERVER['HTTP_HOST'];
	$uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
	$ip1 = $_SERVER['HTTP_X_FORWARDED_FOR'];
	$ip2 = $_SERVER['REMOTE_ADDR'];
	$waktu = date("Y-m-d H:i:s");
	$extra = "index.php";
	if( $_GET["a"] == "" || !isset($_GET["a"]) || $_GET["b"] == "" || !isset($_GET["b"]) ) {
		echo "<script>window.location.href=\"index.php\";</script>";
		//header("Location: http://$host$uri/$extra");
		exit;
	}
	$a = addslashes($_GET["a"]);
	$b = addslashes($_GET["b"]);
	$hsl = mysqli_query($conn, "select * from tbl_lupa where userid='$a' and token='$b' and valid=1");
	if( mysqli_num_rows($hsl) == 0 ) {
		echo "<script>window.location.href=\"index.php\";</script>";
		//header("Location: http://$host$uri/$extra");
		exit;
	}
	$id = $a;
	$B = mysqli_fetch_array($hsl);
	$lupaid = $B["id"];
	$ket = "";
	echo "select * from tbl_pengguna where id='$a'";
	$hsl = mysqli_query($conn, "select * from tbl_pengguna where id='$a'");
	if( mysqli_num_rows($hsl) == 0 ) {
		echo "<script>window.location.href=\"index.php\";</script>";
		//header("Location: http://$host$uri/$extra");
		exit;
	}
	$B = mysqli_fetch_array($hsl);
	$id = $B[0];
	$nlog = $B[1];
	$nlengkap = $B[2];
	$tingkat = $B[4];
	$prov = $B[5];
	$kab = $B[6];
	$email = $B["email"];
	$notelp1 = $B["notel1"];
	$notelp2 = $B["notel2"];
	$nohp = $B["nohp"];
	$jab = $B["jabatan"];
	switch ( $tingkat ) {
		case 10: 
			$hsl = mysqli_query($conn, "select nama from tbl_provinsi where kode='$prov'");
			$B = mysqli_fetch_array($hsl);
			$tk = "Pengguna dengan otoritas petugas Provinsi ".$B[0];
			break;
		case 11:
			$hsl = mysqli_query($conn, "select nama from tbl_kab where kode_kab='$kab'");
			$B = mysqli_fetch_array($hsl);
			$tk = "Pengguna dengan otoritas petugas Kabupaten/Kota ".$B[0];
			$hsl = mysqli_query($conn, "select nama from tbl_prov where kode ='$prov'");
			$B = mysqli_fetch_array($hsl);
			$tk .= " - Provinsi ".$B[0];
			break;
	}

?>


<!DOCTYPE HTML>
<html>

<head>
  <title>.:: ePERDA - Kementerian Dalam Negeri ::.</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>
    <script>
  	function checkit() {
		var errorMsg = "";
	
		//Check for a username
		if (document.form1.nlengkap.value==""){ errorMsg += "\n\t - Isi nama lengkap dahulu";}
		if (document.form1.jab.value==""){ errorMsg += "\n\t - Isi jabatan dahulu";}
		if (document.form1.email.value==""){ errorMsg += "\n\t - Isi email dahulu";}
		if (document.form1.notelp1.value==""){ errorMsg += "\n\t - Isi nomor telpon dahulu";}
		if (document.form1.nohp.value==""){ errorMsg += "\n\t - Isi nomor HP dahulu";}
		if (document.form1.pwd_baru.value==""){ errorMsg += "\n\t - Kata sandi tidak boleh kosong";}
		if (document.form1.pwd_baru.value!=document.form1.pwd_baru1.value){ errorMsg += "\n\t - Kata sandi tidak dapat diverifikasi";}
		
		if (errorMsg != ""){
			msg  = "___________________________________________________________________\n\n";
			msg += "Pendaftaran belum bisa dilakukan dikarenakan kesalahan berikut ini.\n";
			msg += "___________________________________________________________________\n\n";
			errorMsg += alert(msg + errorMsg + "\n\n");
			return false;
		}
		
		return true;
	}
	</script>
</head>

<body>
  <div id="main">
    <header>
      <?php generate_logo(); ?>
      <?php generate_menu(5); ?>
    </header>
    <div id="site_content">
      <div class="gallery"><?php generate_gallery($conn, 950, 150); ?></div>
      <div class="content_webgis">
        <h1><strong>Reset Profil Pengguna</strong></h1>
        <form name="form1" method="post" action="lupa_sandi_verify_confirm.php" onSubmit="return checkit();">
          <table width="95%" border="0" cellspacing="0" cellpadding="3">
            <tr>
              <td width="18%">Nama Login</td>
              <td width="2%">:</td>
              <td width="80%"><?php echo $nlog; ?><input name="id" type="hidden" value="<?php echo $id; ?>"><input name="lupaid" type="hidden" value="<?php echo $lupaid; ?>"></td>
            </tr>
            <tr>
              <td>Nama Lengkap</td>
              <td>:</td>
              <td><input name="nlengkap" type="text" id="nlengkap" value="<?php echo $nlengkap; ?>" size="50" maxlength="50"></td>
            </tr>
            <tr>
              <td>Jabatan</td>
              <td>:</td>
              <td><input name="jab" type="text" id="jab" value="<?php echo $jab; ?>" size="50" maxlength="50"></td>
            </tr>
            <tr>
              <td>Otoritas</td>
              <td>:</td>
              <td><?php echo $tk; ?></td>
            </tr>
            <tr>
              <td>Alamat Email</td>
              <td>:</td>
              <td><input name="email" type="text" id="email" value="<?php echo $email; ?>" size="40" maxlength="40"></td>
            </tr>
            <tr>
              <td>No. Telpon</td>
              <td>:</td>
              <td><input name="notelp1" type="text" id="notelp1" value="<?php echo $notelp1; ?>">
              <input name="notelp2" type="text" id="notelp2" value="<?php echo $notelp2; ?>"></td>
            </tr>
            <tr>
              <td>No. HP</td>
              <td>:</td>
              <td><input name="nohp" type="text" id="nohp" value="<?php echo $nohp; ?>"></td>
            </tr>
            <tr>
              <td>Kata Sandi</td>
              <td>:</td>
              <td><input type="password" name="pwd_baru" id="pwd_baru"></td>
            </tr>
            <tr>
              <td>Konfirmasi Kata Sandi </td>
              <td>:</td>
              <td><input type="password" name="pwd_baru1" id="pwd_baru1"></td>
            </tr>
            <tr>
              <td colspan="3" align="center"><br>
                <br>
                <span style="color: #F00; font-weight: bold;"><?php echo $ket; ?></span><br>
              <br>
              <input type="submit" name="button" id="button" value="Proses"></td>
            </tr>
          </table>
        </form>
        <p>&nbsp;</p>
      </div>
    </div>
    <?php generate_footer(); ?>
  </div>
  <p>&nbsp;</p>
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript" src="js/image_fade.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
    });
  </script>
</body>
</html>
