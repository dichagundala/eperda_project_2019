
<?php
include_once("zz_koneksi_db.php");
	include ("zz_generate_menu.php");
	session_start();
	$host  = $_SERVER['HTTP_HOST'];
	$uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
	$ip1 = $_SERVER['HTTP_X_FORWARDED_FOR'];
	$ip2 = $_SERVER['REMOTE_ADDR'];
	$waktu = date("Y-m-d H:i:s");
	$kesalahan = $_POST["kesalahan"];
?>


<!DOCTYPE HTML>
<html>

<head>
  <title>.:: ePERDA - Kementerian Dalam Negeri ::.</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>
  

</head>

<body>
  <div id="main">
    <header>
      <?php generate_logo(); ?>
      <?php generate_menu(5); ?>
    </header>
    <div id="site_content">
      <div class="gallery"><?php generate_gallery($conn, 950, 150); ?></div>
      <div class="content_webgis">
        <h1><strong>Pendaftaran Pengguna</strong><br>
          <br>
          <br>
          <br>
        </h1>
        <table class="sws_table" width="100%" border="0" cellspacing="0" cellpadding="3">
<?php
	if( $kesalahan == 0 ) {
?>		        
          <tr align="center">
            <td>Proses pendaftaran berhasil. Jika pendaftaran anda diterima, maka sebuah email pemberitahuan akan dikirimkan ke alamat email anda.<br>
            <br>
            Terima kasih atas perhatiannya.<br>
            <br>
            <br>
            Hormat kami<br>
            <br>
<br>
            Pengelola ePERDA Kementerian Dalam Negeri<br>
            <br>
            <br></td>
          </tr>
<?php
	} else {
		if( $kesalahan == 1 ) { $psn_kesalahan = "nama login yang diinginkan sudah dipergunakan pengguna lain"; }
		elseif( $kesalahan == 2 ) { $psn_kesalahan = "nama login yang diinginkan sudah didaftarkan pengguna lain"; }
		else { $psn_kesalahan = "alasan lainnya"; }
?>
		        
          <tr align="center">
            <td>Proses pendaftaran belum berhasil dilakukan karena <?php echo $psn_kesalahan; ?>. Silakan coba lakukan pendaftaran dengan nama login yang berbeda <a href="daftar.php">disini</a><br>
            <br>
            Terima kasih atas perhatiannya.<br>
            <br>
            <br>
            Hormat kami<br>
            <br>
<br>
            Pengelola ePERDA Kementerian Dalam Negeri<br>
            <br>
            <br></td>
          </tr>		
<?php
	}
?>
        </table>
        <p><br>
          <br>
        </p>
      </div>
    </div>
    <?php generate_footer(); ?>
  </div>
  <p>&nbsp;</p>
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript" src="js/image_fade.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
    });
  </script>
</body>
</html>
