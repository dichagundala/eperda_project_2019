<?php
require('../includes/init.php');
if(!check_login())
	header('location: ../../pd_login.php');
else {
	$username = get_username();
	$enroll = get_enroll();
?>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="chat.css" />
<script type="text/javascript" src="jquery.js"></script>
<script type="text/javascript" src="chat.js"></script>
<title>Chatroom</title>
</head>
<body>
<table width="100%" border="1" cellspacing="0" cellpadding="3" height="410">
  <tr height="410">
    <td valign="top" bgcolor="#EEEEEE">
		<div id='chatbox'></div>    
    </td>
    <td width="210" bgcolor="#FFFFFF">
	<div id="online_box" class="online">
		<audio controls="controls" style="display:none;" id="soundHandle"></audio>  <!--this tag is for chat sound	-->
		<div id="online_title_box" class="online" >
			<div id="online_title" onClick="goOnline();">Daftar On-line</div>
		</div>
		
		<div id="online_users_box" class="online">
		</div>
 			
		<div id="online_search_box" class="online" style="display:none" >
			<input type="text" name="user_search" value="Search" onKeyDown="searchUsersOnline()"/>
		</div>
	</div>
    </td>
  </tr>
</table>



</body>
</html>
<?php
}
?>
