<?php include_once("zz_koneksi_db.php"); ?>
<?php include ("zz_generate_menu.php"); ?>
<!DOCTYPE HTML>
<html>

<head>
  <title>.:: ePERDA - Kementerian Dalam Negeri ::.</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>
</head>

<body>
  <div id="main">
    <header>
	  <?php generate_logo(); ?>
      <?php generate_menu(6); ?>
    </header>
    <div id="site_content">
      <div class="gallery"><?php generate_gallery($conn, 950, 150); ?></div>
      <div class="content">
        <h1>Saran Masukan</h1>
        <p>Jika ada pertanyaan, saran dan masukan, silakan isikan form di bawah ini dengan lengkap. Korespondensi selanjutnya akan berlangsung melalui email. Terima kasih.</p>
        <?phpphp
          // This PHP Contact Form is offered &quot;as is&quot; without warranty of any kind, either expressed or implied.
          // Andrew Raynor at www.css3templates.co.uk shall not be liable for any loss or damage arising from, or in any way
          // connected with, your use of, or inability to use, the website templates (even where Andrew Raynor has been advised
          // of the possibility of such loss or damage). This includes, without limitation, any damage for loss of profits,
          // loss of information, or any other monetary loss.

          // Set-up these 3 parameters
          // 1. Enter the email address you would like the enquiry sent to
          // 2. Enter the subject of the email you will receive, when someone contacts you
          // 3. Enter the text that you would like the user to see once they submit the contact form
          $to = 'Masukkan alamat email disini';
          $subject = 'Masukan dari situs';
          $contact_submitted = 'Pesan Anda sudah terkirim.';

          // Do not amend anything below here, unless you know PHP
          function email_is_valid($email) {
            return preg_match('/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i',$email);
          }
          if (!email_is_valid($to)) {
            echo '<p style="color: red;"></p>';
          }
          if (isset($_POST['contact_submitted'])) {
            $return = "\r";
            $youremail = trim(htmlspecialchars($_POST['your_email']));
            $yourname = stripslashes(strip_tags($_POST['your_name']));
            $yourmessage = stripslashes(strip_tags($_POST['your_message']));
            $contact_name = "Name: ".$yourname;
            $message_text = "Message: ".$yourmessage;
            $user_answer = trim(htmlspecialchars($_POST['user_answer']));
            $answer = trim(htmlspecialchars($_POST['answer']));
                        $message = $contact_name . $return . $message_text;
            $headers = "From: ".$youremail;
			//preg_match("%{$regex}%i", $url))
			
            if (email_is_valid($youremail) && !preg_match("%{\r}%i",$youremail) && !preg_match("%{\n}%i",$youremail) && $yourname != "" && $yourmessage != "" && substr(md5($user_answer),5,10) === $answer) {
			  mysqli_query($conn, "insert into kontak VALUES(null,'".addslashes($yourname)."','".addslashes($youremail)."','".addslashes($yourmessage)."','".date("Y-m-d H:i:s")."',1);");
              mail($to,$subject,$message,$headers);
              $yourname = '';
              $youremail = '';
              $yourmessage = '';
              echo '<p style="color: blue;">'.$contact_submitted.'</p>';
            }
            else echo '<p style="color: red;">Harap isikan nama, alamat email yang valid, pesan dan/atau masukan Anda. Selain itu harap menjawab pertanyaan sederhana di bagian bawah form sebelum mengirimkan pesan dan/atau masukan.</p>';
          }
          $number_1 = rand(1, 9);
          $number_2 = rand(1, 9);
          $answer = substr(md5($number_1+$number_2),5,10);
        ?>
        <form id="contact" action="contact.php" method="post">
          <div class="form_settings">
            <p><span>Nama</span><input class="contact" type="text" name="your_name" value="<?phpphp echo $yourname; ?>" /></p>
            <p><span>Alamat Email</span><input class="contact" type="text" name="your_email" value="<?phpphp echo $youremail; ?>" /></p>
            <p><span>Pesan</span><textarea class="contact textarea" rows="5" cols="50" name="your_message"><?phpphp echo $yourmessage; ?></textarea></p>
            <p style="line-height: 1.7em;">Untuk mencegah email spam, harap isikan jawaban dari pertanyaan di bawah ini:</p>
            <p><span><?phpphp echo $number_1; ?> + <?phpphp echo $number_2; ?> = ?</span><input type="text" name="user_answer" /><input type="hidden" name="answer" value="<?phpphp echo $answer; ?>" /></p>
            <p style="padding-top: 15px"><span>&nbsp;</span><input class="submit" type="submit" name="contact_submitted" value="Kirim" /></p>
          </div>
        </form><br><br><br><br>
      </div>
    </div>
    <?php generate_footer(); ?>
  </div>
  <p>&nbsp;</p>
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript" src="js/image_fade.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
    });
  </script>
</body>
</html>
