<?php include_once("zz_koneksi_db.php"); ?>
<?php include ("zz_generate_menu.php"); ?>
<!DOCTYPE HTML>
<html>

<head>
  <title>.:: ePERDA - Kementerian Dalam Negeri ::.</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>
</head>

<body>
  <div id="main">
    <header>
      <?php generate_logo(); ?>
      <?php generate_menu(2); ?>
    </header>
    <div id="site_content">
      <div class="gallery"><?php generate_gallery($conn, 950, 150); ?></div>
      <div class="content">
        <h1>Metodologi</h1>
        <p><br>
        Perhitungan Nett rent lahan sawah dilakukan berdasarkan metode perhitungan sebagai berikut :<br>
        </p>
        <table width="100%" border="0" cellspacing="0" cellpadding="3">
          <tr>
            <td align="center"><img src="images/met_01.jpg" width="473" height="261"></td>
          </tr>
          <tr>
            <td align="center"> <strong><em>Gambar 1. Diagram alir perhitungan nilai produktifitas lahan sawah secara spasial</em></strong><em></em></td>
          </tr>
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="3">
          <tr>
            <td align="center"><img src="images/met_02.jpg" width="501" height="405"></td>
          </tr>
          <tr>
            <td align="center"><em><strong>Gambar 2. Diagram alir perhitungan nett rent lahan sawah</strong></em><strong></strong></td>
          </tr>
        </table>
        <p>Data yang diperlukan dalam model ini antara lain :<br>
        </p>
        <ul>
        <li>Nilai Produktifitas Segmen (dari sistem Kerangka Sample Area (KSA) - <a href="http://neonet.bppt.go.id/padi" target="_blank">http://neonet.bppt.go.id/padi</a>)</li>
            <li>Data spasial dasar kabupaten</li>
            <li>Data spasial Lahan Sawah</li>
            <li>Harga beras (Rp/Kg)</li>
            <li>Rasio proses dari padi ke beras (%) </li>
            <li>Biaya tanam (Rp/Kg)</li>
            <li>Frekuensi panen dalam setahun</li>
            <li>Faktor koreksi terhadap panen (%) bisa minus atau positif. Minus kalau misalnya mau disimulasikan ada hama wereng atau penyebab lain yang membuat hasil panen di bawah ekspektasi</li>
            <li>Biaya Tenaga Kerja per 1x panen (mulai menanam s/d panen) (perhatikan apakah biaya tenaga kerja sudah diperhitungkan di dalam biaya tanam) (Rp/Kg)</li>
            <li>Profit standar (%)</li>
            <li>Biaya transportasi (Rp) berdasarkan penggolongan jarak yang ditempuh</li>
        </ul>
        <p><em>Processing</em> menghasilkan :<br>
       	<ul>
          	<li>Peta Sebaran Produktifitas Lahan Sawah</li>
          	<li>Nilai biaya angkut per Kecamatan</li>
          	<li>Nilai Produksi per Kecamatan</li>
          	<li>Nilai luasan lahan baku sawah per Kecamatan</li>
          	<li>Nilai Produktifitas rata-rata per Kecamatan</li>
        	<li>Nilai Nett Rent</li>
         </ul>
            
            </p>
        <p><strong>Rumus yang digunakan</strong>:<br>
          c = penghasilan kotor dalam Rp/Ha<br>
          c 	= a (Ton/Ha)* 1000 * b (Rp/Ton)<br>
          <br>
          e = keuntungan bersih dalam Rp/Ha<br>
          e 	= c (Rp/Ha) - (a (Ton/Ha)* 1000 * d (Rp/Ton))<br>
          <br>
          f = biaya transportasi dalam Rp/Ha<br>
          <br>
          <strong>Nett Rent </strong>= e - f<br>
          di mana :<br>
          a = produktifitas dalam ton/Ha<br>
          d = biaya tanam dalam Rp/Kg<br>
          f = biaya transpor dalam Rp/Ton<br>
          b = harga beras dalam Rp/Kg<br>
          l = luas sawah dalam Ha<br>
        </p>
        <p></p>
        <p><br>
        </p>
      </div>
    </div>
    <?php generate_footer(); ?>
  </div>
  <p>&nbsp;</p>
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript" src="js/image_fade.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
    });
  </script>
</body>
</html>
