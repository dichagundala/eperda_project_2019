<?php include_once("zz_koneksi_db.php"); ?>
<?php
	require 'PHPMailer/PHPMailerAutoload.php';
	$host  = $_SERVER['HTTP_HOST'];
	$uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
	if( $_SESSION["sws_tingkat"] == 10 || $_SESSION["sws_tingkat"] == 11 ) {
	} else {
		$extra = "reg_login.php";
		header("Location: http://$host$uri/$extra");
		exit;
	}
	
	function uniqueFilename($strExt) {
		$arrIp = explode('.', $_SERVER['REMOTE_ADDR']);
		list($usec, $sec) = explode(' ', microtime());
		$usec = (integer) ($usec * 65536);
		$sec = ((integer) $sec) & 0xFFFF;
		$strUid = sprintf("%08x-%04x-%04x", ($arrIp[0] << 24) | ($arrIp[1] << 16) | ($arrIp[2] << 8) | $arrIp[3], $sec, $usec);
		// tack on the extension and return the filename
		return $strUid . $strExt;
	}
	
	function getToken($length)  {
		$validCharacters = "abcdefghjkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUXYVWZ123456789";
		$validCharNumber = strlen($validCharacters);
		$result = "";
		for ($i = 0; $i < $length; $i++) {
			$index = mt_rand(0, $validCharNumber - 1);
			$result .= $validCharacters[$index];
		}
		return $result;
	}
	
	$judul = addslashes($_POST["judul"]);
	$ringkasan = addslashes($_POST["nosurat"]);
	if( empty($_FILES['berkas']) ) {
		$extra = "reg_ranperda_fin.php?a=1";
		header("Location: http://$host$uri/$extra");
		exit;
	}
	if( empty($_FILES['berkas1']) ) {
		$ada_kepdprd = 0;
		$kepdprd_db = "";
		$kepdprd_a = "";
	} else {
		$ada_kepdprd = 1;
		$kepdprd_a = $_FILES['berkas1']['name'];
		$dum = explode(".",$kepdprd_a);
		if( count($dum) > 1 ) { $kepdprd_db = uniqueFilename(".".$dum[count($dum)-1]); } else { $kepdprd_db = uniqueFilename(""); }
		$uploaddir = "upload/".$kepdprd_db;
		if(move_uploaded_file($_FILES['berkas1']['tmp_name'], $uploaddir)) {
		} else {
			$ada_kepdprd = 0;
			$kepdprd_db = "";
			$kepdprd_a = "";
		}
	}
	
	if( empty($_FILES['berkas2']) ) {
		$ada_sp = 0;
		$sp_db = "";
		$sp_a = "";
	} else {
		$ada_sp = 1;
		$sp_a = $_FILES['berkas2']['name'];
		$dum = explode(".",$sp_a);
		if( count($dum) > 1 ) { $sp_db = uniqueFilename(".".$dum[count($dum)-1]); } else { $sp_db = uniqueFilename(""); }
		$uploaddir = "upload/".$sp_db;
		if(move_uploaded_file($_FILES['berkas2']['tmp_name'], $uploaddir)) {
		} else {
			$ada_sp = 0;
			$sp_db = "";
			$sp_a = "";
		}
	}
		
	$fname = $_FILES['berkas']['name'];
	$dum = explode(".",$fname);
	if( count($dum) > 1 ) { $fname_db = uniqueFilename(".".$dum[count($dum)-1]); } else { $fname_db = uniqueFilename(""); }
	$wkirim = date("Y-m-d H:i:s");
	$onama = addslashes($_SESSION["sws_nama_pengguna"]);
	$oid = $_SESSION["sws_id"];
	$prov = $_SESSION["sws_prov"];
	$kab = $_SESSION["sws_kab"];
	$kirimke = $_SESSION["sws_email"];
	$atoken = getToken(6);
	$uploaddir = "upload/".$fname_db;
	if(move_uploaded_file($_FILES['berkas']['tmp_name'], $uploaddir)) {
		$ayear = date("Y");
		$hsl = mysqli_query($conn, "select * from tbl_lastnoreg where thn='$ayear'");
		if( mysqli_num_rows($hsl) == 0 ) {
			$cnoreg = 1;
			mysqli_query($conn, "insert into tbl_lastnoreg VALUES (null, '$cnoreg', '$ayear')");
		} else {
			$B = mysqli_fetch_array($hsl);
			$cnoreg = $B[1] + 1;
			mysqli_query($conn, "update tbl_lastnoreg set lastnoreg='$cnoreg' where thn='$ayear'");
		}
		$tkt = $_SESSION["sws_tingkat"];
		
		$hsl = mysqli_query($conn, "insert into tbl_ranperda VALUES (null, '$judul', '$fname','$fname_db','$ringkasan', '$wkirim', '$prov', '$kab', '$onama', '$oid', '$atoken', '$kirimke', '$cnoreg', '$tkt', '1', '$ada_kepdprd', '$kepdprd_a', '$kepdprd_db', '$ada_sp', '$sp_a', '$sp_db', '0')");
		$newid = mysqli_insert_id($conn);
		$extra = "reg_ranperda_fin.php?a=99&b=$newid";
		header("Location: http://$host$uri/$extra");
		exit;
	} else {
		$extra = "reg_ranperda_fin.php?a=2";
		header("Location: http://$host$uri/$extra");
		exit;
	}
?>	