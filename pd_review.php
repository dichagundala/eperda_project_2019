<?php include_once("zz_koneksi_db.php"); ?>
<?php include ("zz_generate_menu.php"); ?>
<?php
	session_start();
	require('calendar/tc_calendar.php');
	$host  = $_SERVER['HTTP_HOST'];
	$uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
	$ip1 = $_SERVER['HTTP_X_FORWARDED_FOR'];
	$ip2 = $_SERVER['REMOTE_ADDR'];
	$waktu = date("Y-m-d H:i:s");
	$extra = "pd_login.php";
	if( $_SESSION["sws_id"] == "" || !isset($_SESSION["sws_id"]) || $_SESSION["sws_id"] == 0 ) {
		echo "<script>window.location.href=\"pd_login.php\";</script>";
		//header("Location: http://$host$uri/$extra");
		exit;
	}
	if( $_SESSION["sws_tingkat"] == 99 || $_SESSION["sws_tingkat"] == 1 || $_SESSION["sws_tingkat"] == 2 ) {
	} else {
		echo "<script>window.location.href=\"pd_login.php\";</script>";
		//header("Location: http://$host$uri/$extra");
		exit;
	}
	$anid = $_POST["id"];
	$hsl = mysqli_query($conn, "select * from tbl_ranperda where id='$anid'");
	if( mysqli_num_rows($hsl) == 0 ) {
		echo "<script>window.location.href=\"pd_login.php\";</script>";
		//header("Location: http://$host$uri/$extra");
		exit;
	}
	$B = mysqli_fetch_array($hsl);
	$token = $B["utoken"];
	$judul = $B["judul"];
	$ringkasan = str_replace (array("\r\n", "\n", "\r"), '<br>', $B["ringkasan"]);
	$wkirim = get_waktu($B["wkirim"]);
	$hsl1 = mysqli_query($conn, "select nama from tbl_prov where kode='".$B["prov"]."'");
	$B1 = mysqli_fetch_array($hsl1);
	$wilayah = $B1[0];
	if( $B["kabkota"] != 0 ) {
		$hsl1 = mysqli_query($conn, "select nama from tbl_kab where kode_kab='".$B["kabkota"]."'");
		$B1 = mysqli_fetch_array($hsl1);
		$wilayah .= " / ".$B1[0];
	}
	$hsl1 = mysqli_query($conn, "select count(id) from tbl_review where token='$token'");
	if( mysqli_num_rows($hsl1) == 0 ) {
		$jreview = 0;
	} else {
		$B1 = mysqli_fetch_array($hsl1);
		if( $B1[0] == "" || !isset($B1[0]) ) { $jreview = 0; } else { $jreview = $B1[0]; }
	}
	$ada1 = 1;
	$hsl1 = mysqli_query($conn, "select id, fa from tbl_review where token='$token' and adaberkas=1 ORDER BY id DESC LIMIT 1");
	if( mysqli_num_rows($hsl1) == 0 ) {
		//tidak pernah revisi
		$bj1 = 1;
		$bid1 = $anid;
		$nf1 = $B["nfile_asli"];
	} else {
		$bj1 = 2;
		$B1 = mysqli_fetch_array($hsl1);
		$bid1 = $B1[0];
		$nf1 = $B1["fa"];
	}
	$hsl1 = mysqli_query($conn, "select id, fa1 from tbl_review where token='$token' and ada1=1 ORDER BY id DESC LIMIT 1");
	if( mysqli_num_rows($hsl1) == 0 ) {
		if( $B["ada_kepdprd"] == 1 ) {
			$ada2 = 1;
			$bj2 = 1;
			$bid2 = $anid;
			$nf2 = $B["kepdprd_asli"];
		} else {
			$ada2 = 0;
			$nf2 = "Berkas belum ada";
		}
	} else {
		$ada2 = 1;
		$bj2 = 2;
		$B1 = mysqli_fetch_array($hsl1);
		$bid2 = $B1[0];
		$nf2 = $B1[1];
	}
	$hsl1 = mysqli_query($conn, "select id, fa2 from tbl_review where token='$token' and ada2=1 ORDER BY id DESC LIMIT 1");
	if( mysqli_num_rows($hsl1) == 0 ) {
		if( $B["ada_sp"] == 1 ) {
			$ada3 = 1;
			$bj3 = 1;
			$bid3 = $anid;
			$nf3 = $B["sp_asli"];
		} else {
			$ada3 = 0;
			$nf3 = "Berkas belum ada";
		}
	} else {
		$ada3 = 1;
		$bj3 = 2;
		$B1 = mysqli_fetch_array($hsl1);
		$bid3 = $B1[0];
		$nf3 = $B1[1];
	}
	
	$branperda = "<td colspan=\"2\" onClick=\"unduh(1,'$bid1', '$bj1');\" onMouseOver=\"this.style.cursor='pointer';\"><u>".$nf1."</u></td>";
	if( $ada2 == 1 ) {
		$bdprd = "<td colspan=\"2\" onClick=\"unduh(2,'$bid2', '$bj2');\" onMouseOver=\"this.style.cursor='pointer';\"><u>".$nf2."</u></td>";
	} else {
		$bdprd = "<td colspan=\"2\">Berkas belum ada</td>";
	}
	if( $ada3 == 1 ) {
		$bsp = "<td colspan=\"2\" onClick=\"unduh(3,'$bid3', '$bj3');\" onMouseOver=\"this.style.cursor='pointer';\"><u>".$nf3."</u></td>";
	} else {
		$bsp = "<td colspan=\"2\">Berkas belum ada</td>";
	}
	
	function get_waktu($a) {
		$dum = explode(" ",$a);
		$dum1 = explode("-", $dum[0]);
		$ostr = $dum1[2];
		if( $dum1[1] == "01" ) { $ostr .= " Januari ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "02" ) { $ostr .= " Februari ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "03" ) { $ostr .= " Maret ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "04" ) { $ostr .= " April ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "05" ) { $ostr .= " Mei ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "06" ) { $ostr .= " Juni ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "07" ) { $ostr .= " Juli ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "08" ) { $ostr .= " Agustus ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "09" ) { $ostr .= " September ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "10" ) { $ostr .= " Oktober ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "11" ) { $ostr .= " November ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "12" ) { $ostr .= " Desember ".$dum1[0]." ".$dum[1]; }
		
		$tnow = time();
		$tunduh = strtotime($a);
		$time = $tnow - $tunduh;
		$time = ($time < 1) ? 1 : $time;
		$tokens = array(
			31536000 => 'tahun',
			2592000  => 'bulan',
			604800   => 'minggu',
			86400    => 'hari',
			3600     => 'jam',
			60		 => 'menit',
			1		 => 'detik');
			
		foreach($tokens as $unit => $text ) {
			if( $time < $unit ) continue;
			$nou = floor($time / $unit);
			$et = $nou." ".$text;
			break;
		}
		return $ostr." (".$et." yang lalu)";
	}
?>


<!DOCTYPE HTML>
<html>

<head>
  <title>.:: ePERDA - Kementerian Dalam Negeri ::.</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0" />
<script src="tinymce/js/tinymce/tinymce.dev.js"></script>
<script src="tinymce/js/tinymce/plugins/table/plugin.dev.js"></script>
<script src="tinymce/js/tinymce/plugins/paste/plugin.dev.js"></script>
<script src="tinymce/js/tinymce/plugins/spellchecker/plugin.dev.js"></script>
<script src="tinymce/js/tinymce/plugins/codesample/plugin.dev.js"></script>
<script>
	tinymce.init({
		selector: "textarea#elm1",
		theme: "modern",
		plugins: [
			"advlist autolink autosave link image lists charmap print preview hr anchor pagebreak spellchecker",
			"searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
			"save table contextmenu directionality emoticons template textcolor paste fullpage textcolor colorpicker codesample"
		],
		external_plugins: {
			//"moxiemanager": "/moxiemanager-php/plugin.js"
		},
		content_css: "css/development.css",
		add_unload_trigger: false,
		autosave_ask_before_unload: false,

		toolbar1: "save newdocument fullpage | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
		toolbar2: "cut copy paste pastetext | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image media help code | insertdatetime preview | forecolor backcolor",
		toolbar3: "table | hr removeformat | subscript superscript | charmap emoticons | print fullscreen | ltr rtl | spellchecker | visualchars visualblocks nonbreaking template pagebreak restoredraft | insertfile insertimage codesample",
		menubar: false,
		toolbar_items_size: 'small',

		style_formats: [
			{title: 'Bold text', inline: 'b'},
			{title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
			{title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
			{title: 'Example 1', inline: 'span', classes: 'example1'},
			{title: 'Example 2', inline: 'span', classes: 'example2'},
			{title: 'Table styles'},
			{title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
		],

		templates: [
			{title: 'My template 1', description: 'Some fancy template 1', content: 'My html'},
			{title: 'My template 2', description: 'Some fancy template 2', url: 'development.html'}
		],

        spellchecker_callback: function(method, data, success) {
			if (method == "spellcheck") {
				var words = data.match(this.getWordCharPattern());
				var suggestions = {};

				for (var i = 0; i < words.length; i++) {
					suggestions[words[i]] = ["First", "second"];
				}

				success({words: suggestions, dictionary: true});
			}

			if (method == "addToDictionary") {
				success();
			}
		}
	});
</script>

<script language="javascript">
function unduh(b, a, c) {
	document.form_unduh.id.value = a;
	document.form_unduh.jenis.value = b;
	document.form_unduh.tabel.value = c;
	document.form_unduh.submit();
}

function go_history(a, b) {
	document.form_hist.id.value = a;
	document.form_hist.token.value = b;
	document.form_hist.submit();
}

function toggle_kepmen() {
	var aval = document.form1.sproses.value;
	if( aval == 3 ) {
		document.getElementById("kepmen1").style.display = "";
		document.getElementById("kepmen2").style.display = "";
		document.getElementById("kepmen3").style.display = "";
	} else {
		document.getElementById("kepmen1").style.display = "none";
		document.getElementById("kepmen2").style.display = "none";
		document.getElementById("kepmen3").style.display = "none";
	}
}

function checkit() {
	var errorMsg = "";
	var aval = document.form1.sproses.value;
	if( aval == 3 ) {
		if( document.form1.bkepmen.value == "" ) { errorMsg += "\n\t - Masukkan berkas Surat Keputusan Menteri dahulu";}
		if (document.form1.nkepmen.value =="" ){ errorMsg += "\n\t - Nomor Surat Keputusan Menteri belum diisi";}
	}
	if (errorMsg != ""){
		msg  = "___________________________________________________________________\n\n";
		msg += "Fasilitasi belum bisa dilakukan dikarenakan kesalahan berikut ini.\n";
		msg += "___________________________________________________________________\n\n";
		errorMsg += alert(msg + errorMsg + "\n\n");
		return false;
	}
		
	return true;
}

</script>  
  
</head>

<body>
  <div id="main">
    <header>
      <?php generate_logo(); ?>
      <?php generate_menu(5); ?>
    </header>
    <div id="site_content">
      <div class="gallery"><?php generate_gallery($conn, 950, 150); ?></div>
      <div class="content">
        <h1>Fasilitasi Rancangan PERDA</h1>
        <table width="100%" border="0" cellspacing="0" cellpadding="3" style="border-top:2px solid #000000; font-size:80%" >
          <tr>
            <td colspan="3"><h2><?php echo $judul; ?></h2></td>
          </tr>
          <tr>
            <td>Provinsi / KabKota</td>
            <td colspan="2"><?php echo $wilayah; ?></td>
          </tr>
          <tr valign="top">
            <td width="24%">Ringkasan</td>
            <td colspan="2"><?php echo $ringkasan; ?></td>
          </tr>
          <tr>
            <td>Pengunggahan Pertama</td>
            <td colspan="2"><?php echo $wkirim; ?></td>
          </tr>
          <tr>
            <td>Berkas Ranperda</td>
            <?php echo $branperda; ?>
          </tr>
          <tr>
            <td>Terakhir mengunggah</td>
            <td colspan="2"><?php echo $wkirim; ?></td>
          </tr>
          <tr>
            <td>Data history</td>
            <td width="11%" onClick="go_history('<?php echo $anid; ?>', '<?php echo $token; ?>');" onMouseOver="this.style.cursor='pointer';"><u><?php echo $jreview; ?></u></td>
            <td width="65%">&nbsp;</td>
          </tr>
        </table>
        <form action="pd_review_confirm.php" method="post" enctype="multipart/form-data" name="form1" onSubmit="return checkit();">
          <table width="100%" border="0" cellspacing="0" cellpadding="3" style="border-top:2px solid #000000; font-size:80%">
            <tr valign="top" id="kepmen1" style="display:none">
              <td width="20%">Nomor Kepmen</td>
              <td width="2%">&nbsp;</td>
              <td width="78%"><input type="text" name="nkepmen" id="nkepmen"></td>
            </tr>
            <tr valign="top" id="kepmen3" style="display:none">
              <td>Tgl Disahkan</td>
              <td>&nbsp;</td>
              <td>
<?php
		$dnow = date("Y-m-d");
		$tl = explode("-",$dnow);
	  $myCalendar = new tc_calendar("wkepmen", true, false);
	  $myCalendar->setPicture("calendar/images/iconCalendar.gif");
	  $myCalendar->setDate($tl[2],$tl[1],$tl[0]);
	  $myCalendar->setPath("calendar/");
	  $myCalendar->setYearSelect(1930, date('Y'));
	  $myCalendar->dateAllow('1930-01-01', date('Y-m-d',mktime(0, 0, 0, 31, 12, date("Y"))));
	  // VERY IMPORTANT: next line REQUIRED only for localized version!
	  $myCalendar->setDateFormat(str_replace("%","",str_replace("B","F",str_replace("d","j",L_CAL_FORMAT))));
	  $myCalendar->setAlignment('left', 'bottom');
	  //$myCalendar->autoSubmit(true, "form1");
	  $myCalendar->writeScript();        
?>              
              
              </td>
            </tr>
            <tr valign="top" id="kepmen2" style="display:none">
              <td>Unggah Kepmen</td>
              <td>&nbsp;</td>
              <td><input name="bkepmen" type="file" id="bkepmen" size="40" maxlength="40"></td>
            </tr>
            <tr valign="top">
              <td>Surat Fasilitasi</td>
              <td>&nbsp;</td>
              <td><input name="fasdirjen" type="file" id="fasdirjen" size="40" maxlength="40"></td>
            </tr>
            <tr valign="top">
              <td>Berkas Pendukung</td>
              <td>&nbsp;</td>
              <td><input name="bpend" type="file" id="bpend" size="40" maxlength="40"></td>
            </tr>
            <tr valign="top">
              <td>Feedback</td>
              <td>&nbsp;</td>
              <td>&nbsp;<input name="sproses" type="hidden" value="2"><input type="hidden" name="id" id="id" value="<?php echo $anid; ?>"><input type="hidden" name="token" id="token" value="<?php echo $token; ?>"></td>
            </tr>
            <tr>
              <td colspan="3"><textarea id="elm1" name="elm1" rows="15" style="width: 100%"></textarea></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td><input type="submit" name="button" id="button" value="Proses"></td>
            </tr>
          </table>
        </form>
        <br>
        <p>&nbsp;</p>
      </div>
    </div>
    <?php generate_footer(); ?>
  </div>
  <p>&nbsp;</p>
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript" src="js/image_fade.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
    });
  </script>
  
<form action="pd_man_user_edit.php" method="post" name="form_edit">
<input name="id" type="hidden" value="" />
</form>
<form action="pd_man_user_del.php" method="post" name="form_hapus">
<input name="id" type="hidden" value="" />
</form>
<form action="pd_unduh_01.php" method="post" name="form_unduh">
<input name="id" type="hidden" value="" />
<input name="jenis" type="hidden" value="" />
<input name="tabel" type="hidden" value="" />
</form>
<form action="pd_history.php" method="post" name="form_hist">
<input name="id" type="hidden" value="" />
<input name="token" type="hidden" value="" />
</form>
  
</body>
</html>
