AmCharts.loadJSON = function(url) {
	if (window.XMLHttpRequest) {
    // IE7+, Firefox, Chrome, Opera, Safari
    	var request = new XMLHttpRequest();
  	} else {
    // code for IE6, IE5
    	var request = new ActiveXObject('Microsoft.XMLHTTP');
  }
  request.open('GET', url, false);
  request.send();
  // parse adn return the output
  return eval(request.responseText);
  
};

var chartData1 = AmCharts.loadJSON('zz_get_perda.php');
var chart1 = AmCharts.makeChart("chart1div", {
	"type": "serial",
	"dataProvider": chartData1,
    "pathToImages": "http://cdn.amcharts.com/lib/3/images/",
    "categoryField": "prov",
	"categoryAxis": { "gridPosition": "start", "labelRotation": 45 },
	"chartCursor": {},
	"chartScrollbar": {},
	"trendLines": [],
	"legend": {
		"data": [{title:"Jumlah PERDA", color:"#721212"}]
	},
    "graphs": [ {
      "valueField": "jml",
	  "lineColor": "#000000",
      "bullet": "round",
      "bulletBorderColor": "#FFFFFF",
      "bulletBorderThickness": 1,
      "lineThickness ": 4
    }]
  } );