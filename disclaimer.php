<?php include_once("zz_koneksi_db.php"); ?>
<?php include ("zz_generate_menu.php"); ?>

<!DOCTYPE HTML>
<html>

<head>
  <title>.:: ePERDA - Kementerian Dalam Negeri ::.</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>
</head>

<body>
  <div id="main">
    <header>
      <?php generate_logo(); ?>
      <?php generate_menu(2); ?>
    </header>
    <div id="site_content">
      <div class="gallery"><?php generate_gallery($conn, 950, 150); ?></div>
      <div class="content">
        <h1>Disclaimer</h1>
        <p>ePERDA adalah produk dari Direktorat Produk Hukum Daerah - Direktorat Jenderal Otonomi Daerah. Kementerian Dalam Negeri.<br>
		Seluruh data dan informasi Rancangan Peraturan Daerah dan Nomor Register Rancangan Peraturan Daerah yang terdapat dalam basis data ePERDA hanya digunakan untuk kepentingan kalangan terbatas dan bukan merupakan konsumsi publik.<br>
		Segala macam cara untuk mendapatkan data dan informasi yang tersimpan dalam aplikasi ini, dalam segala bentuk (format) dan tersimpan di segala media, merupakan pelanggaran terhadap kerahasiaan data.<br>
		Kami tidak bertanggung  jawab bila terjadi serangan virus/malware/adware yang bisa jadi menimpa  perangkat komputer Anda ketika Anda mengakses ePerda dan/atau setelah Anda mengunduh/mengunggah data dan informasi dari situs ini.&#13;<br>
		</p>

        <div>Untuk informasi lebih  lanjut silakan hubungi <a href="pusdatinkomtel@kemendagri.go.id">pusdatinkomtel@kemendagri.go.id</a>&#13;<br>
          <br>
        </div>
      </div>
    </div>
    <?php generate_footer(); ?>
  </div>
  <p>&nbsp;</p>
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript" src="js/image_fade.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
    });
  </script>
</body>
</html>
