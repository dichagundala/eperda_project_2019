<?php include_once("zz_koneksi_db.php"); ?>
<?php include ("zz_generate_menu.php"); ?>
<?php
	$host  = $_SERVER['HTTP_HOST'];
	$uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
	$ip1 = $_SERVER['HTTP_X_FORWARDED_FOR'];
	$ip2 = $_SERVER['REMOTE_ADDR'];
	$waktu = date("Y-m-d H:i:s");
	$extra = "berita.php";
	if( $_GET["a"] == "" || !isset($_GET["a"]) ) {
		echo "<script>window.location.href=\"index.php\";</script>";
		//header("Location: http://$host$uri/$extra");
		exit;
	}
	$a = $_GET["a"];
	settype( $a, "int" );
	$hsl = mysqli_query($conn, "select * from berita where id='$a'");
	if( mysqli_num_rows($hsl) == 0 ) {
		echo "<script>window.location.href=\"index.php\";</script>";
		//header("Location: http://$host$uri/$extra");
		exit;
	}
	$B = mysqli_fetch_array($hsl);
?>

<!DOCTYPE HTML>
<html>

<head>
  <title>.:: ePERDA - Kementerian Dalam Negeri ::.</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>
</head>

<body>
  <div id="main">
    <header>
      <?php generate_logo(); ?>
      <?php generate_menu(1); ?>
    </header>
    <div id="site_content">
      <div class="gallery"><?php generate_gallery($conn, 950, 150); ?></div>
      <div class="content_webgis">
		  <h2><span><?php echo $B["judul"]; ?></span></h2>
          <p>Dikirim oleh <a href="#"><?php echo $B["oleh"]; ?></a> tanggal <?php echo sws_get_tgl($B["tgl"]); ?></p>
          <p><?php echo $B["isi"]; ?></p>
          <p>&nbsp;</p>
        <p><br>
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
        </p>


	 </div><br><br><br><br>
    </div>
    <?php generate_footer(); ?>
  </div>
  <p>&nbsp;</p>
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript" src="js/image_fade.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
    });
  </script>
</body>
</html>
