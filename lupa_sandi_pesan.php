<?php include_once("zz_koneksi_db.php"); ?>
<?php include ("zz_generate_menu.php"); ?>
<?php
	if( $_POST["stat"] == "" || !isset($_POST["stat"]) ) {
		$stat = 0;
	} else {
		$stat = $_POST["stat"];
		settype($stat,"int");
		if( $stat > 1 || $stat < 0 ) { $stat = 0; }
		if( $stat == 1 ) {
			$nl = $_POST["nl"];
			$email = $_POST["email"];
		}
	}
?>
		
		
<!DOCTYPE HTML>
<html>

<head>
  <title>.:: ePERDA - Kementerian Dalam Negeri ::.</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>
</head>

<body>
  <div id="main">
    <header>
	  <?php generate_logo(); ?>
      <?php generate_menu(7); ?>
    </header>
    <div id="site_content">
      <div class="gallery"><?php generate_gallery($conn, 950, 150); ?></div>
      <div class="content">
        <h1>Lupa Kata Sandi</h1>
<?php
	if( $stat == 1 ) {
?>		        
        <p>Bapak/Ibu <?php echo $nl; ?> Yth</p>
        <p>Sebuah email sudah dilayangkan ke alamat <?php echo $email; ?> Silakan ikuti tautan yang tertulis di dalam email tersebut untuk meneruskan proses reset kata sandi Bapak/Ibu.<br>
          <br>
          Jika Bapak/Ibu masih mengalami kendala dalam mengakses sistem, silakan layangkan pemberitahuan melalui formulir <a href="contact.php">feedback</a>.<br>
        </p>
        <p>Atas perhatiannya diucapkan terima kasih.<br>
        </p>
<?php
	} else {
?>		
        Bapak/Ibu Yth<br>
        <br>
        <p>Proses reset kata sandi tidak dapat dilakukan. Silakan periksa apakah kondisi-kondisi di bawah ini
        sudah terpenuhi:</p><br>
        <p>a. Nama login sudah betul</p>
        <p>b. Alamat email sudah betul</p>
        <p>c. Nama account sudah didaftarkan dan aktif</p>
        <br>
        <p>Jika Bapak/Ibu masih mengalami kendala dalam mengakses sistem, silakan layangkan pemberitahuan melalui formulir <a href="contact.php">feedback</a>.<br>
        </p>
        <p>Atas perhatiannya diucapkan terima kasih.</p>
        <br>
<?php
	}
?>
        <br>
<br>
<br><br><br>
      </div>
    </div>
    <?php generate_footer(); ?>
  </div>
  <p>&nbsp;</p>
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript" src="js/image_fade.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
    });
  </script>
</body>
</html>
