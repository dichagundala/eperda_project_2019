<?php include_once("zz_koneksi_db.php"); ?>
<?php
	$host  = $_SERVER['HTTP_HOST'];
	$uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
	if( $_SESSION["sws_tingkat"] == 10 || $_SESSION["sws_tingkat"] == 11 ) {
	} else {
		$extra = "reg_login.php";
		header("Location: http://$host$uri/$extra");
		exit;
	}
	
	function uniqueFilename($strExt) {
		$arrIp = explode('.', $_SERVER['REMOTE_ADDR']);
		list($usec, $sec) = explode(' ', microtime());
		$usec = (integer) ($usec * 65536);
		$sec = ((integer) $sec) & 0xFFFF;
		$strUid = sprintf("%08x-%04x-%04x", ($arrIp[0] << 24) | ($arrIp[1] << 16) | ($arrIp[2] << 8) | $arrIp[3], $sec, $usec);
		// tack on the extension and return the filename
		return $strUid . $strExt;
	}
	
	function getToken($length)  {
		$validCharacters = "abcdefghjkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUXYVWZ123456789";
		$validCharNumber = strlen($validCharacters);
		$result = "";
		for ($i = 0; $i < $length; $i++) {
			$index = mt_rand(0, $validCharNumber - 1);
			$result .= $validCharacters[$index];
		}
		return $result;
	}
	$judul = addslashes($_POST["judul"]);
	$jenis = $_POST["jenis"];
	if( empty($_FILES['berkas']) ) {
		$extra = "eregister_fin.php?a=1";
		header("Location: http://$host$uri/$extra");
		exit;
	} else {
		$adaberkas = 1;
		$berkas = $_FILES['berkas']['name'];
		$dum = explode(".",$berkas);
		if( count($dum) > 1 ) { $berkas_db = uniqueFilename(".".$dum[count($dum)-1]); } else { $berkas_db = uniqueFilename(""); }
		$uploaddir = "upload/".$berkas_db;
		if(move_uploaded_file($_FILES['berkas']['tmp_name'], $uploaddir)) {
		} else {
			$extra = "eregister_fin.php?a=1";
			header("Location: http://$host$uri/$extra");
			exit;
		}
	}
	$no_kepdprd = addslashes($_POST["no_kepdprd"]);
	$tgl_kepdprd = $_POST["tgl_kepdprd"];
	if( empty($_FILES['berkas1']) ) {
		$extra = "eregister_fin.php?a=2";
		header("Location: http://$host$uri/$extra");
		exit;
	} else {
		$adaberkas1 = 1;
		$berkas1 = $_FILES['berkas1']['name'];
		$dum = explode(".",$berkas1);
		if( count($dum) > 1 ) { $berkas1_db = uniqueFilename(".".$dum[count($dum)-1]); } else { $berkas1_db = uniqueFilename(""); }
		$uploaddir = "upload/".$berkas1_db;
		if(move_uploaded_file($_FILES['berkas1']['tmp_name'], $uploaddir)) {
		} else {
			$extra = "eregister_fin.php?a=2";
			header("Location: http://$host$uri/$extra");
			exit;
		}
	}
	$no_sp = addslashes($_POST["no_sp"]);
	$tgl_sp = $_POST["tgl_sp"];
	if( empty($_FILES['berkas2']) ) {
		$extra = "eregister_fin.php?a=3";
		header("Location: http://$host$uri/$extra");
		exit;
	} else {
		$adaberkas2 = 1;
		$berkas2 = $_FILES['berkas2']['name'];
		$dum = explode(".",$berkas2);
		if( count($dum) > 1 ) { $berkas2_db = uniqueFilename(".".$dum[count($dum)-1]); } else { $berkas2_db = uniqueFilename(""); }
		$uploaddir = "upload/".$berkas2_db;
		if(move_uploaded_file($_FILES['berkas2']['tmp_name'], $uploaddir)) {
		} else {
			$extra = "eregister_fin.php?a=3";
			header("Location: http://$host$uri/$extra");
			exit;
		}
	}
	$no_kepmen = addslashes($_POST["no_kepmen"]);
	$tgl_kepmen = $_POST["tgl_kepmen"];
	if( empty($_FILES['berkas3']) ) {
		if( $jenis == 1 ) {
			$extra = "eregister_fin.php?a=4";
			header("Location: http://$host$uri/$extra");
			exit;
		} else {
			$adaberkas3 = 0;
			$berkas3 = "";
			$berkas3_db = "";
		}
	} else {
		$adaberkas3 = 1;
		$berkas3 = $_FILES['berkas3']['name'];
		$dum = explode(".",$berkas3);
		if( count($dum) > 1 ) { $berkas3_db = uniqueFilename(".".$dum[count($dum)-1]); } else { $berkas3_db = uniqueFilename(""); }
		$uploaddir = "upload/".$berkas3_db;
		if(move_uploaded_file($_FILES['berkas3']['tmp_name'], $uploaddir)) {
		} else {
			if( $jenis == 1 ) {
				$extra = "eregister_fin.php?a=4";
				header("Location: http://$host$uri/$extra");
				exit;
			} else {
				$adaberkas3 = 0;
				$berkas3 = "";
				$berkas3_db = "";
			}
		}
	}
	$ringkasan = addslashes($_POST["ringkasan"]);
	$klas = $_POST["klas"];
	
	$wkirim = date("Y-m-d H:i:s");
	$onama = addslashes($_SESSION["sws_nama_pengguna"]);
	$oid = $_SESSION["sws_id"];
	$prov = $_SESSION["sws_prov"];
	$kab = $_SESSION["sws_kab"];
	$tkt = $_SESSION["sws_tingkat"];
	$kirimke = $_SESSION["sws_email"];
	$atoken = getToken(6);
	
	$hsl = mysqli_query($conn, "select nama from tbl_klasifikasi where id='$klas'");
	$B = mysqli_fetch_array($hsl);
	$aklas = addslashes($B[0]);
	
	$hsl = mysqli_query($conn, "select max(nprov) from tbl_reg_ranperda");
	if( mysqli_num_rows($hsl) == 0 ) {
		$nprov = 1;
	} else {
		$B = mysqli_fetch_array($hsl);
		if( $B[0] == "" || !isset($B[0]) ) { $nprov = 1; } else { $nprov = $B[0] + 1; }
	}
	$hsl = mysqli_query($conn, "insert into tbl_reg_ranperda VALUES (null, '$judul', '$berkas', '$berkas_db', '$ringkasan','$wkirim', '$prov', '$kab', '$onama', '$oid', '$atoken', '$kirimke', '0', '$tkt', '1', '$adaberkas1', '$no_kepdprd', '$tgl_kepdprd', '$berkas1', '$berkas1_db', '$adaberkas2', '$no_sp', '$tgl_sp', '$berkas2', '$berkas2_db', '$adaberkas3', '$no_kepmen', '$tgl_kepmen', '$berkas3', '$berkas3_db', '$jenis', '0', '', '0000-00-00', '', '', '0', '0000-00-00', '0000-00-00', '', '$aklas', '$nprov', '0', '0', '0', '', '', '')");
		//echo "insert into tbl_review VALUES (null, '$token', '$ringkasan','$wkirim','$oid', '$onama', '1', '$kirimke', '1', '$fname', '$fname_db')";
	$newid = mysqli_insert_id($conn);
	$extra = "eregister_fin.php?a=99&b=$newid";
	header("Location: http://$host$uri/$extra");
	exit;
?>	