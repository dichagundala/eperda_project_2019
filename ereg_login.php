<?php include_once("zz_koneksi_db.php"); ?>
<?php include ("zz_generate_menu.php"); ?>
<!DOCTYPE HTML>
<html>

<head>
  <title>.:: ePERDA - Kementerian Dalam Negeri ::.</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>
</head>

<body>
  <div id="main">
    <header>
	  <?php generate_logo(); ?>
      <?php generate_menu(7); ?>
    </header>
    <div id="site_content">
      <div class="gallery"><?php generate_gallery($conn, 950, 150); ?></div>
      <div class="content">
        <h1>e-Register</h1><p>Anda belum Log-in. Silakan isi formulir di bawah ini:
        </p>
        <form id="frm_login" action="ereg_login_confirm.php" method="post">
          <div class="form_settings">
            <p><span>Nama</span>
              <input name="nama" type="text" class="contact" id="nama" /><input type="hidden" name="sc" value="reg_ranperda" />
            </p>
            <p><span>Kata Sandi</span>
              <input name="pwd" type="password" class="contact" id="pwd" />
            </p>
            <p>&nbsp;</p>
            <p style="padding-top: 15px"><span>&nbsp;</span>
              <input class="submit" type="submit" name="Submit" value="Masuk" />
            </p>
          </div>
        </form>

<br>
Lupa dengan kata sandi? Silakan klik <a href="lupa_sandi.php">disini</a><br><br><br>
      </div>
    </div>
    <?php generate_footer(); ?>
  </div>
  <p>&nbsp;</p>
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript" src="js/image_fade.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
    });
  </script>
</body>
</html>
