<?php include_once("zz_koneksi_db.php"); ?>
<?php include ("zz_generate_menu.php"); ?>
<!DOCTYPE HTML>
<html>

<head>
  <title>.:: ePERDA - Kementerian Dalam Negeri ::.</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <link rel="stylesheet" type="text/css" href="yoxview/yoxview.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>
    <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript" src="js/image_fade.js"></script>
  <script type="text/javascript" src="yoxview/yoxview-init.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
    });
  </script>
  
  <script>
  	function sws_expand(a, b, c) {
		for( i = 0; i < b; i++ ) {
			document.getElementById("tbl_" + i).style.display = "none";
		}
		var divid = "tbl_" + c;
		var astyle = document.getElementById(divid).style.display;
		if( astyle == "none" ) {
			document.getElementById(divid).style.display = "";
		} else {
			document.getElementById(divid).style.display = "none";
			return;
		}
		
		document.getElementById(divid).innerHTML = "";
	
		url = "zz_getfoto.php?a=" + a;
		var url_ajax=url;
		if (window.XMLHttpRequest) { // Mozilla, Safari,...
			http_temp1 = new XMLHttpRequest();
			if (http_temp1.overrideMimeType) {
				http_temp1.overrideMimeType('text/xml');
			}
		} else if (window.ActiveXObject) { // IE
			try {
				http_temp1 = new ActiveXObject("Msxml2.XMLHTTP");	
			} catch (e) {
				try {
					http_temp1 = new ActiveXObject("Microsoft.XMLHTTP");
				} catch (e) {}
			}
		}
		if (!http_temp1) {
			alert('Browser tidak mendukung');
			return false;
		}
		
		http_temp1.onreadystatechange=function() {
			if (http_temp1.readyState == 4) {
				if (http_temp1.status == 200) {
					var dataku= http_temp1.responseText;
					//parseScript(dataku);
					document.getElementById(divid).style="";
					document.getElementById(divid).innerHTML=dataku;
					//parseScript(isiparse.innerHTML);
				} else {
					var dataku='Gagal dalam mengambil content';
					document.getElementById(divid).innerHTML=dataku;
					$("#yoxview").yoxview({
					lang: "ja",
					backgroundColor: "#000099",
					autoPlay: true
				});
				}
			}
		}
		http_temp1.open('GET', url_ajax, true);
		http_temp1.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		http_temp1.send(null);
		
	}
  </script>
  
<style type="text/css">
.sws_caption {
	font-size: small;
}
</style>
</head>

<body>
  <div id="main">
    <header>
      <?php generate_logo(); ?>
      <?php generate_menu(2); ?>
    </header>
    <div id="site_content">
      <div class="gallery"><?php generate_gallery($conn, 950, 150); ?></div>
      <div class="content">
        <h1>Gallery Foto Kegiatan</h1>
        <p><br>
          <table width="100%" border="0" cellspacing="0" cellpadding="3">
<?php
	$urut = 0;
	$hsl = mysqli_query($conn, "select * from tbl_foto_topik");
	while( $B = mysqli_fetch_array($hsl) ) {
		$id[$urut] = $B[0];
		$ntopik[$urut] = $B[1];
		$dtopik[$urut] = $B[2];
		$urut += 1;
	}
	
	for( $iix = 0; $iix < $urut; $iix++ ) {
?>		          
            <tr>
              <td colspan="2" onClick="sws_expand('<?php echo $id[$iix]; ?>','<?php echo $urut; ?>', '<?php echo $iix; ?>');" onMouseOver="this.style.cursor='pointer';"><strong><?php echo $ntopik[$iix]; ?></strong></td>
            </tr>
            <tr>
              <td colspan="2"><?php echo $dtopik[$iix]; ?></td>
            </tr>
            <tr id="tbl_<?php echo $iix; ?>" style="display:none">
              <td width="6%">&nbsp;</td>
              <td width="94%">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="2"><hr></td>
            </tr>
            
<?php
	}
?>
          </table>
          <p><br>
          <br>
          <br>
        </p>
</div>
    </div>
    <?php generate_footer(); ?>
  </div>
  <p>&nbsp;</p>
</body>
</html>
