<?php include_once("zz_koneksi_db.php"); ?>
<?php include ("zz_generate_menu.php"); ?>
<?php
	if( $_SESSION["sws_daerah"] == "" || !isset($_SESSION["sws_daerah"]) ) {
		echo "<script>window.location.href = \"reg_login.php\";</script>";
	}
	function get_waktu($a) {
		$dum = explode(" ",$a);
		$dum1 = explode("-", $dum[0]);
		$ostr = $dum1[2];
		if( $dum1[1] == "01" ) { $ostr .= " Januari ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "02" ) { $ostr .= " Februari ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "03" ) { $ostr .= " Maret ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "04" ) { $ostr .= " April ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "05" ) { $ostr .= " Mei ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "06" ) { $ostr .= " Juni ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "07" ) { $ostr .= " Juli ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "08" ) { $ostr .= " Agustus ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "09" ) { $ostr .= " September ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "10" ) { $ostr .= " Oktober ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "11" ) { $ostr .= " November ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "12" ) { $ostr .= " Desember ".$dum1[0]." ".$dum[1]; }
		
		$tnow = time();
		$tunduh = strtotime($a);
		$time = $tnow - $tunduh;
		$time = ($time < 1) ? 1 : $time;
		$tokens = array(
			31536000 => 'tahun',
			2592000  => 'bulan',
			604800   => 'minggu',
			86400    => 'hari',
			3600     => 'jam',
			60		 => 'menit',
			1		 => 'detik');
			
		foreach($tokens as $unit => $text ) {
			if( $time < $unit ) continue;
			$nou = floor($time / $unit);
			$et = $nou." ".$text;
			break;
		}
		return $ostr." (".$et." yang lalu)";
	}
	function get_waktu1($a) {
		$dum = explode(" ",$a);
		$dum1 = explode("-", $dum[0]);
		$ostr = $dum1[2];
		if( $dum1[1] == "01" ) { $ostr .= " Januari ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "02" ) { $ostr .= " Februari ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "03" ) { $ostr .= " Maret ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "04" ) { $ostr .= " April ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "05" ) { $ostr .= " Mei ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "06" ) { $ostr .= " Juni ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "07" ) { $ostr .= " Juli ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "08" ) { $ostr .= " Agustus ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "09" ) { $ostr .= " September ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "10" ) { $ostr .= " Oktober ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "11" ) { $ostr .= " November ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "12" ) { $ostr .= " Desember ".$dum1[0]." ".$dum[1]; }
		
		return $ostr;
	}
	
	if( $_POST["contact_submitted"] ) {
		$ada = true;
		$token = addslashes($_POST["token"]);
		$astr = "select * from tbl_reg_ranperda where utoken='$token' and prov='".$_SESSION["sws_prov"]."'";
		if( $_SESSION["sws_tingkat"] == 11 ) { $astr .= " and kabkota='".$_SESSION["sws_kab"]."'"; }
		$hsl = mysqli_query($conn, $astr);
		if( mysqli_num_rows($hsl) == 0 ) {
			$ditemukan = false;
		} else {
			$ditemukan = true;
			$B = mysqli_fetch_array($hsl);
			$anid = $B[0];
			for( $iix = 0; $iix < 14; $iix++ ) { $dawal[$iix] = $B[$iix];}
			$hsl1 = mysqli_query($conn, "select nama from tbl_prov where kode='".$B["prov"]."'");
			$B1 = mysqli_fetch_array($hsl1);
			$wilayah = $B1[0];
			if( $B["kabkota"] != 0 ) {
				$hsl1 = mysqli_query($conn, "select nama from tbl_kab where kode_kab='".$B["kabkota"]."'");
				$B1 = mysqli_fetch_array($hsl1);
				$wilayah .= " / ".$B1[0];
			}
			$ringkasan = str_replace (array("\r\n", "\n", "\r"), '<br>', $dawal[4]);
			if( $B["sproses"] == 1 ) { $astat = "<font color=\"#FF0000\"><strong>Menunggu diproses</strong></font>"; }
			if( $B["sproses"] == 2 ) { $astat = "<font color=\"#000033\">Sedang diproses</font>"; }
			if( $B["sproses"] == 3 ) { $astat = "<font color=\"#00CC00\">Sudah selesai</font>"; }
			$klas = $B["klasifikasi"];
			if( $B["nfile_db"] == "" || !isset($B["nfile_db"]) ) {
				$basli = "Tidak ada";
			} else {
				$basli = "<div onClick=\"unduh(1,$anid);\" onMouseOver=\"this.style.cursor='pointer';\"><u>".$B["nfile_asli"]."</u></div>";
			}
			if( $B["kepdprd_db"] == "" || !isset($B["kepdprd_db"]) ) {
				$bdprd = "Tidak ada";
			} else {
				$bdprd = "<div onClick=\"unduh(2,$anid);\" onMouseOver=\"this.style.cursor='pointer';\"><u>".$B["kepdprd_asli"]."</u></div>";
			}
			if( $B["sp_db"] == "" || !isset($B["sp_db"]) ) {
				$bsp = "Tidak ada";
			} else {
				$bsp = "<div onClick=\"unduh(3,$anid);\" onMouseOver=\"this.style.cursor='pointer';\"><u>".$B["sp_asli"]."</u></div>";
			}
			if( $B["kepmen_db"] == "" || !isset($B["kepmen_db"]) ) {
				$bkepmen = "Tidak ada";
			} else {
				$bkepmen = "<div onClick=\"unduh(4,$anid);\" onMouseOver=\"this.style.cursor='pointer';\"><u>".$B["kepmen_asli"]."</u></div>";
			}
			if( $B["skreg_db"] == "" || !isset($B["skreg_db"]) ) {
				$bsr = "Tidak ada";
			} else {
				$bsr = "<div onClick=\"unduh(5,$anid);\" onMouseOver=\"this.style.cursor='pointer';\"><u>".$B["skreg_asli"]."</u></div>";
			}
			if( $B["btamb_db"] == "" || !isset($B["btamb_db"]) ) {
				$btamb = "Tidak ada";
			} else {
				$btamb = "<div onClick=\"unduh(6,$anid);\" onMouseOver=\"this.style.cursor='pointer';\"><u>".$B["btamb_asli"]."</u></div>";
			}
		}
	} else {
		$ada = false;
		$token = "";
	}
?>
<!DOCTYPE HTML>
<html>

<head>
  <title>.:: ePERDA - Kementerian Dalam Negeri ::.</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>
  
  <script>
  	function unduh(a, b) {
		document.form_unduh.a.value = a;
		document.form_unduh.b.value = b;
		document.form_unduh.submit();	
	}
  
  	function checkit() {
		var errorMsg = "";
	
		//Check for a username
		if (document.form1.token.value==""){ errorMsg += "\n\t - Isi token dahulu";}
		if (errorMsg != ""){
			msg  = "___________________________________________________________________\n\n";
			msg += "Pencarian data belum bisa dilakukan dikarenakan kesalahan berikut ini.\n";
			msg += "___________________________________________________________________\n\n";
			errorMsg += alert(msg + errorMsg + "\n\n");
			return false;
		}
		
		return true;
	}
  </script>
</head>

<body>
  <div id="main">
    <header>
	  <?php generate_logo(); ?>
      <?php generate_menu(7); ?>
    </header>
    <div id="site_content">
      <div class="gallery"><?php generate_gallery($conn, 950, 150); ?></div>
      <div class="content">
        <h1>Pemantauan Register Perda</h1>
        <p>Silakan mengisikan kode token di bawah ini untuk mencari status pengajuan Rancangan PERDA.<br>
          <br>
        </p>
        <form id="contact" name="form1" action="tl_eregister.php" method="post" onsubmit="return checkit();">
          <div class="form_settings">
            <p><span>Token</span>
            <input name="token" type="text" class="contact" id="token" value="<?php echo $token; ?>" placeholder="Isikan token..." /></p>
            <p style="padding-top: 15px"><span>&nbsp;</span><input class="submit" type="submit" name="contact_submitted" value="Proses" /></p>
          </div>
        </form><br><br>
<?php
	if( $ada ) {
		if( !$ditemukan ) {
?>			
      <table width="100%" border="0" cellspacing="0" cellpadding="3">
          <tr>
            <td align="center"><strong>Kode token tidak valid</strong></td>
          </tr>
        </table>
<?php
		} else {
?>			
        <strong><?php echo $dawal[1]; ?></strong>
			<table width="100%" border="0" cellpadding="10" cellspacing="0" class="sws_table" style="border-top:2px solid #999">
              <tr>
                <td width="30%">Provinsi / KabKota</td>
                <td width="70%"><?php echo $wilayah; ?></td>
              </tr>
              <tr valign="top" style="border-top:2px solid #999">
                <td>Klasifikasi</td>
                <td><?php echo $klas; ?></td>
              </tr>
              <tr valign="top">
                <td>Ringkasan</td>
                <td><?php echo $ringkasan; ?></td>
              </tr>
              <tr>
                <td>Diunggah Pertama Kali</td>
                <td><?php echo get_waktu($dawal[5]); ?></td>
              </tr>
              <tr>
                <td valign="top">Status Terakhir</td>
                <td valign="top"><?php echo $astat; ?></td>
              </tr>
              <tr>
                <td valign="top">Berkas Ranperda</td>
                <td valign="top"><?php echo $basli; ?></td>
              </tr>
              <tr>
                <td valign="top">Berkas SK DPRD</td>
                <td valign="top"><?php echo $bdprd; ?></td>
              </tr>
              <tr>
                <td valign="top">Berkas Surat Permohonan</td>
                <td valign="top"><?php echo $bsp; ?></td>
              </tr>
              <tr>
                <td valign="top">Berkas Keputusan Menteri</td>
                <td valign="top"><?php echo $bkepmen; ?></td>
              </tr>
            </table>
            
        <table width="100%" border="0" cellspacing="0" cellpadding="3">
          <tr>
            <td>
            <br>
<?php
			if( $B["sproses"] >= 2 ) {            
?>            
            <h2>TINDAK LANJUT</h2><br>
<?php
				if( $B["sproses"] == 2 ) {
					//baru disposisi
?>
			<table width="100%" border="0" cellspacing="0" cellpadding="3" style="border-top:2px solid #999">
  <tr>
    <td width="30%">Tgl. Disposisi</td>
    <td width="70%"><?php echo get_waktu1($B["tgl_disposisi"]); ?></td>
  </tr>
</table>
<?php
				} else {
?>				
			<table class="sws_table" width="100%" border="0" cellspacing="0" cellpadding="3" style="border-top:2px solid #999">
              <tr valign="top">
                <td>No. Register</td>
                <td colspan="2"><?php echo $B["noreg"]; ?></td>
              </tr>
              <tr valign="top">
                <td>Tahun</td>
                <td colspan="2"><?php echo $B["tahun"]; ?></td>
              </tr>
              <tr valign="top">
                <td>Tgl. Surat Register</td>
                <td colspan="2"><?php echo get_waktu($B["tgl_skreg"]); ?></td>
              </tr>
              <tr valign="top">
                <td>No. Surat Register</td>
                <td colspan="2"><?php echo $B["no_skreg"]; ?></td>
              </tr>
              <tr valign="top">
                <td>Berkas Surat Register</td>
                <td colspan="2"><?php echo $bsr; ?></td>
              </tr>
              <tr valign="top">
                <td>Berkas Tambahan</td>
                <td colspan="2"><?php echo $btamb; ?></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td width="49%">&nbsp;</td>
                <td width="26%">&nbsp;</td>
              </tr>
            </table>
<?php
				}
			}
?>
            </td>
          </tr>
        </table>

<?php
		}
	}
?>
        <br><br>
      </div>
    </div>
    <?php generate_footer(); ?>
  </div>
  <p>&nbsp;</p>
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript" src="js/image_fade.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
    });
  </script>
  
  <form action="tl_eregister_unduh.php" method="post" name="form_unduh">
  <input name="a" type="hidden" value=""><input name="b" type="hidden" value="">
  </form>
</body>
</html>
