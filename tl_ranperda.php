<?php include_once("zz_koneksi_db.php"); ?>
<?php include ("zz_generate_menu.php"); ?>
<?php
	if(!isset($_SESSION["sws_daerah"]) ) {
		echo "<script>window.location.href = \"reg_login.php\";</script>";
	}
	function get_waktu($a) {
		$dum = explode(" ",$a);
		$dum1 = explode("-", $dum[0]);
		$ostr = $dum1[2];
		if( $dum1[1] == "01" ) { $ostr .= " Januari ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "02" ) { $ostr .= " Februari ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "03" ) { $ostr .= " Maret ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "04" ) { $ostr .= " April ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "05" ) { $ostr .= " Mei ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "06" ) { $ostr .= " Juni ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "07" ) { $ostr .= " Juli ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "08" ) { $ostr .= " Agustus ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "09" ) { $ostr .= " September ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "10" ) { $ostr .= " Oktober ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "11" ) { $ostr .= " November ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "12" ) { $ostr .= " Desember ".$dum1[0]." ".$dum[1]; }
		
		$tnow = time();
		$tunduh = strtotime($a);
		$time = $tnow - $tunduh;
		$time = ($time < 1) ? 1 : $time;
		$tokens = array(
			31536000 => 'tahun',
			2592000  => 'bulan',
			604800   => 'minggu',
			86400    => 'hari',
			3600     => 'jam',
			60		 => 'menit',
			1		 => 'detik');
			
		foreach($tokens as $unit => $text ) {
			if( $time < $unit ) continue;
			$nou = floor($time / $unit);
			$et = $nou." ".$text;
			break;
		}
		return $ostr." (".$et." yang lalu)";
	}
	
	if( isset($_POST["contact_submitted"]) ) {
		$ada = true;
		$token = addslashes($_POST["token"]);
		$astr = "select * from tbl_ranperda where utoken='$token' and prov='".$_SESSION["sws_prov"]."'";
		if( $_SESSION["sws_tingkat"] == 11 ) { $astr .= " and kabkota='".$_SESSION["sws_kab"]."'"; }
		$hsl = mysqli_query($conn, $astr);
		if( mysqli_num_rows($hsl) == 0 ) {
			$ditemukan = false;
		} else {
			$ditemukan = true;
			$B = mysqli_fetch_array($hsl);
			
			for( $iix = 0; $iix < 14; $iix++ ) {
				$dawal[$iix] = $B[$iix];
			}
			$hsl1 = mysqli_query($conn, "select nama from tbl_prov where kode='".$B["prov"]."'");
			$B1 = mysqli_fetch_array($hsl1);
			$wilayah = $B1[0];
			if( $B["kabkota"] != 0 ) {
				$hsl1 = mysqli_query($conn, "select nama from tbl_kab where kode_kab='".$B["kabkota"]."'");
				$B1 = mysqli_fetch_array($hsl1);
				$wilayah .= " / ".$B1[0];
			}
			$ringkasan = str_replace (array("\r\n", "\n", "\r"), '<br>', $dawal[4]);
			if( $B["sproses"] == 1 ) { $astat = "<font color=\"#FF0000\"><strong>Menunggu fasilitasi</strong></font>"; }
			if( $B["sproses"] == 2 ) { $astat = "<font color=\"#000033\">Tindak lanjut</font>"; }
			if( $B["sproses"] == 3 ) { $astat = "<font color=\"#00CC00\">Sudah disetujui</font>"; }
			$hsl = mysqli_query($conn, "select * from tbl_review where token='$token' order by waktu ASC");
		}
	} else {
		$ada = false;
		$token = "";
	}
?>
<!DOCTYPE HTML>
<html>

<head>
  <title>.:: ePERDA - Kementerian Dalam Negeri ::.</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>
  
  <script>
  	function unduh(a, b) {
		document.form_unduh.a.value = a;
		document.form_unduh.b.value = b;
		document.form_unduh.submit();	
	}
  
  	function checkit() {
		var errorMsg = "";
	
		//Check for a username
		if (document.form1.token.value==""){ errorMsg += "\n\t - Isi token dahulu";}
		if (errorMsg != ""){
			msg  = "___________________________________________________________________\n\n";
			msg += "Pencarian data belum bisa dilakukan dikarenakan kesalahan berikut ini.\n";
			msg += "___________________________________________________________________\n\n";
			errorMsg += alert(msg + errorMsg + "\n\n");
			return false;
		}
		
		return true;
	}
  </script>
</head>

<body>
  <div id="main">
    <header>
	  <?php generate_logo(); ?>
      <?php generate_menu(7); ?>
    </header>
    <div id="site_content">
      <div class="gallery"><?php generate_gallery($conn, 950, 150); ?></div>
      <div class="content">
        <h1>Pemantauan Rancangan Perda</h1>
        <p>Silakan mengisikan kode token di bawah ini untuk mencari status pengajuan Rancangan PERDA.<br>
          <br>
        </p>
        <form id="contact" name="form1" action="tl_ranperda.php" method="post" onsubmit="return checkit();">
          <div class="form_settings">
            <p><span>Token</span>
            <input name="token" type="text" class="contact" id="token" value="<?php echo $token; ?>" placeholder="Isikan token..." /></p>
            <p style="padding-top: 15px"><span>&nbsp;</span><input class="submit" type="submit" name="contact_submitted" value="Proses" /></p>
          </div>
        </form><br><br>
<?php
	if( $ada ) {
		if( !$ditemukan ) {
?>			
      <table width="100%" border="0" cellspacing="0" cellpadding="3">
          <tr>
            <td align="center"><strong>Kode token tidak valid</strong></td>
          </tr>
        </table>
<?php
		} else {
?>			
        
        <table width="100%" border="0" cellspacing="0" cellpadding="3">
          <tr>
            <td><table class="sws_table" width="100%" border="0" cellspacing="0" cellpadding="3" style="border-top:2px solid #999">
              <tr>
                <td width="29%">Judul Rancangan PERDA</td>
                <td width="71%"><strong><?php echo $dawal[1]; ?></strong></td>
              </tr>
              <tr>
                <td>Provinsi / KabKota</td>
                <td><?php echo $wilayah; ?></td>
              </tr>
              <tr valign="top">
                <td>Ringkasan</td>
                <td><?php echo $ringkasan; ?></td>
              </tr>
              <tr>
                <td>Diunggah Pertama Kali</td>
                <td><?php echo get_waktu($dawal[5]); ?></td>
              </tr>
              <tr>
                <td>Status Terakhir</td>
                <td><?php echo $astat; ?></td>
              </tr>
              <tr>
                <td onClick="unduh(1,<?php echo $dawal[0]; ?>);">Unduh Berkas Asal</td>
                <td>&nbsp;</td>
              </tr>
            </table>
            <br>
            <h2>TINDAK LANJUT</h2><br>
<?php
			if( mysqli_num_rows($hsl) == 0 ) {
?>
			Belum ada
<?php
			} else {
				while( $B = mysqli_fetch_array($hsl) ) {
					$ringkasan = str_replace (array("\r\n", "\n", "\r"), '<br>', $B["isi"]);
					if( $B["sproses"] == 1 ) { $astat = "<font color=\"#FF0000\"><strong>Menunggu fasilitasi</strong></font>"; }
					if( $B["sproses"] == 2 ) { $astat = "<font color=\"#000033\">Sudah difasilitasi</font>"; }
					if( $B["sproses"] == 3 ) { $astat = "<font color=\"#00CC00\">Sudah disetujui</font>"; }
					if( $B["adafb1"] == 1 ) { $bsp = "<td onClick=\"unduh(2,".$B[0].");\">Unduh Surat Pengantar</td>"; } else { $bsp = "<td>&nbsp;</td>"; }
					if( $B["adafb2"] == 1 ) { $bfb = "<td width=\"40%\" onClick=\"unduh(3,".$B[0].");\">Unduh Berkas Feedback</td>"; } else { $bfb = "<td>&nbsp;</td>"; }
					
?>				
			<table class="sws_table" width="100%" border="0" cellspacing="0" cellpadding="3" style="border-top:2px solid #999">
              <tr valign="top">
                <td width="29%">Isi Pesan</td>
                <td colspan="2"><?php echo $ringkasan; ?></td>
              </tr>
              <tr valign="top">
                <td>Status Proses</td>
                <td colspan="2"><?php echo $astat; ?></td>
              </tr>
              <tr>
                <td>Waktu Proses</td>
                <td colspan="2"><?php echo get_waktu($B["waktu"]); ?></td>
              </tr>
              <tr>
              	<?php  echo $bsp; ?><?php echo $bfb; ?>
                <td width="48%">&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
            </table>
<?php
				}
			}
?>
            </td>
          </tr>
        </table>

<?php
		}
	}
?>
        <br><br>
      </div>
    </div>
    <?php generate_footer(); ?>
  </div>
  <p>&nbsp;</p>
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript" src="js/image_fade.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
    });
  </script>
  
  <form action="tl_ranperda_unduh.php" method="post" name="form_unduh">
  <input name="a" type="hidden" value=""><input name="b" type="hidden" value="">
  </form>
</body>
</html>
