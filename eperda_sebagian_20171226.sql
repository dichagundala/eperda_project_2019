/*
Navicat MySQL Data Transfer

Source Server         : eperda-chat.com Induk
Source Server Version : 50505
Source Host           : eperda-chat.com:3306
Source Database       : u11963zci_eperd

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2017-12-27 07:08:34
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_pengguna
-- ----------------------------
DROP TABLE IF EXISTS `tbl_pengguna`;
CREATE TABLE `tbl_pengguna` (
`id`  int(11) NOT NULL AUTO_INCREMENT ,
`nlogin`  varchar(16) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`nlengkap`  varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`pwd`  blob NULL ,
`tingkat`  tinyint(4) NULL DEFAULT NULL ,
`prov`  int(11) NULL DEFAULT NULL ,
`kabkota`  int(11) NULL DEFAULT NULL ,
`konfirmasi`  tinyint(4) NULL DEFAULT NULL ,
`email`  varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`notel1`  varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`notel2`  varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`nohp`  varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`jabatan`  varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
PRIMARY KEY (`id`)
)
ENGINE=MyISAM
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=1748

;

-- ----------------------------
-- Records of tbl_pengguna
-- ----------------------------
BEGIN;
INSERT INTO `tbl_pengguna` VALUES ('1737', 'eperda admin', 'Web Admin ePERDA', 0x2A30433930394246373641303733303842313533464133343834444342374432354143413738343646, '99', null, null, '1', 'sa-eperda@eperda-chat.com', null, null, null, 'Web Admin ePERDA'), ('1738', 'eperda sv', 'Supervisor ePERDA', 0x2A30433930394246373641303733303842313533464133343834444342374432354143413738343646, '1', null, null, '1', 'sv-eperda@eperda-chat.com', null, null, null, 'Supervisor ePERDA'), ('1739', 'Wil I', 'Wilayah I', 0x2A30433930394246373641303733303842313533464133343834444342374432354143413738343646, '2', '1', null, '1', 'wil1@eperda-chat.com', null, null, null, 'Wilayah I'), ('1740', 'Wil II', 'Wilayah II', 0x2A30433930394246373641303733303842313533464133343834444342374432354143413738343646, '2', '2', null, '1', 'wil2@eperda-chat.com', null, null, null, 'Wilayah II'), ('1741', 'Wil III', 'Wilayah III', 0x2A30433930394246373641303733303842313533464133343834444342374432354143413738343646, '2', '3', null, '1', 'wil3@eperda-chat.com', null, null, null, 'Wilayah III'), ('1742', 'Wil IV', 'Wilayah IV', 0x2A30433930394246373641303733303842313533464133343834444342374432354143413738343646, '2', '4', null, '1', 'wil4@eperda-chat.com', null, null, null, 'Wilayah IV'), ('1743', 'dumAdmin', 'Dummy Web Admin', 0x2A30433930394246373641303733303842313533464133343834444342374432354143413738343646, '99', null, null, '1', 'sa-eperda@eperda-chat.com', null, null, null, 'Dummy Web Admin'), ('1744', 'dumSV', 'Dummy SV', 0x2A30433930394246373641303733303842313533464133343834444342374432354143413738343646, '1', null, null, '1', 'sv-eperda@eperda-chat.com', null, null, null, 'Dummy SV'), ('1745', 'dumW1', 'Dummy Wil I', 0x2A30433930394246373641303733303842313533464133343834444342374432354143413738343646, '2', '1', null, '1', 'wil1@eperda-chat.com', null, null, null, 'Dummy Wil 1'), ('1746', 'dumAceh', 'Dummy Prov Aceh', 0x2A30433930394246373641303733303842313533464133343834444342374432354143413738343646, '10', '11', null, '1', 'test_aceh@eperda-chat.com', null, null, null, 'Dummy Prov Aceh'), ('1747', 'dum1101', 'Dummy Kab 1101', 0x2A30433930394246373641303733303842313533464133343834444342374432354143413738343646, '10', '11', '1101', '1', 'test_kab@eperda-chat.com', null, null, null, 'Dummy Kab 1101');
COMMIT;

-- ----------------------------
-- Table structure for tbl_region
-- ----------------------------
DROP TABLE IF EXISTS `tbl_region`;
CREATE TABLE `tbl_region` (
`id`  int(11) NOT NULL AUTO_INCREMENT ,
`prov`  varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`reg`  varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
`ks`  varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ,
PRIMARY KEY (`id`)
)
ENGINE=MyISAM
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
AUTO_INCREMENT=35

;

-- ----------------------------
-- Records of tbl_region
-- ----------------------------
BEGIN;
INSERT INTO `tbl_region` VALUES ('1', '11', '1', 'I'), ('2', '12', '1', 'I'), ('3', '13', '1', 'I'), ('4', '14', '1', 'I'), ('5', '15', '1', 'I'), ('6', '16', '1', 'I'), ('7', '17', '1', 'I'), ('8', '18', '1', 'I'), ('9', '19', '1', 'I'), ('10', '21', '1', 'I'), ('11', '31', '2', 'II'), ('12', '32', '2', 'II'), ('13', '33', '2', 'II'), ('14', '34', '2', 'II'), ('15', '35', '2', 'II'), ('16', '36', '2', 'II'), ('17', '51', '2', 'II'), ('18', '52', '4', 'IV'), ('19', '53', '4', 'IV'), ('20', '61', '3', 'III'), ('21', '62', '3', 'III'), ('22', '63', '3', 'III'), ('23', '64', '3', 'III'), ('24', '65', '3', 'III'), ('25', '71', '3', 'III'), ('26', '72', '3', 'III'), ('27', '73', '3', 'III'), ('28', '74', '3', 'III'), ('29', '75', '3', 'III'), ('30', '76', '3', 'III'), ('31', '81', '4', 'IV'), ('32', '82', '4', 'IV'), ('33', '91', '4', 'IV'), ('34', '92', '4', 'IV');
COMMIT;

-- ----------------------------
-- Auto increment value for tbl_pengguna
-- ----------------------------
ALTER TABLE `tbl_pengguna` AUTO_INCREMENT=1748;

-- ----------------------------
-- Auto increment value for tbl_region
-- ----------------------------
ALTER TABLE `tbl_region` AUTO_INCREMENT=35;
