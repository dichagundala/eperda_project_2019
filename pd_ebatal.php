<?php include_once("zz_koneksi_db.php"); ?>
<?php include ("zz_generate_menu.php"); ?>
<?php
	session_start();
	require('calendar/tc_calendar.php');
	$host  = $_SERVER['HTTP_HOST'];
	$uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
	$ip1 = $_SERVER['HTTP_X_FORWARDED_FOR'];
	$ip2 = $_SERVER['REMOTE_ADDR'];
	$waktu = date("Y-m-d H:i:s");
	$extra = "pd_login.php";
	if( $_SESSION["sws_id"] == "" || !isset($_SESSION["sws_id"]) || $_SESSION["sws_id"] == 0 ) {
		echo "<script>window.location.href=\"pd_login.php\";</script>";
		//header("Location: http://$host$uri/$extra");
		exit;
	}
	if( $_SESSION["sws_tingkat"] == 99 || $_SESSION["sws_tingkat"] == 1 || $_SESSION["sws_tingkat"] == 2 ) {
	} else {
		echo "<script>window.location.href=\"pd_login.php\";</script>";
		//header("Location: http://$host$uri/$extra");
		exit;
	}
?>


<!DOCTYPE HTML>
<html>

<head>
  <title>.:: ePERDA - Kementerian Dalam Negeri ::.</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0" />
  
  <script>
  	function checkit() {
		var errorMsg = "";
	
		//Check for a username
		if (document.form1.nomer.value==""){ errorMsg += "\n\t - Isi Nomor Perda dahulu";}
		if (document.form1.judul.value==""){ errorMsg += "\n\t - Isi Judul Perda dahulu";}
		if (document.form1.alasan.value==""){ errorMsg += "\n\t - Isi alasan pembatalan dahulu";}
		if (document.form1.nosk.value==""){ errorMsg += "\n\t - Isi nomer Surat Keputusan Pembatalan dahulu";}
		
		if (errorMsg != ""){
			msg  = "___________________________________________________________________\n\n";
			msg += "Pendaftaran belum bisa dilakukan dikarenakan kesalahan berikut ini.\n";
			msg += "___________________________________________________________________\n\n";
			errorMsg += alert(msg + errorMsg + "\n\n");
			return false;
		}
		
		return true;
	}
	
	var kab_arr = new Array();
var kab_opt = new Array();
var acounter = 0;
kab_arr[0] = new Array(" --- Pilih --- ");
kab_opt[0] = new Array('0');
<?php
	$hsl = mysqli_query($conn, "select kode from tbl_prov");
	while( $B = mysqli_fetch_array($hsl) ) {
		$kdprop = $B[0];
		$hsl1 = mysqli_query($conn, "select kode_kab, nama from tbl_kab where kode_prov='$kdprop' order by kode_kab");
		$calar = "\" --- Pilih --- \"";
		$oplar = "\"0\"";
		while( $B1 = mysqli_fetch_array($hsl1) ) {
			$kdkab = $B1[0];
			if( $calar == "" ) { $calar = "\"".$B1[1]."\""; } else { $calar .= ",\"".$B1[1]."\""; }
			if( $oplar == "" ) { $oplar = "\"".$B1[0]."\""; } else { $oplar .= ",\"".$B1[0]."\""; }
		}
?>
		kab_arr[<?php echo $kdprop; ?>] = new Array(<?php echo $calar; ?>);
		kab_opt[<?php echo $kdprop; ?>] = new Array(<?php echo $oplar; ?>);
<?php
	}
?>

function change_kab(combo1){
	var comboValue = combo1.value;
	document.forms["form1"].elements["kab"].options.length=0;
	for (var i=0;i<kab_arr[comboValue].length;i++){
		var option = document.createElement("option");
		option.setAttribute('value',kab_opt[comboValue][i]);
		option.innerHTML = kab_arr[comboValue][i];
		document.forms["form1"].elements["kab"].appendChild(option);
	}
}


function change_01_kab(combo1, indikator){
	var selin = 0;
	var comboValue = document.forms["form1"].elements[combo1].value;
	document.forms["form1"].elements["kab"].options.length=0;
	for (var i=0;i<kab_arr[comboValue].length;i++){
		var option = document.createElement("option");
		option.setAttribute('value',kab_opt[comboValue][i]);
		option.innerHTML = kab_arr[comboValue][i];
		document.forms["form1"].elements["kab"].appendChild(option);
		if( kab_opt[comboValue][i] == indikator ) { selin = i; }
	}
	document.forms["form1"].elements["kab"].options.selectedIndex=selin;
}
  </script>  
<script language="javascript">
function unduh(a) {
	document.form_unduh.id.value = a;
	document.form_unduh.submit();
}

function go_history(a, b) {
	document.form_hist.id.value = a;
	document.form_hist.token.value = b;
	document.form_hist.submit();
}

</script>  
  
</head>

<body>
  <div id="main">
    <header>
      <?php generate_logo(); ?>
      <?php generate_menu(5); ?>
    </header>
    <div id="site_content">
      <div class="gallery"><?php generate_gallery($conn, 950, 150); ?></div>
      <div class="content_webgis">
        <h1><strong>Pembatalan PERDA</strong></h1>
        <form action="pd_ebatal_confirm.php" method="post" enctype="multipart/form-data" name="form1" onSubmit="return checkit();">
          <table width="100%" border="0" cellspacing="0" cellpadding="3">
            <tr valign="top">
              <td width="18%"><h6>Nomer Perda</h6></td>
              <td width="1%">&nbsp;</td>
              <td width="81%"><input name="nomer" type="text" id="nomer" size="25" maxlength="25"></td>
            </tr>
            <tr valign="top">
              <td><h6>Tahun</h6></td>
              <td>&nbsp;</td>
              <td><input name="tahun" type="text" id="nomer" size="5" maxlength="4"></td>
            </tr>
            <tr valign="top">
              <td><h6>Judul Perda</h6></td>
              <td>&nbsp;</td>
              <td><input name="judul" type="text" id="judul" size="100" maxlength="100"></td>
            </tr>
            <tr valign="top">
              <td><h6>Provinsi</h6></td>
              <td>&nbsp;</td>
              <td><select name="prov" id="prov" onChange="change_kab(this);">
<?php
	$hsl = mysqli_query($conn, "select * from tbl_prov");
	while( $B = mysqli_fetch_array($hsl) ) {
?>
		<option value="<?php echo $B["kode"]; ?>"><?php echo $B["nama"]; ?></option>
<?php
	}
?>
              </select></td>
            </tr>
            <tr valign="top">
              <td><h6>Kabupaten/Kota</h6></td>
              <td>&nbsp;</td>
              <td><select name="kab" id="kab">
              </select></td>
            </tr><script>change_01_kab('prov','11');</script>
            <tr valign="top">
              <td><h6>Ringkasan</h6></td>
              <td>&nbsp;</td>
              <td><textarea name="ringkasan" id="ringkasan" cols="70" rows="5"></textarea></td>
            </tr>
            <tr valign="top">
              <td><h6>Dibatalkan Oleh</h6></td>
              <td>&nbsp;</td>
              <td><select name="sbatal" id="sbatal">
                <option value="1">Kementerian Dalam Negeri</option>
                <option value="2">Pemerintah Daerah Tingkat Provinsi</option>
                <option value="3">Pemerintah Daerah Tingkat Kabupaten/Kota</option>
              </select></td>
            </tr>
            <tr valign="top">
              <td><h6>No. Surat  Keputusan</h6></td>
              <td>&nbsp;</td>
              <td><input name="skep" type="text" id="skep" size="50" maxlength="50"></td>
            </tr>
            <tr valign="top">
              <td><h6>Berkas Surat Keputusan</h6></td>
              <td>&nbsp;</td>
              <td><input name="bskep" type="file" id="bskep" size="50" maxlength="50"></td>
            </tr>
            <tr valign="top">
              <td><h6>Waktu Pembatalan</h6></td>
              <td>&nbsp;</td>
              <td class="sws_table_kecil">
<?php
		$dnow = date("Y-m-d");
		$tl = explode("-",$dnow);
		$jam = date("H");
		$menit = date("i");
	  $myCalendar = new tc_calendar("waktu", true, false);
	  $myCalendar->setPicture("calendar/images/iconCalendar.gif");
	  $myCalendar->setDate($tl[2],$tl[1],$tl[0]);
	  $myCalendar->setPath("calendar/");
	  $myCalendar->setYearSelect(1930, date('Y'));
	  $myCalendar->dateAllow('1930-01-01', date('Y-m-d',mktime(0, 0, 0, 31, 12, date("Y"))));
	  // VERY IMPORTANT: next line REQUIRED only for localized version!
	  $myCalendar->setDateFormat(str_replace("%","",str_replace("B","F",str_replace("d","j",L_CAL_FORMAT))));
	  $myCalendar->setAlignment('left', 'bottom');
	  //$myCalendar->autoSubmit(true, "form1");
	  $myCalendar->writeScript();        
?></td>
            </tr>
            <tr valign="top">
              <td><h6>Alasan Pembatalan</h6></td>
              <td>&nbsp;</td>
              <td><textarea name="alasan" id="alasan" cols="70" rows="4"></textarea></td>
            </tr>
            <tr valign="top">
              <td><h6>Berkas Perda</h6></td>
              <td>&nbsp;</td>
              <td><input name="berkas" type="file" id="berkas" size="50" maxlength="50"></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td><input type="submit" name="button" id="button" value="Proses"></td>
            </tr>
          </table>
        </form>
        <br>
        <p>&nbsp;</p>
      </div>
    </div>
    <?php generate_footer(); ?>
  </div>
  <p>&nbsp;</p>
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript" src="js/image_fade.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
    });
  </script>
  
<form action="pd_man_user_edit.php" method="post" name="form_edit">
<input name="id" type="hidden" value="" />
</form>
<form action="pd_man_user_del.php" method="post" name="form_hapus">
<input name="id" type="hidden" value="" />
</form>
<form action="pd_unduh.php" method="post" name="form_unduh">
<input name="id" type="hidden" value="" />
</form>
<form action="pd_history.php" method="post" name="form_hist">
<input name="id" type="hidden" value="" />
<input name="token" type="hidden" value="" />
</form>
  
</body>
</html>
