<?php include_once("zz_koneksi_db.php"); ?>
<?php include ("zz_generate_menu.php"); ?>
<?php
	$targetpage = "berita.php"; 	
	$limit = 2; 
	$query = "SELECT COUNT(id) FROM berita where ok=1 and hapus=0";
	$B = mysqli_fetch_array(mysqli_query($conn, $query));
	$total_pages = $B[0];
	$stages = 3;
	$page = mysqli_escape_string($conn,$_GET['page']);
	if($page) { $start = ($page - 1) * $limit;  } else { $start = 0; }
	$hsl = mysqli_query($conn, "select * from berita where ok=1 and hapus=0 ORDER BY id DESC LIMIT $start, $limit");
	$query1 = "SELECT * FROM berita where ok=1 and hapus=0 ORDER BY id DESC LIMIT $start, $limit";
	$result = mysqli_query($conn, $query1);
	
	if ($page == 0){$page = 1;}
	$prev = $page - 1;	
	$next = $page + 1;							
	$lastpage = ceil($total_pages/$limit);		
	$LastPagem1 = $lastpage - 1;
	$paginate = '';
	
	if($lastpage > 1) {	
		$paginate .= "<div class='paginate'>";
		if ($page > 1){
			$paginate.= "&nbsp;&nbsp;<a href='$targetpage?page=$prev'>Halaman Sebelumnya</a>&nbsp;&nbsp;";
		} else {
			$paginate.= "&nbsp;&nbsp;<span class='disabled'>Halaman Sebelumnya&nbsp;&nbsp;</span>";	
		}
		if ($lastpage < 7 + ($stages * 2)) {	
			for ($counter = 1; $counter <= $lastpage; $counter++) {
				if ($counter == $page){
					$paginate.= "&nbsp;&nbsp;<span class='current'>$counter</span>&nbsp;&nbsp;";
				}else{
					$paginate.= "&nbsp;&nbsp;<a href='$targetpage?page=$counter'>$counter</a>&nbsp;&nbsp;";
				}					
			}
		} elseif($lastpage > 5 + ($stages * 2))	{
			if($page < 1 + ($stages * 2)) {
				for ($counter = 1; $counter < 4 + ($stages * 2); $counter++) {
					if ($counter == $page) {
						$paginate.= "&nbsp;&nbsp;<span class='current'>$counter</span>&nbsp;&nbsp;";
					}else{
						$paginate.= "&nbsp;&nbsp;<a href='$targetpage?page=$counter'>$counter</a>&nbsp;&nbsp;";
					}					
				}
				$paginate.= "...";
				$paginate.= "&nbsp;&nbsp;<a href='$targetpage?page=$LastPagem1'>$LastPagem1</a>&nbsp;&nbsp;";
				$paginate.= "&nbsp;&nbsp;<a href='$targetpage?page=$lastpage'>$lastpage</a>&nbsp;&nbsp;";		
			} elseif($lastpage - ($stages * 2) > $page && $page > ($stages * 2)) {
				$paginate.= "&nbsp;&nbsp;<a href='$targetpage?page=1'>1</a>&nbsp;&nbsp;";
				$paginate.= "&nbsp;&nbsp;<a href='$targetpage?page=2'>2</a>&nbsp;&nbsp;";
				$paginate.= "...";
				for ($counter = $page - $stages; $counter <= $page + $stages; $counter++) {
					if ($counter == $page){
						$paginate.= "&nbsp;&nbsp;<span class='current'>$counter</span>&nbsp;&nbsp;";
					}else{
						$paginate.= "&nbsp;&nbsp;<a href='$targetpage?page=$counter'>$counter</a>&nbsp;&nbsp;";
					}					
				}
				$paginate.= "...";
				$paginate.= "&nbsp;&nbsp;<a href='$targetpage?page=$LastPagem1'>$LastPagem1</a>&nbsp;&nbsp;";
				$paginate.= "&nbsp;&nbsp;<a href='$targetpage?page=$lastpage'>$lastpage</a>&nbsp;&nbsp;";		
			} else {
				$paginate.= "&nbsp;&nbsp;<a href='$targetpage?page=1'>1</a>&nbsp;&nbsp;";
				$paginate.= "&nbsp;&nbsp;<a href='$targetpage?page=2'>2</a>&nbsp;&nbsp;";
				$paginate.= "...";
				for ($counter = $lastpage - (2 + ($stages * 2)); $counter <= $lastpage; $counter++) {
					if ($counter == $page){
						$paginate.= "&nbsp;&nbsp;<span class='current'>$counter</span>&nbsp;&nbsp;";
					}else{
						$paginate.= "&nbsp;&nbsp;<a href='$targetpage?page=$counter'>$counter</a>&nbsp;&nbsp;";
					}					
				}
			}
		}
					
		if ($page < $counter - 1){ 
			$paginate.= "&nbsp;&nbsp;<a href='$targetpage?page=$next'>Halaman Berikutnya</a>&nbsp;&nbsp;";
		}else{
			$paginate.= "&nbsp;&nbsp;<span class='disabled'>Halaman Berikutnya</span>&nbsp;&nbsp;";
		}
			
		$paginate.= "</div>";		
	}
?>

<!DOCTYPE HTML>
<html>

<head>
  <title>.:: ePERDA - Kementerian Dalam Negeri ::.</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>
</head>

<body>
  <div id="main">
    <header>
      <?php generate_logo(); ?>
      <?php generate_menu(1); ?>
    </header>
    <div id="site_content">
      <div class="gallery"><?php generate_gallery($conn, 950, 150); ?></div>
      <div class="content_webgis">
        <h1>Daftar Berita</h1>
<?php
	echo "&nbsp;&nbsp;Total ada $total_pages berita";
 	echo $paginate;
	echo "<hr><br><br>";
	while( $B = mysqli_fetch_array($hsl) ) {    
?>    
      
        <div class="article">
          <h2><span><?php echo $B["judul"]; ?></span></h2>
          <p>Dikirim oleh <a href="#"><?php echo $B["oleh"]; ?></a> tanggal <?php echo sws_get_tgl($B["tgl"]); ?></p>
          <p><?php echo sws_potong($B["isi"],150); ?></p><br><br>
          <p><a href="berita_satu.php?a=<?php echo $B["id"]; ?>">Baca selengkapnya</a></p>
        </div>
      <hr /><br></div>
<?php
	}
?>
      </div>
    <br><br><br><br>
    </div>
    <?php generate_footer(); ?>
  </div>
  <p>&nbsp;</p>
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript" src="js/image_fade.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
    });
  </script>
</body>
</html>
