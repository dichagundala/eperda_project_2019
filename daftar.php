
<?php
	include_once("zz_koneksi_db.php");
	include ("zz_generate_menu.php");
	//session_start();
	//$host  = $_SERVER['HTTP_HOST'];
	//$uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
	//$ip1 = $_SERVER['HTTP_X_FORWARDED_FOR'];
	//$ip2 = $_SERVER['REMOTE_ADDR'];
	//$waktu = date("Y-m-d H:i:s");
?>


<!DOCTYPE HTML>
<html>

<head>
  <title>.:: ePERDA - Kementerian Dalam Negeri ::.</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>
  
  <script>
  	function checkit() {
		var errorMsg = "";
	
		//Check for a username
		if (document.form1.nlog.value==""){ errorMsg += "\n\t - Isi nama login dahulu";}
		if (document.form1.nlengkap.value==""){ errorMsg += "\n\t - Isi nama lengkap dahulu";}
		if (document.form1.jab.value==""){ errorMsg += "\n\t - Isi jabatan dahulu";}
		if (document.form1.email.value==""){ errorMsg += "\n\t - Isi email dahulu";}
		if (document.form1.notelp1.value==""){ errorMsg += "\n\t - Isi nomor telpon dahulu";}
		if (document.form1.nohp.value==""){ errorMsg += "\n\t - Isi nomor HP dahulu";}
		if (document.form1.pwd.value==""){ errorMsg += "\n\t - Kata sandi tidak boleh kosong";}
		if (document.form1.pwd.value!=document.form1.pwd1.value){ errorMsg += "\n\t - Kata sandi tidak dapat diverifikasi";}
		if (document.form1.prov.value=="0"){ errorMsg += "\n\t - Pilih Provinsi dahulu";}
		
		if (errorMsg != ""){
			msg  = "___________________________________________________________________\n\n";
			msg += "Pendaftaran belum bisa dilakukan dikarenakan kesalahan berikut ini.\n";
			msg += "___________________________________________________________________\n\n";
			errorMsg += alert(msg + errorMsg + "\n\n");
			return false;
		}
		
		return true;
	}
	
	var kab_arr = new Array();
var kab_opt = new Array();
var acounter = 0;
kab_arr[0] = new Array(" --- Pilih --- ");
kab_opt[0] = new Array('0');
<?php
	$hsl = mysqli_query($conn, "select kode from tbl_prov");
	while( $B = mysqli_fetch_array($hsl) ) {
		$kdprop = $B[0];
		$hsl1 = mysqli_query($conn, "select kode_kab, nama from tbl_kab where kode_prov='$kdprop' order by kode_kab");
		$calar = "\" --- Pilih --- \"";
		$oplar = "\"0\"";
		while( $B1 = mysqli_fetch_array($hsl1) ) {
			$kdkab = $B1[0];
			if( $calar == "" ) { $calar = "\"".$B1[1]."\""; } else { $calar .= ",\"".$B1[1]."\""; }
			if( $oplar == "" ) { $oplar = "\"".$B1[0]."\""; } else { $oplar .= ",\"".$B1[0]."\""; }
		}
?>
		kab_arr[<?php echo $kdprop; ?>] = new Array(<?php echo $calar; ?>);
		kab_opt[<?php echo $kdprop; ?>] = new Array(<?php echo $oplar; ?>);
<?php
	}
?>

function change_kab(combo1){
	var comboValue = combo1.value;
	document.forms["form1"].elements["kab"].options.length=0;
	for (var i=0;i<kab_arr[comboValue].length;i++){
		var option = document.createElement("option");
		option.setAttribute('value',kab_opt[comboValue][i]);
		option.innerHTML = kab_arr[comboValue][i];
		document.forms["form1"].elements["kab"].appendChild(option);
	}
}


function change_01_kab(combo1, indikator){
	var selin = 0;
	var comboValue = document.forms["form1"].elements[combo1].value;
	document.forms["form1"].elements["kab"].options.length=0;
	for (var i=0;i<kab_arr[comboValue].length;i++){
		var option = document.createElement("option");
		option.setAttribute('value',kab_opt[comboValue][i]);
		option.innerHTML = kab_arr[comboValue][i];
		document.forms["form1"].elements["kab"].appendChild(option);
		if( kab_opt[comboValue][i] == indikator ) { selin = i; }
	}
	document.forms["form1"].elements["kab"].options.selectedIndex=selin;
}
  </script>
</head>

<body>
  <div id="main">
    <header>
      <?php generate_logo(); ?>
      <?php generate_menu(5); ?>
    </header>
    <div id="site_content">
      <div class="gallery"><?php generate_gallery($conn, 950, 150); ?></div>
      <div class="content_webgis">
        <h1><strong>Pendaftaran Pengguna</strong></h1>
        <form name="form1" method="post" action="daftar_confirm.php" onSubmit="return checkit();">
          <table width="95%" border="0" cellspacing="0" cellpadding="3">
            <tr>
              <td width="23%">Nama Login</td>
              <td width="2%">:</td>
              <td colspan="3"><input name="nlog" type="text" id="nlog" size="16" maxlength="16"></td>
            </tr>
            <tr>
              <td>Nama Lengkap</td>
              <td>:</td>
              <td colspan="3"><input name="nlengkap" type="text" id="nlengkap" size="50" maxlength="50"></td>
            </tr>
            <tr>
              <td>Jabatan</td>
              <td>:</td>
              <td colspan="3"><input name="jab" type="text" id="jab" size="50" maxlength="50"></td>
            </tr>
            <tr>
              <td>Otoritas</td>
              <td>&nbsp;</td>
              <td width="3%"><input name="radio" type="radio" id="radio" value="10" checked></td>
              <td width="13%">Provinsi</td>
              <td width="59%"><select name="prov" id="prov" onChange="change_kab(this);">
              	<option value="0">--- Pilih ---</option>
<?php
	$hsl = mysqli_query($conn, "select * from tbl_prov");
	while( $B = mysqli_fetch_array($hsl) ) {
?>
				<option value="<?php echo $B[0]; ?>"><?php echo $B[1]; ?></option>
<?php
	}
?>
              </select></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td><input type="radio" name="radio" id="radio2" value="11"></td>
              <td>Kabupaten / Kota</td>
              <td><select name="kab" id="kab">
              </select></td>
            </tr>
            <tr>
              <td>Alamat Email</td>
              <td>:</td>
              <td colspan="3"><input name="email" type="email" id="email" size="40" maxlength="40"></td>
            </tr>
            <tr>
              <td>No. Telpon</td>
              <td>:</td>
              <td colspan="3"><input name="notelp1" type="text" id="notelp1">
              <input name="notelp2" type="text" id="notelp2"></td>
            </tr>
            <tr>
              <td>No. HP</td>
              <td>:</td>
              <td colspan="3"><input name="nohp" type="text" id="nohp"></td>
            </tr>
            <tr>
              <td>Kata Sandi</td>
              <td>:</td>
              <td colspan="3"><input type="password" name="pwd" id="pwd"></td>
            </tr>
            <tr>
              <td>Konfirmasi Kata Sandi</td>
              <td>&nbsp;</td>
              <td colspan="3"><input type="password" name="pwd1" id="pwd1"></td>
            </tr>
            <tr>
              <td colspan="5" align="center"><br>
                <br>
              <br>
              <input type="submit" name="button" id="button" value="Proses"></td>
            </tr>
          </table>
        </form>
        <p>&nbsp;</p>
      </div>
    </div>
    <?php generate_footer(); ?>
  </div>
  <p>&nbsp;</p>
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript" src="js/image_fade.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
    });
  </script>
</body>
</html>
