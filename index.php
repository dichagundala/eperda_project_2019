<?php include_once("zz_koneksi_db.php"); ?>
<?php include ("zz_generate_menu.php"); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="Partnerinaja" />
    <meta name="description" content="ePerda - Kemendagri">
    <!-- Document title -->
    <title>ePerda | Kementerian Dalam Negeri</title>
    <!-- Stylesheets & Fonts -->
    <link href="assets/css/plugins.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/custom.css" rel="stylesheet">
    <link href="assets/css/responsive.css" rel="stylesheet">

    <!-- Template color -->
    <link href="assets/css/color-variations/red-dark.css" rel="stylesheet" type="text/css" media="screen">

    <!-- Morris.js css -->
    <link href="assets/js/plugins/components/morrisjs/morris.css" rel="stylesheet">
</head>

<body>


    <!-- Body Inner -->    
    <div class="body-inner">

        <!-- Header -->
        <header id="header" class="header-modern">
            <div class="header-inner">
                <div class="container">
                    <!--Logo-->
                    <div id="logo">
                        <a href="index.html" class="logo" data-src-dark="assets/images/logo-dark.png">
                            <img src="assets/images/logo.png" alt="Polo Logo">
                        </a>
                    </div>
                    <!--End: Logo-->

                    <!-- Search -->
                    <div id="search">
                        <div id="search-logo"><img src="assets/images/logo.png" alt="Polo Logo"></div>
                        <button id="btn-search-close" class="btn-search-close" aria-label="Close search form"><i
                            class="icon-x"></i></button>
                            <form class="search-form" action="search-results-page.html" method="get">
                                <input class="form-control" name="q" type="search" placeholder="Search..."
                                autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" />
                                <span class="text-muted">Start typing & press "Enter" or "ESC" to close</span>
                            </form>
                        </div>
                        <!-- end: search -->

                        <!--Header Extras-->
                        <div class="header-extras">
                            <ul>
                                <li>
                                    <!--search icon-->
                                    <a id="btn-search" href="#"> <i class="icon-search1"></i></a>
                                    <!--end: search icon-->
                                </li>
                            </ul>
                        </div>
                        <!--end: Header Extras-->

                        <!--Navigation Resposnive Trigger-->
                        <div id="mainMenu-trigger">
                            <button class="lines-button x"> <span class="lines"></span> </button>
                        </div>
                        <!--end: Navigation Resposnive Trigger-->

                        <!--Navigation-->
                        <div id="mainMenu" class="light">
                            <div class="container">
                                <nav>
                                    <ul>
                                        <li><a href="index.html">Beranda</a></li>
                                        <li class="dropdown"> <a href="#">Pelayanan</a>
                                            <ul class="dropdown-menu">
                                               <li class="dropdown-submenu"><a href="#">Topbar</a>
                                                <ul class="dropdown-menu">
                                                    <li><a href="header-topbar.html">Light</a></li>
                                                    <li><a href="header-topbar-dark.html">Dark</a></li>
                                                    <li><a href="header-topbar-transparent.html">Transparent</a></li>
                                                    <li><a href="header-topbar-colored.html">Colored</a></li>
                                                    <li><a href="header-topbar-fullwidth.html">Fullwidth</a></li>
                                                </ul>
                                            </li>
                                            <li class="dropdown-submenu"><a href="#">Main Menu<span class="badge badge-danger">NEW</span></a>
                                                <ul class="dropdown-menu">
                                                    <li><a href="menu.html">Default</a></li>
                                                    <li class="dropdown-submenu"><a href="#">Outline</a>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="menu-outline.html">Default</a></li>
                                                            <li><a href="menu-outline-dark.html">Dark</a></li>
                                                            <li><a href="menu-outline-light.html">Light</a></li>
                                                        </ul>
                                                    </li>
                                                    <li><a href="menu-rounded-dropdown.html">Rounded Dropdown</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="dropdown"> <a href="#">Produk Hukum</a>
                                        <ul class="dropdown-menu">
                                            <li class="dropdown-submenu"><a href="#">Sliders</a>
                                                <ul class="dropdown-menu">
                                                    <li><a href="slider-revolution-slider.html">Revolution Slider</a></li>
                                                    <li><a href="slider-owl-slider.html">OWL Slider</a></li>
                                                    <li><a href="slider-static-media-image.html">Fullscreen Image</a></li>
                                                    <li><a href="slider-static-media-parallax.html">Fullscreen Parallax</a></li>
                                                    <li><a href="slider-static-media-text-rotator.html">Fullscreen Text Rotator</a></li>
                                                    <li><a href="slider-static-media-video.html">Fullscreen HTML5 Video</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="widgets.html">Widgets </a></li>
                                            <li><a href="page-loaders.html">Page Loaders<span class="badge badge-danger">NEW</span></a></li>
                                            <li class="dropdown-submenu"><a href="#">Modal Auto Open<span class="badge badge-danger">NEW</span></a>
                                                <ul class="dropdown-menu">
                                                    <li><a href="modal-auto-open-subscription.html">Subscription</a></li>
                                                    <li><a href="modal-auto-open-shop.html">Shop Promotion</a></li>
                                                    <li><a href="modal-auto-open-login.html">Login Form</a></li>
                                                    <li><a href="modal-auto-open-audio.html">Audio Player</a></li>
                                                    <li><a href="modal-auto-open-video.html">HTML5 Video</a></li>
                                                    <li><a href="modal-auto-open-youtube.html">YouTube Video</a></li>
                                                    <li><a href="modal-auto-open-vimeo.html">Vimeo Video</a></li>
                                                    <li><a href="modal-auto-open-iframe.html">Iframe</a></li>
                                                    <li><a href="modal-auto-open.html">Sample Text</a></li>
                                                </ul>
                                            </li>
                                            <li class="dropdown-submenu"><a href="#">Cookie Notify<span class="badge badge-danger">NEW</span></a>
                                                <ul class="dropdown-menu">
                                                    <li><a href="cookie-notify.html">Top position</a> </li>
                                                    <li><a href="cookie-notify-bottom.html">Bottom position</a> </li>
                                                </ul>
                                            </li>
                                            <li class="dropdown-submenu"><a href="#">Menu Labels</a>
                                                <ul class="dropdown-menu">
                                                    <li><a href="#">Label (new)<span class="badge badge-danger">NEW</span></a> </li>
                                                    <li><a href="#">Label (hot)<span class="badge badge-danger">HOT</span></a> </li>
                                                    <li><a href="#">Label (popular)<span class="badge badge-success">POPULAR</span></a> </li>
                                                    <li><a href="#">Label (sale)<span class="badge badge-warning">SALE</span></a> </li>
                                                    <li><a href="#">Label (info)<span class="badge badge-info">INFO</span></a> </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <!--end: Navigation-->
                </div>
            </div>
        </header>
        <!-- end: Header -->

        <!-- Inspiro Slider -->
        <div id="slider" class="inspiro-slider arrows-large arrows-creative dots-creative" data-height-xs="360" data-height="450" style="border-radius: 0 0 60px 60px;">
            <!-- Slide 1 -->
            <div class="slide" style="background-image:url('assets/homepages/creative/images/slider/4.jpg');">
            </div>
            <!-- end: Slide 1 -->
            <!-- Slide 2 -->
            <div class="slide background-overlay-grey-dark" style="background-image:url('assets/homepages/creative/images/slider/5.jpg');">
            </div>
            <!-- end: Slide 2 -->
        </div>
        <!--end: Inspiro Slider -->

        <section>
            <div class="container">
                <div class="col-lg-12 col-md-12 m-b-40 text-center">
                    <h2>Statistik</h2>
                    <p class="lead">Lorem ipsum dolor sit amet, consecte adipiscing elit. Suspendisse condimentum porttitor cursumus.</p>
                </div>
                <div class="card-deck">
                    <div class="card cd-cus">
                        <div class="card-header cd-h">
                            Wilayah I
                        </div>
                        <div class="card-body">
                            <table width="100%" cellspacing="0" cellpadding="1" border="0">
                                <tbody>
                                    <tr>
                                        <td>Menunggu Fasilitas</td>
                                        <td class="text-right">100</td>
                                    </tr>
                                    <tr>
                                        <td>Sudah Fasilitas</td>
                                        <td class="text-right">100</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"><hr></td>
                                    </tr>
                                    <tr>
                                        <td>Permohonan Register</td>
                                        <td class="text-right">100</td>
                                    </tr>
                                    <tr>
                                        <td>Diproses</td>
                                        <td class="text-right">100</td>
                                    </tr>
                                    <tr>
                                        <td>Sudah dikirim</td>
                                        <td class="text-right">100</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- Wilayah 2 -->
                    <div class="card cd-cus">
                        <div class="card-header cd-h">
                            Wilayah II
                        </div>
                        <div class="card-body">
                            <table width="100%" cellspacing="0" cellpadding="1" border="0">
                                <tbody>
                                    <tr>
                                        <td>Menunggu Fasilitas</td>
                                        <td class="text-right">100</td>
                                    </tr>
                                    <tr>
                                        <td>Sudah Fasilitas</td>
                                        <td class="text-right">100</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"><hr></td>
                                    </tr>
                                    <tr>
                                        <td>Permohonan Register</td>
                                        <td class="text-right">100</td>
                                    </tr>
                                    <tr>
                                        <td>Diproses</td>
                                        <td class="text-right">100</td>
                                    </tr>
                                    <tr>
                                        <td>Sudah dikirim</td>
                                        <td class="text-right">100</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- Wilayah 3 -->
                    <div class="card cd-cus">
                        <div class="card-header cd-h">
                            Wilayah III
                        </div>
                        <div class="card-body">
                            <table width="100%" cellspacing="0" cellpadding="1" border="0">
                                <tbody>
                                    <tr>
                                        <td>Menunggu Fasilitas</td>
                                        <td class="text-right">100</td>
                                    </tr>
                                    <tr>
                                        <td>Sudah Fasilitas</td>
                                        <td class="text-right">100</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"><hr></td>
                                    </tr>
                                    <tr>
                                        <td>Permohonan Register</td>
                                        <td class="text-right">100</td>
                                    </tr>
                                    <tr>
                                        <td>Diproses</td>
                                        <td class="text-right">100</td>
                                    </tr>
                                    <tr>
                                        <td>Sudah dikirim</td>
                                        <td class="text-right">100</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- Wilayah 4 -->
                    <div class="card cd-cus">
                        <div class="card-header cd-h">
                            Wilayah IV
                        </div>
                        <div class="card-body">
                            <table width="100%" cellspacing="0" cellpadding="1" border="0">
                                <tbody>
                                    <tr>
                                        <td>Menunggu Fasilitas</td>
                                        <td class="text-right">100</td>
                                    </tr>
                                    <tr>
                                        <td>Sudah Fasilitas</td>
                                        <td class="text-right">100</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"><hr></td>
                                    </tr>
                                    <tr>
                                        <td>Permohonan Register</td>
                                        <td class="text-right">100</td>
                                    </tr>
                                    <tr>
                                        <td>Diproses</td>
                                        <td class="text-right">100</td>
                                    </tr>
                                    <tr>
                                        <td>Sudah dikirim</td>
                                        <td class="text-right">100</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- end: Card Deck -->
                <div class="row m-t-40">
                    <div class="col-sm-8">
                        <div class="card cd-cus2">
                          <div class="card-body">
                            <h5 class="card-title">Jumlah PERDA Pemerintah Daerah Tingkat Provinsi yang terdata pada e-Perda</h5>
                            <div class="col-lg-12">
                                <div id="chartdiv2" style="width: 100%; height: 500px;"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="card cd-cus2">
                      <div class="card-body">
                        <h5 class="card-title">Register PERDA Tahun 2019</h5>
                        <div class="col-lg-12">
                            <div id="chartdiv2" style="width: 100%; height: 500px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end: row card -->
        <div class="row m-t-40">
            <div class="col-lg-12">
                <iframe class="cd-cus2" bordercolor="#000000" frameborder="0" src="zz_peta.php" width="100%" height="300" scrolling="no"></iframe>
            </div>
        </div>
        <!-- end: row card -->
    </div>
</section>


<!-- Our numbers -->
<section class="background-colored p-t-120 p-b-40">
    <div id="particles-js" class="particles"></div>
    <div class="container xs-text-center sm-text-center text-light">
        <div class="row">
            <div class="col-lg-3">
                <h1>Apa itu ePerda ?</h1>
            </div>
            <div class="col-lg-9 p-b-40">
                <p class="lead">Aplikasi Online Register dan Fasilitasi Rancangan Peraturan Daerah (ePERDA) adalah aplikasi yang dikembangkan oleh Direktorat Produk Hukum Daerah - Direktorat Jenderal Otonomi Daerah - Kementerian Dalam Negeri Republik Indonesia yang bertujuan untuk mempercepat proses register dan fasilitasi dari Rancangan Peraturan Daerah. 
                    <br><br>
                    Alasan dan latar belakang dibangunnya aplikasi ePERDA ini melihat bahwa sampai saat ini pencantuman dan pendefinisian Nomor Register Peraturan Daerah masih bersifat manual yang mengakibatkan kurang optimalnya tujuan pencapaian tertib administrasi. 
                    <br><br>
                Selengkapnya ...</p>
            </div>
        </div>
    </div>
</section>
<!-- end: Our numbers -->


<!-- Artikel -->
<section>
    <div class="container">
        <div class="col-lg-12 col-md-12 m-b-40 text-center">
            <h2>Berita</h2>
            <p class="lead">Lorem ipsum dolor sit amet, consecte adipiscing elit. Suspendisse condimentum porttitor cursumus.</p>
        </div>
        <!-- Blog -->

        <div class="carousel" data-items="3">

            <!-- Post item-->
            <div class="post-item border">
                <div class="post-item-wrap shadows">
                    <div class="post-image">
                        <a href="#">
                            <img alt="" src="assets/images/blog/1.jpg">
                        </a>
                    </div>
                    <div class="post-item-description">
                        <span class="post-meta-comments"><a href="">Admin - </a></span>
                        <span class="post-meta-date">Jan 21, 2017</span>
                        <h2><a href="#">Standard post with a single image
                        </a></h2>
                        <p>Curabitur pulvinar euismod ante, ac sagittis ante posuere ac. Vivamus luctus commodo dolor porta feugiat. Fusce at velit id ligula pharetra laoreet.</p>

                        <a href="#" class="btn btn-sm btn-grad">Selengkapnya <i class="fa fa-arrow-right"></i></a>

                    </div>
                </div>
            </div>
            <!-- end: Post item-->

            <!-- Post item-->
            <div class="post-item border">
                <div class="post-item-wrap shadows">
                    <div class="post-image">
                        <a href="#">
                            <img alt="" src="assets/images/blog/1.jpg">
                        </a>
                    </div>
                    <div class="post-item-description">
                        <span class="post-meta-comments"><a href="">Admin - </a></span>
                        <span class="post-meta-date">Jan 21, 2017</span>
                        <h2><a href="#">Standard post with a single image
                        </a></h2>
                        <p>Curabitur pulvinar euismod ante, ac sagittis ante posuere ac. Vivamus luctus commodo dolor porta feugiat. Fusce at velit id ligula pharetra laoreet.</p>

                        <a href="#" class="btn btn-sm btn-grad">Selengkapnya <i class="fa fa-arrow-right"></i></a>

                    </div>
                </div>
            </div>
            <!-- end: Post item-->

            <!-- Post item-->
            <div class="post-item border">
                <div class="post-item-wrap shadows">
                    <div class="post-image">
                        <a href="#">
                            <img alt="" src="assets/images/blog/1.jpg">
                        </a>
                    </div>
                    <div class="post-item-description">
                        <span class="post-meta-comments"><a href="">Admin - </a></span>
                        <span class="post-meta-date">Jan 21, 2017</span>
                        <h2><a href="#">Standard post with a single image
                        </a></h2>
                        <p>Curabitur pulvinar euismod ante, ac sagittis ante posuere ac. Vivamus luctus commodo dolor porta feugiat. Fusce at velit id ligula pharetra laoreet.</p>

                        <a href="#" class="btn btn-sm btn-grad">Selengkapnya <i class="fa fa-arrow-right"></i></a>

                    </div>
                </div>
            </div>
            <!-- end: Post item-->

            <!-- Post item-->
            <div class="post-item border">
                <div class="post-item-wrap shadows">
                    <div class="post-image">
                        <a href="#">
                            <img alt="" src="assets/images/blog/1.jpg">
                        </a>
                    </div>
                    <div class="post-item-description">
                        <span class="post-meta-comments"><a href="">Admin - </a></span>
                        <span class="post-meta-date">Jan 21, 2017</span>
                        <h2><a href="#">Standard post with a single image
                        </a></h2>
                        <p>Curabitur pulvinar euismod ante, ac sagittis ante posuere ac. Vivamus luctus commodo dolor porta feugiat. Fusce at velit id ligula pharetra laoreet.</p>

                        <a href="#" class="btn btn-sm btn-grad">Selengkapnya <i class="fa fa-arrow-right"></i></a>

                    </div>
                </div>
            </div>
            <!-- end: Post item-->

        </div>
        <!-- end: Blog -->
    </div>
</section>
<!-- end: Artikel -->

<hr class="our-space">

<!-- Client logo -->
<section class="background-grey text-center">
    <div class="container">
     <div class="carousel" data-items="6" data-items-sm="4" data-items-xs="3"  data-items-xxs="2"  data-margin="20" data-arrows="false" data-autoplay="true" data-autoplay-timeout="3000" data-loop="true">
        <div>
            <a href="#"><img alt="" src="assets/images/clients/1.png"> </a>
        </div>
        <div>
            <a href="#"><img alt="" src="assets/images/clients/2.png"> </a>
        </div>
        <div>
            <a href="#"><img alt="" src="assets/images/clients/3.png"> </a>
        </div>
        <div>
            <a href="#"><img alt="" src="assets/images/clients/4.png"> </a>
        </div>
    </div>
</div>
</section>
<!-- end: Client logo -->

<!-- Footer -->
<footer id="footer">
    <div class="footer-content text-light">
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    <div class="widget">
                        <div id="logo">
                            <a href="index.html" class="logo">
                                <img src="assets/images/logo-footer.png" alt="Kemendagri Footer Logo" style="width: 80%;">
                            </a>
                        </div>
                        <hr class="m-t-20 m-b-20">
                        <p class="mb-5"><strong>Jl. Medan Merdeka Utara No. 7, Jakarta Pusat </strong><br>
                            <strong>Telp.</strong> (021) 3453627 <strong>Fax.</strong> (021) 3453627 <br>
                            <strong>e-mail:</strong> phd.otda@kemendagri.go.id</p>
                        </div>
                    </div> <div class="col-lg-7">
                        <div class="row">
                            <div class="col-lg-5">
                                <div class="widget">
                                    <div class="widget-title">Menu</div>
                                    <hr>
                                    <ul class="list-icon list-icon-caret">                                           
                                        <li><a href="#">Tentang e-Perda</a></li>
                                        <li><a href="#">e-Konsultasi Publik</a></li>
                                        <li><a href="#">e-Fasilitasi</a></li>
                                        <li><a href="#">e-Register</a></li>
                                    </ul>
                                </div>  
                            </div>

                            <div class="col-lg-7">
                                <div class="widget">
                                    <div class="widget-title">Tautan</div>
                                    <hr>
                                    <ul class="list-icon list-icon-caret">                                           
                                        <li><a href="#">Kementerian Dalam Negeri</a></li>
                                        <li><a href="#">Ditjen Otonomi Daerah</a></li>
                                        <li><a href="#">Sekretariat Negara</a></li>
                                        <li><a href="#">Kementerian Hukum dan HAM</a></li>
                                        <li><a href="#">Ditjen Bina Pembangunan Daerah</a></li>
                                        <li><a href="#">Jaringan Dokumentasi dan Informasi Hukum Kementerian Dalam Negeri</a></li>
                                        <li><a href="#">Ditjen Bina Keuangan Daerah</a></li>
                                    </ul>
                                </div>  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright-content">
            <div class="container">
                <div class="copyright-text text-center">&copy; 2020 DIREKTORAT PRODUK HUKUM DAERAH - All Right Reserved <a href="https://www.partnerinaja.com" target="_blank"> Developer</a> </div>
            </div>
        </div>
    </footer>
    <!-- end: Footer -->

</div>
<!-- end: Body Inner -->

<!-- Scroll top -->
<a id="scrollTop"><i class="icon-chevron-up1"></i><i class="icon-chevron-up1"></i></a>
<!--Plugins-->
<script src="assets/js/jquery.js"></script>
<script src="assets/js/plugins.js"></script>

<!--Template functions-->
<script src="assets/js/functions.js"></script>

<!--Particles-->
<script src="assets/js/plugins/components/particles.js"></script>
<script src="assets/js/plugins/components/particles-animation.js"></script>


<!-- amcharts component-->
<script src="assets/js/plugins/components/amcharts/core.js"></script>
<script src="assets/js/plugins/components/amcharts/charts.js"></script>


<script src="assets/js/plugins/components/amcharts/maps.js"></script>
<script src="assets/js/plugins/components/amcharts/geodata/worldLow.js"></script>
<script src="assets/js/plugins/components/amcharts/geodata/usaLow.js"></script>

<script src="assets/js/plugins/components/amcharts/themes/animated.js"></script>

<script type="text/javascript">
    // Create chart instance
    var chart = am4core.create("chartdiv2", am4charts.XYChart);
    chart.scrollbarX = new am4core.Scrollbar();

        // Add data
        chart.data = [{
            "country": "USA",
            "visits": 3025
        }, {
            "country": "China",
            "visits": 1882
        }, {
            "country": "Japan",
            "visits": 1809
        }, {
            "country": "Germany",
            "visits": 1322
        }, {
            "country": "UK",
            "visits": 1122
        }, {
            "country": "France",
            "visits": 1114
        }, {
            "country": "India",
            "visits": 984
        }, {
            "country": "Spain",
            "visits": 711
        }, {
            "country": "Netherlands",
            "visits": 665
        }, {
            "country": "Russia",
            "visits": 580
        }, {
            "country": "South Korea",
            "visits": 443
        }, {
            "country": "Canada",
            "visits": 441
        }];

        // Create axes
        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "country";
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 30;
        categoryAxis.renderer.labels.template.horizontalCenter = "right";
        categoryAxis.renderer.labels.template.verticalCenter = "middle";
        categoryAxis.renderer.labels.template.rotation = 270;
        categoryAxis.tooltip.disabled = true;
        categoryAxis.renderer.minHeight = 110;

        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.renderer.minWidth = 50;

        // Create series
        var series = chart.series.push(new am4charts.ColumnSeries());
        series.sequencedInterpolation = true;
        series.dataFields.valueY = "visits";
        series.dataFields.categoryX = "country";
        series.tooltipText = "[{categoryX}: bold]{valueY}[/]";
        series.columns.template.strokeWidth = 0;

        series.tooltip.pointerOrientation = "vertical";

        series.columns.template.column.cornerRadiusTopLeft = 10;
        series.columns.template.column.cornerRadiusTopRight = 10;
        series.columns.template.column.fillOpacity = 0.8;

        // on hover, make corner radiuses bigger
        var hoverState = series.columns.template.column.states.create("hover");
        hoverState.properties.cornerRadiusTopLeft = 0;
        hoverState.properties.cornerRadiusTopRight = 0;
        hoverState.properties.fillOpacity = 1;

        series.columns.template.adapter.add("fill", (fill, target) => {
            return chart.colors.getIndex(target.dataItem.index);
        })
    </script>

</body>
</html>
