<?php include_once("zz_koneksi_db.php"); ?>
<?php include ("zz_generate_menu.php"); ?>
<?php
	if( $_SESSION["sws_daerah"] == "" || !isset($_SESSION["sws_daerah"]) ) {
		echo "<script>window.location.href = \"reg_login.php\";</script>";
	}
?>
<!DOCTYPE HTML>
<html>

<head>
  <title>.:: ePERDA - Kementerian Dalam Negeri ::.</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>
  
  <script>
  	function checkit() {
		var errorMsg = "";
	
		//Check for a username
		if (document.form1.judul.value==""){ errorMsg += "\n\t - Isi kode token dahulu";}
		if (document.form1.ringkasan.value==""){ errorMsg += "\n\t - Isi keterangan tambahan dahulu";}
		if (document.form1.answer.value!=document.form1.user_answer.value){ errorMsg += "\n\t - Tidak dapat melakukan verifikasi";}
		if (errorMsg != ""){
			msg  = "___________________________________________________________________\n\n";
			msg += "Update data belum bisa dilakukan dikarenakan kesalahan berikut ini.\n";
			msg += "___________________________________________________________________\n\n";
			errorMsg += alert(msg + errorMsg + "\n\n");
			return false;
		}
		
		return true;
	}
  </script>
</head>

<body>
  <div id="main">
    <header>
	  <?php generate_logo(); ?>
      <?php generate_menu(7); ?>
    </header>
    <div id="site_content">
      <div class="gallery"><?php generate_gallery($conn, 950, 150); ?></div>
      <div class="content">
        <h1>e-Register</h1>
        <p>Silakan mengisikan informasi di bawah ini dan mengunggah draft Rancangan Peraturan Daerah yang dimaksud. Draft Rancangan Peraturan Daerah harus dalam format MS  Word dan besaran berkas tidak melebihi 5 MB.<br>
          <br>
          Jika proses Register berhasil, maka sebuah email yang berisi informasi pengajuan Rancangan Peraturan Daerah akan dilayangkan. Untuk itu pastikan alamat e-mail yang terdaftar masih aktif dan dapat diakses.
        </p>
        <?phpphp
          $number_1 = rand(1, 9);
          $number_2 = rand(1, 9);
          $answer = $number_1+$number_2;
        ?>
        <form id="contact" name="form1" action="lengkap_ranperda_confirm.php" method="post" enctype="multipart/form-data" onsubmit="return checkit();">
          <div class="form_settings">
            <p><span>Kode Token</span>
              <input class="contact" type="text" name="judul" placeholder="Isikan kode token terdahulu" /></p>
			<p><span>RANPERDA</span>
              <input class="contact" type="file" name="berkas" value="" /></p>
            <p><span>Berkas Keputusan DPRD</span>
              <input class="contact" type="file" name="berkas1" value="" /></p>
            <p><span>Surat Permohonan</span>
              <input class="contact" type="file" name="berkas2" value="" /></p>              
            <p><span>Keterangan Tambahan</span>
              <textarea class="contact textarea" rows="5" cols="50" name="ringkasan" placeholder="Isikan ringkasan Ranperda"></textarea></p>
            <p style="line-height: 1.7em;">Untuk mencegah email spam, harap isikan jawaban dari pertanyaan di bawah ini:</p>
            <p><span><?phpphp echo $number_1; ?> + <?phpphp echo $number_2; ?> = ?</span><input type="text" name="user_answer" /><input type="hidden" name="answer" value="<?phpphp echo $answer; ?>" /><input type="hidden" name="sc" value="reg_ranperda" /></p>
            <p style="padding-top: 15px"><span>&nbsp;</span><input class="submit" type="submit" name="contact_submitted" value="Kirim" /></p>
          </div>
        </form><br><br><br><br>
      </div>
    </div>
    <?php generate_footer(); ?>
  </div>
  <p>&nbsp;</p>
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript" src="js/image_fade.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
    });
  </script>
</body>
</html>
