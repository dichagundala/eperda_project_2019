<?php include_once("zz_koneksi_db.php"); ?>
<?php include ("zz_generate_menu.php"); ?>
<!DOCTYPE HTML>
<html>

<head>
  <title>.:: ePERDA - Kementerian Dalam Negeri ::.</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>
</head>

<body>
  <div id="main">
    <header>
	  <?php generate_logo(); ?>
      <?php generate_menu(7); ?>
    </header>
    <div id="site_content">
      <div class="gallery"><?php generate_gallery($conn, 950, 150); ?></div>
      <div class="content">
        <h1>Lupa Kata Sandi</h1>
        <p>Silakan memasukkan nama login pada formulir di bawah ini. Jika nama login sudah terdaftar maka sebuah email yang berisi kata sandi yang baru akan dilayangkan ke alamat email pengguna. Untuk itu pastikan alamat email yang terdaftar masih aktif dan dapat diakses.<br>
          <br>
          Jika pengguna masih mengalami kendala dalam mengakses sistem, silakan layangkan pemberitahuan melalui formulir <a href="contact.php">feedback</a>.<br>
        </p>
        <p>Atas perhatiannya diucapkan terima kasih.<br>
        </p>
        <form id="frm_login" action="lupa_sandi_confirm.php" method="post">
          <div class="form_settings">
            <p><span>Nama Login</span>
              <input name="nama" type="text" class="contact" id="nama" size="16" maxlength="16" /><input type="hidden" name="sc" value="reg_ranperda" />
            </p>
            <p>&nbsp;</p>
            <p style="padding-top: 15px"><span>&nbsp;</span>
              <input class="submit" type="submit" name="Submit" value="Reset" />
            </p>
          </div>
        </form>

<br>
<br><br><br>
      </div>
    </div>
    <?php generate_footer(); ?>
  </div>
  <p>&nbsp;</p>
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript" src="js/image_fade.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
    });
  </script>
</body>
</html>
