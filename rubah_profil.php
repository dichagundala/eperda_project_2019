<?php include_once("zz_koneksi_db.php"); ?>
<?php include ("zz_generate_menu.php"); ?>
<?php
	session_start();
	$host  = $_SERVER['HTTP_HOST'];
	$uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
	$ip1 = $_SERVER['HTTP_X_FORWARDED_FOR'];
	$ip2 = $_SERVER['REMOTE_ADDR'];
	$waktu = date("Y-m-d H:i:s");
	$extra = "reg_login.php";
	if( $_SESSION["sws_id"] == "" || !isset($_SESSION["sws_id"]) || $_SESSION["sws_id"] == 0 ) {
		echo "<script>window.location.href=\"pd_login.php\";</script>";
		//header("Location: http://$host$uri/$extra");
		exit;
	}
	$id = $_SESSION["sws_id"];
	$ket = "";
	$hsl = mysqli_query($conn, "select * from tbl_pengguna where id='".$_SESSION["sws_id"]."'");
	if( mysqli_num_rows($hsl) == 0 ) {
		echo "<script>window.location.href=\"pd_login.php\";</script>";
		//header("Location: http://$host$uri/$extra");
		exit;
	}
	if( $_POST["button"] ) {
		$anid = $_POST["id"];
		$notelp1 = addslashes($_POST["notelp1"]);
		$notelp2 = addslashes($_POST["notelp2"]);
		$nohp = addslashes($_POST["nohp"]);
		$email = addslashes($_POST["email"]);
		$jab = addslashes($_POST["jab"]);
		$estr = ", notel1='$notelp1', notel2='$notelp2', nohp='$nohp', jabatan='$jab', email='$email'";
		if( $_POST["nlengkap"] == "" || !isset($_POST["nlengkap"]) ) {
			$ket = "Perubahan profil pengguna tidak dapat dilakukan. Harap mengisi nama lengkap."; 
		} else {
			$nl = addslashes($_POST["nlengkap"]);
		}
		if( $_POST["cbpwd"] ) {
			//ganti pwd
			if( $_POST["pwd_lama"] == "" || !isset($_POST["pwd_lama"]) ) { $pwdlama = ""; } else { $pwdlama = $_POST["pwd_lama"]; }
			if( $_POST["pwd_baru"] == "" || !isset($_POST["pwd_baru"]) ) { $pwdbaru1 = ""; } else { $pwdbaru1 = $_POST["pwd_baru"]; }
			if( $_POST["pwd_baru1"] == "" || !isset($_POST["pwd_baru1"]) ) { $pwdbaru2 = ""; } else { $pwdbaru2 = $_POST["pwd_baru1"]; }
			$hsl = mysqli_query($conn, "select * from tbl_pengguna where id='$id' and pwd=password('$pwdlama')");
			if( mysqli_num_rows($hsl) == 0 ) {
				$ket = "Perubahan profil pengguna tidak dapat dilakukan. Kata sandi lama tidak dapat diverifikasi.";
			} else {
				if( $pwdbaru1 != $pwdbaru2 ) {
					$ket = "Perubahan profil pengguna tidak dapat dilakukan. Kata sandi baru tidak dapat dikonfirmasi.";
				} else {
					mysqli_query($conn, "update tbl_pengguna SET nlengkap='$nl' $estr, pwd=password('$pwdbaru1') where id='$id'");
					$ket = "Perubahan profil berhasil dilakukan. Perubahan akan berefek ketika masuk kembali ke halaman terbatas";
				}
			}
		} else {
			mysqli_query($conn, "update tbl_pengguna SET nlengkap='$nl' $estr where id='$id'");
			$ket = "Perubahan profil berhasil dilakukan. Perubahan akan berefek ketika masuk kembali ke halaman terbatas";
		}
	}
	
	
	$hsl = mysqli_query($conn, "select * from tbl_pengguna where id='".$_SESSION["sws_id"]."'");	
	$B = mysqli_fetch_array($hsl);
	$id = $B[0];
	$nlog = $B[1];
	$nlengkap = $B[2];
	$tingkat = $B[4];
	$prov = $B[5];
	$kab = $B[6];
	$email = $B["email"];
	$notelp1 = $B["notel1"];
	$notelp2 = $B["notel2"];
	$nohp = $B["nohp"];
	$jab = $B["jabatan"];
	switch ( $tingkat ) {
		case 10: 
			$hsl = mysqli_query($conn, "select nama from tbl_provinsi where kode='$prov'");
			$B = mysqli_fetch_array($hsl);
			$tk = "Pengguna dengan otoritas petugas Provinsi ".$B[0];
			break;
		case 11:
			$hsl = mysqli_query($conn, "select nama from tbl_kab where kode_kab='$kab'");
			$B = mysqli_fetch_array($hsl);
			$tk = "Pengguna dengan otoritas petugas Kabupaten/Kota ".$B[0];
			$hsl = mysqli_query($conn, "select nama from tbl_prov where kode ='$prov'");
			$B = mysqli_fetch_array($hsl);
			$tk .= " - Provinsi ".$B[0];
			break;
	}


?>


<!DOCTYPE HTML>
<html>

<head>
  <title>.:: ePERDA - Kementerian Dalam Negeri ::.</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>
  <script>
  	function checkit() {
		var errorMsg = "";
	
		//Check for a username
		if (document.form1.nlengkap.value==""){ errorMsg += "\n\t - Isi nama lengkap dahulu";}
		if (document.form1.jab.value==""){ errorMsg += "\n\t - Isi jabatan dahulu";}
		if (document.form1.email.value==""){ errorMsg += "\n\t - Isi email dahulu";}
		if (document.form1.notelp1.value==""){ errorMsg += "\n\t - Isi nomor telpon dahulu";}
		if (document.form1.nohp.value==""){ errorMsg += "\n\t - Isi nomor HP dahulu";}
		if( document.form1.cbpwd.checked) {
			if (document.form1.pwd_baru.value==""){ errorMsg += "\n\t - Kata sandi tidak boleh kosong";}
			if (document.form1.pwd_baru.value!=document.form1.pwd_baru1.value){ errorMsg += "\n\t - Kata sandi tidak dapat diverifikasi";}
		}
		
		if (errorMsg != ""){
			msg  = "___________________________________________________________________\n\n";
			msg += "Perubahan profil belum bisa dilakukan dikarenakan kesalahan berikut ini.\n";
			msg += "___________________________________________________________________\n\n";
			errorMsg += alert(msg + errorMsg + "\n\n");
			return false;
		}
		
		return true;
	}
	</script>
</head>

<body>
  <div id="main">
    <header>
      <?php generate_logo(); ?>
      <?php generate_menu(5); ?>
    </header>
    <div id="site_content">
      <div class="gallery"><?php generate_gallery($conn, 950, 150); ?></div>
      <div class="content_webgis">
        <h1><strong>Perubahan Profil Pengguna</strong></h1>
        <form name="form1" method="post" action="" onSubmit="return checkit();">
          <table width="95%" border="0" cellspacing="0" cellpadding="3">
            <tr>
              <td width="23%">Nama Login</td>
              <td width="2%">:</td>
              <td width="75%"><?php echo $nlog; ?><input name="id" type="hidden" value="<?php echo $id; ?>"></td>
            </tr>
            <tr>
              <td>Nama Lengkap</td>
              <td>:</td>
              <td><input name="nlengkap" type="text" id="nlengkap" value="<?php echo $nlengkap; ?>" size="50" maxlength="50"></td>
            </tr>
            <tr>
              <td>Jabatan</td>
              <td>:</td>
              <td><input name="jab" type="text" id="jab" value="<?php echo $jab; ?>" size="50" maxlength="50"></td>
            </tr>
            <tr valign="top">
              <td>Otoritas</td>
              <td>:</td>
              <td><?php echo $tk; ?></td>
            </tr>
            <tr>
              <td>Alamat Email</td>
              <td>:</td>
              <td><input name="email" type="text" id="email" value="<?php echo $email; ?>" size="40" maxlength="40"></td>
            </tr>
            <tr>
              <td>No. Telpon</td>
              <td>:</td>
              <td><input name="notelp1" type="text" id="notelp1" value="<?php echo $notelp1; ?>">
              <input name="notelp2" type="text" id="notelp2" value="<?php echo $notelp2; ?>"></td>
            </tr>
            <tr>
              <td>No. HP</td>
              <td>:</td>
              <td><input name="nohp" type="text" id="nohp" value="<?php echo $nohp; ?>"></td>
            </tr>
            <tr>
              <td align="right"><input name="cbpwd" type="checkbox" id="cbpwd" value="1"></td>
              <td>&nbsp;</td>
              <td>Merubah Kata Sandi</td>
            </tr>
            <tr>
              <td>Kata Sandi Lama</td>
              <td>:</td>
              <td><input type="password" name="pwd_lama" id="pwd_lama"></td>
            </tr>
            <tr>
              <td>Kata Sandi Baru</td>
              <td>:</td>
              <td><input type="password" name="pwd_baru" id="pwd_baru"></td>
            </tr>
            <tr>
              <td>Konfirmasi Kata Sandi Baru</td>
              <td>:</td>
              <td><input type="password" name="pwd_baru1" id="pwd_baru1"></td>
            </tr>
            <tr>
              <td colspan="3" align="center"><br>
                <br>
                <span style="color: #F00; font-weight: bold;"><?php echo $ket; ?></span><br>
              <br>
              <input type="submit" name="button" id="button" value="Proses"></td>
            </tr>
          </table>
        </form>
        <p>&nbsp;</p>
      </div>
    </div>
    <?php generate_footer(); ?>
  </div>
  <p>&nbsp;</p>
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript" src="js/image_fade.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
    });
  </script>
</body>
</html>
