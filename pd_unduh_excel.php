<?php include_once("zz_koneksi_db.php"); ?>
<?php include ("zz_generate_menu.php"); ?>
<!DOCTYPE HTML>
<html>

<head>
  <title>.:: ePERDA - Kementerian Dalam Negeri ::.</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>
</head>

<body>
  <div id="main">
    <header>
      <?php generate_logo(); ?>
      <?php generate_menu(1); ?>
    </header>
    <div id="site_content">
      <div class="gallery"><?php generate_gallery($conn, 950, 150); ?></div>
      <div class="content">
        <h1>Pengunduhan Template MS Excel</h1>
        <p>Untuk memudahkan pengguna dalam melakukan pemasukan data, di bawah ini diberikan file template Excel. Setelah pengguna melakukan pengisian data, pengguna diminta untuk melakukan pengunggahan data tersebut sehingga dapat diintegrasikan ke dalam basis data MORENA.<br>
          <br>
          Silakan klik tautan di bawah ini untuk memulai proses pengunduhan file template Excel.
<br>
          <br>
          1. <a href="pd_get_excel.php?a=1">File template Excel untuk memasukkan Nett Rent Kawasan Lahan Sawah</a><br>
          2. 
          <a href="pd_get_excel.php?a=2">File template Excel untuk memasukkan Nett Rent Kawasan Pemukiman</a><br>
          3. <a href="pd_get_excel.php?a=3">File template Excel untuk memasukkan Nett Rent Kawasan Industri </a><br>
          <br>
        </p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p><br>
          <br>
        </p>
      </div>
    </div>
    <?php generate_footer(); ?>
  </div>
  <p>&nbsp;</p>
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript" src="js/image_fade.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
    });
  </script>
</body>
</html>
