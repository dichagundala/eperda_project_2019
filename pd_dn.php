<?php include_once("zz_koneksi_db.php"); ?>
<?php include ("zz_generate_menu.php"); ?>
<?php
	session_start();
	$host  = $_SERVER['HTTP_HOST'];
	$uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
	$ip1 = $_SERVER['HTTP_X_FORWARDED_FOR'];
	$ip2 = $_SERVER['REMOTE_ADDR'];
	$waktu = date("Y-m-d H:i:s");
	$extra = "pd_login.php";
	if( $_SESSION["sws_id"] == "" || !isset($_SESSION["sws_id"]) || $_SESSION["sws_id"] == 0 ) {
		echo "<script>window.location.href=\"pd_login.php\";</script>";
		//header("Location: http://$host$uri/$extra");
		exit;
	}
	if( $_SESSION["sws_tingkat"] == 99 || $_SESSION["sws_tingkat"] == 1 || $_SESSION["sws_tingkat"] == 2 ) {
	} else {
		echo "<script>window.location.href=\"pd_login.php\";</script>";
		//header("Location: http://$host$uri/$extra");
		exit;
	}
?>


<!DOCTYPE HTML>
<html>

<head>
  <title>.:: ePERDA - Kementerian Dalam Negeri ::.</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>

<script language="javascript">

function go_edit(a) {
	document.form_edit.id.value = a;
	document.form_edit.submit();
}

function go_hapus(a) {
	var ans; 
	ans=window.confirm('Yakin akan menon-aktifkan pengguna ini?');
	if( ans == true ) {
		document.form_hapus.id.value = a;
		document.form_hapus.submit();
	}
}

var kab_arr = new Array();
var kab_opt = new Array();
var acounter = 0;
kab_arr[0] = new Array(" --- Pilih --- ");
kab_opt[0] = new Array('0');
<?php
	$hsl = mysqli_query($conn, "select kode from tbl_prov");
	while( $B = mysqli_fetch_array($hsl) ) {
		$kdprop = $B[0];
		$hsl1 = mysqli_query($conn, "select kode_kab, nama from tbl_kab where kode_prov='$kdprop' order by kode_kab");
		$calar = "\" --- Pilih --- \"";
		$oplar = "\"0\"";
		while( $B1 = mysqli_fetch_array($hsl1) ) {
			$kdkab = $B1[0];
			if( $calar == "" ) { $calar = "\"".$B1[1]."\""; } else { $calar .= ",\"".$B1[1]."\""; }
			if( $oplar == "" ) { $oplar = "\"".$B1[0]."\""; } else { $oplar .= ",\"".$B1[0]."\""; }
		}
?>
		kab_arr[<?php echo $kdprop; ?>] = new Array(<?php echo $calar; ?>);
		kab_opt[<?php echo $kdprop; ?>] = new Array(<?php echo $oplar; ?>);
<?php
	}
?>

function change_kab(combo1){
	var comboValue = combo1.value;
	document.forms["form1"].elements["kab"].options.length=0;
	for (var i=0;i<kab_arr[comboValue].length;i++){
		var option = document.createElement("option");
		option.setAttribute('value',kab_opt[comboValue][i]);
		option.innerHTML = kab_arr[comboValue][i];
		document.forms["form1"].elements["kab"].appendChild(option);
	}
}


function change_01_kab(combo1, indikator){
	var selin = 0;
	var comboValue = document.forms["form1"].elements[combo1].value;
	document.forms["form1"].elements["kab"].options.length=0;
	for (var i=0;i<kab_arr[comboValue].length;i++){
		var option = document.createElement("option");
		option.setAttribute('value',kab_opt[comboValue][i]);
		option.innerHTML = kab_arr[comboValue][i];
		document.forms["form1"].elements["kab"].appendChild(option);
		if( kab_opt[comboValue][i] == indikator ) { selin = i; }
	}
	document.forms["form1"].elements["kab"].options.selectedIndex=selin;
}


</script>  
  
</head>

<body>
  <div id="main">
    <header>
      <?php generate_logo(); ?>
      <?php generate_menu(5); ?>
    </header>
    <div id="site_content">
      <div class="gallery"><?php generate_gallery($conn, 950, 150); ?></div>
      <div class="content">
        <h1>Notifikasi Rancangan PERDA</h1>
        <table width="100%" border="0" cellspacing="0" cellpadding="3" style="border-top: 2px solid #000;" >
<?php
	$hsl = mysqli_query($conn, "select distinct reg from tbl_region");
	while( $B = mysqli_fetch_array($hsl) ) {
		$areg = $B[0];
		$strreg = "";
		$hsl1 = mysqli_query($conn, "select prov from tbl_region where reg='$areg'");
		while( $B1 = mysqli_fetch_array($hsl1) ) {
			if( $strreg == "" ) { $strreg = " prov IN ('".$B1[0]."'"; } else { $strreg .= ",'".$B1[0]."'"; }
		}
		$strreg .= ") ";
		$hariini = date("Y-m-d");
		$strwaktu = " AND (BETWEEN '$hariini 00:00:00' and '$hariini 23:59:59')";
		$qhariini = "select count(id) from tbl_ranperda where $strreg $strwaktu";
		$hsl1 = mysqli_query($conn, $qhariini);
		if( mysqli_num_rows($hsl1) == 0 ) {
			$jhariini = 0;
		} else {
			$B1 = mysqli_fetch_array($hsl1);
			if( $B1[0] == "" || !isset($B1[0]) ) { $jhariini = 0; } else { $jhariini = $B1[0]; }
		}
		$qhariini = "select count(id) from tbl_ranperda where $strreg";
		$hsl1 = mysqli_query($conn, $qhariini);
		if( mysqli_num_rows($hsl1) == 0 ) {
			$jtotal = 0;
		} else {
			$B1 = mysqli_fetch_array($hsl1);
			if( $B1[0] == "" || !isset($B1[0]) ) { $jtotal = 0; } else { $jtotal = $B1[0]; }
		}
		$qhariini = "select count(id) from tbl_ranperda where $strreg and sproses = 1";
		$hsl1 = mysqli_query($conn, $qhariini);
		if( mysqli_num_rows($hsl1) == 0 ) {
			$j1 = 0;
		} else {
			$B1 = mysqli_fetch_array($hsl1);
			if( $B1[0] == "" || !isset($B1[0]) ) { $j1 = 0; } else { $j1 = $B1[0]; }
		}
		$qhariini = "select count(id) from tbl_ranperda where $strreg and sproses = 2";
		$hsl1 = mysqli_query($conn, $qhariini);
		if( mysqli_num_rows($hsl1) == 0 ) {
			$j2 = 0;
		} else {
			$B1 = mysqli_fetch_array($hsl1);
			if( $B1[0] == "" || !isset($B1[0]) ) { $j2 = 0; } else { $j2 = $B1[0]; }
		}
		$qhariini = "select count(id) from tbl_ranperda where $strreg and sproses = 3";
		$hsl1 = mysqli_query($conn, $qhariini);
		if( mysqli_num_rows($hsl1) == 0 ) {
			$j3 = 0;
		} else {
			$B1 = mysqli_fetch_array($hsl1);
			if( $B1[0] == "" || !isset($B1[0]) ) { $j3 = 0; } else { $j3 = $B1[0]; }
		}
		if( $_SESSION["sws_tingkat"] == 99 || $_SESSION["sws_tingkat"] == 1 ) {
			$lihat = "<a href=\"pd_dreg.php?a=$areg\">Lihat Daftar</a>";
		} else {
			if( $_SESSION["sws_prov"] == $areg ) {
				$lihat = "<a href=\"pd_dreg.php?a=$areg\">Lihat Daftar</a>";
			} else {
				$lihat = "";
			}
		}
?>		
		
          <tr>
            <td><h2>SEKSI WILAYAH <?php echo $areg; ?></h2></td>
          </tr>
          <tr>
            <td><table class="sws_table" width="100%" border="0" cellspacing="0" cellpadding="3" style="border-top: 2px solid #FF0000; border-bottom: 2px solid #000">
              <tr>
                <td width="38%">Jumlah Rancangan PERDA hari ini</td>
                <td width="62%"><?php echo number_format($jhariini); ?></td>
              </tr>
              <tr>
                <td>Jumlah Rancangan PERDA total</td>
                <td><?php echo number_format($jtotal); ?></td>
              </tr>
              <tr>
                <td>Menunggu fasilitasi</td>
                <td><?php echo number_format($j1); ?></td>
              </tr>
              <tr>
                <td>Menunggu revisi atau perbaikan</td>
                <td><?php echo number_format($j2); ?></td>
              </tr>
              <tr>
                <td>Disetujui</td>
                <td><?php echo number_format($j3); ?></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td><?php echo $lihat; ?></td>
              </tr>
            </table></td>
          </tr>
<?php
	}
?>
        </table>
        <br>
        <p>&nbsp;</p>
      </div>
    </div>
    <?php generate_footer(); ?>
  </div>
  <p>&nbsp;</p>
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript" src="js/image_fade.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
    });
  </script>
  
<form action="pd_man_user_edit.php" method="post" name="form_edit">
<input name="id" type="hidden" value="" />
</form>
<form action="pd_man_user_del.php" method="post" name="form_hapus">
<input name="id" type="hidden" value="" />
</form>  
  
</body>
</html>
