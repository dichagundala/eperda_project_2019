<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
  <script type="text/javascript" src="amcharts/amcharts.js"></script>
  <script type="text/javascript" src="amcharts/serial.js"></script>
  <script type="text/javascript" src="amcharts/themes/dark.js"></script>
  <script src="amcharts/plugins/dataloader/dataloader.min.js"></script>
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
    });
	
	function update_status_online() {
		$.get("zz_update_chat.php", function(data) {
		});
	}
	update_status_online();
	var myVar6 = setInterval(update_status_online, 20000);
	
	
	AmCharts.loadJSON = function(url) {
		if (window.XMLHttpRequest) {
    		// IE7+, Firefox, Chrome, Opera, Safari
    		var request = new XMLHttpRequest();
  		} else {
	    // code for IE6, IE5
    		var request = new ActiveXObject('Microsoft.XMLHTTP');
  		}
  		request.open('GET', url, false);
  		request.send();
  		// parse adn return the output
		return eval(request.responseText);
	};

	var chartData1 = AmCharts.loadJSON('zz_jml_perda.php');
	var chart1 = AmCharts.makeChart("chart1div", {
		"type": "serial",
		"titles": [{
			"text" : "Jumlah PERDA Pemerintah Daerah Tingkat Provinsi yang terdata pada ePERDA",
			"size": 10
		}],
		"dataProvider": chartData1,
    	"pathToImages": "http://cdn.amcharts.com/lib/3/images/",
    	"categoryField": "prov",
		"balloon": {
			
		},
		"categoryAxis": { 
			"gridPosition": "start",
			"fontSize": 8,
			"labelRotation": 45 },
			"valueAxis": {
				"labelsEnabled": true
			},
		    "graphs": [ {
				"valueField": "jml",
				"fillAlphas": 0.8,
				"fontSize": 8,
				"labelText": "[[jml]]",
				"fillColor": "#8d0404",
				"lineColor": "#8d0404",
				"type": "column",
				"balloonFunction": "getnilai()",
				"lineThickness ": 4
			}]
	});
	chart1.addListener("clickGraphItem", handleClick);
	
	function getnilai(graphDataItem, graph){
		return "Test";
	}
	
	
	function handleClick(event) {
		document.form1.nm.value = event.item.category;
		document.form1.submit();
	}
  </script>
</head>

<body><div id="chart1div" style="width: 99%; height: 225px;"></div>

<form action="zz_chart_perda_kab.php" method="post" name="form1">
<input name="nm" type="hidden" value="" />
</form>
</body>
</html>