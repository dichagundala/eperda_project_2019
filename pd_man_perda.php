<?php include_once("zz_koneksi_db.php"); ?>
<?php include ("zz_generate_menu.php"); ?>
<?php
	session_start();
	$host  = $_SERVER['HTTP_HOST'];
	$uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
	$ip1 = $_SERVER['HTTP_X_FORWARDED_FOR'];
	$ip2 = $_SERVER['REMOTE_ADDR'];
	$waktu = date("Y-m-d H:i:s");
	$extra = "pd_login.php";
	if( $_SESSION["sws_id"] == "" || !isset($_SESSION["sws_id"]) || $_SESSION["sws_id"] == 0 ) {
		echo "<script>window.location.href=\"pd_login.php\";</script>";
		//header("Location: http://$host$uri/$extra");
		exit;
	}
	if( $_SESSION["sws_tingkat"] == 99 || $_SESSION["sws_tingkat"] == 1 || $_SESSION["sws_tingkat"] == 2 ) {
	} else {
		echo "<script>window.location.href=\"pd_login.php\";</script>";
		//header("Location: http://$host$uri/$extra");
		exit;
	}
	if( $_POST["proses"] ) {
		$a = $_POST["a"];
		$b = $_POST["b"];
		$c = $_POST["c"];
		$d = $_POST["d"];
	} else {
		if( $_GET["a"] == "" || !isset($_GET["a"]) ) { $a = 0; } else { $a = $_GET["a"]; }
		$b = 0;
		$c = 0;
		$d = 1;
	}
	$rstr = " ok=0 "; 
	if( $b == 0 ) { $ostr = " ORDER BY id"; }
	if( $b == 1 ) { $ostr = " ORDER BY prov"; }
	if( $b == 2 ) { $ostr = " ORDER BY judul"; }
	if( $b == 3 ) { $ostr = " ORDER BY nomer"; }
	if( $b == 4 ) { $ostr = " ORDER BY tkt"; }
	if( $b == 5 ) { $ostr = " ORDER BY tgl"; }
	if( $c == 0 ) { $ostr .= " DESC"; } else { $ostr .= " ASC"; }
	
	$hsl = mysqli_query($conn, "select count(id) from tbl_upload_perda where $rstr");
	if( mysqli_num_rows($hsl) == 0 ) { $jumlah_record = 0; }
	else {
		$B = mysqli_fetch_array($hsl);
		if( $B[0] == "" || !isset($B[0]) ) { $jumlah_record = 0; } else { $jumlah_record = $B[0]; }
	}
	$perhal = 10;
	$maxhal = floor($jumlah_record / $perhal);
	if( $maxhal * $perhal != $jumlah_record ) { $maxhal += 1; }
	$str_limit = " LIMIT ".(($d - 1)*$perhal).", ".$perhal;
	$fixquery = "select * from tbl_upload_perda where $rstr $ostr $str_limit";
	//echo "FIXQUERY --> ".$fixquery;
	
	
	function get_waktu($a) {
		$dum = explode(" ",$a);
		$dum1 = explode("-", $dum[0]);
		$ostr = $dum1[2];
		if( $dum1[1] == "01" ) { $ostr .= " Januari ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "02" ) { $ostr .= " Februari ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "03" ) { $ostr .= " Maret ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "04" ) { $ostr .= " April ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "05" ) { $ostr .= " Mei ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "06" ) { $ostr .= " Juni ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "07" ) { $ostr .= " Juli ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "08" ) { $ostr .= " Agustus ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "09" ) { $ostr .= " September ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "10" ) { $ostr .= " Oktober ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "11" ) { $ostr .= " November ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "12" ) { $ostr .= " Desember ".$dum1[0]." ".$dum[1]; }
		
		$tnow = time();
		$tunduh = strtotime($a);
		$time = $tnow - $tunduh;
		$time = ($time < 1) ? 1 : $time;
		$tokens = array(
			31536000 => 'tahun',
			2592000  => 'bulan',
			604800   => 'minggu',
			86400    => 'hari',
			3600     => 'jam',
			60		 => 'menit',
			1		 => 'detik');
			
		foreach($tokens as $unit => $text ) {
			if( $time < $unit ) continue;
			$nou = floor($time / $unit);
			$et = $nou." ".$text;
			break;
		}
		return $ostr." (".$et." yang lalu)";
	}
?>


<!DOCTYPE HTML>
<html>

<head>
  <title>.:: ePERDA - Kementerian Dalam Negeri ::.</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>

<script language="javascript">

function rubah_data() {
	document.form_tampil.b.value = document.form1.b.value;
	document.form_tampil.c.value = document.form1.c.value;
	document.form_tampil.d.value = document.form1.d.value;
	document.form_tampil.submit();
}

function go_edit(a) {
	document.form_edit.id.value = a;
	document.form_edit.submit();
}

function setuju(a) {
	var ans; 
	ans=window.confirm('Yakin akan menyetujui PERDA ini?');
	if( ans == true ) {
		document.form_setuju.id.value = a;
		document.form_setuju.jns.value = 1;
		document.form_setuju.submit();
	}
}

function tolak(a) {
	var ans; 
	ans=window.confirm('Yakin akan menolak PERDA ini?');
	if( ans == true ) {
		document.form_setuju.id.value = a;
		document.form_setuju.jns.value = 2;
		document.form_setuju.submit();
	}
}


function go_history(a, b) {
	document.form_history.id.value = a;
	document.form_history.token.value = b;
	document.form_history.submit();
}

function unduh(a) {
	document.form_unduh.id.value = a;
	document.form_unduh.submit();
}

function review(a) {
	document.form_review.id.value = a;
	document.form_review.submit();
}

function go_hapus(a) {
	var ans; 
	ans=window.confirm('Yakin akan menon-aktifkan pengguna ini?');
	if( ans == true ) {
		document.form_hapus.id.value = a;
		document.form_hapus.submit();
	}
}



</script>  
  
</head>

<body>
  <div id="main">
    <header>
      <?php generate_logo(); ?>
      <?php generate_menu(5); ?>
    </header>
    <div id="site_content">
      <div class="gallery"><?php generate_gallery($conn, 950, 150); ?></div>
      <div class="content_webgis_up">
        <h1>Daftar Pengunggahan PERDA</h1>
<?php
	if( $jumlah_record == 0 ) {
?>		     
	<strong><br>
<br>
        TIDAK ADA DATA</strong><br>
        <br>
        <br>
<br>
        <br>
        <br>
<br><br><br><br>
<?php
	} else {
?>		
        <br>
        <br>
        <form name="form1" method="post" action="">
          <table width="100%" border="0" cellspacing="0" cellpadding="3">
            <tr>
              <td width="44%">Ditemukan <?php echo number_format($jumlah_record); ?>  PERDA</td>
              <td width="41%" align="right">Urut berdasar
                <select name="b" id="b" onChange="rubah_data();">
                  <option value="0" <?php if( $b == 0 ) { ?> selected <?php } ?>>ID</option>
                  <option value="1" <?php if( $b == 1 ) { ?> selected <?php } ?>>Provinsi</option>
                  <option value="2" <?php if( $b == 2 ) { ?> selected <?php } ?>>Judul PERDA</option>
                  <option value="3" <?php if( $b == 3 ) { ?> selected <?php } ?>>Nomor PERDA</option>
                  <option value="5" <?php if( $b == 5 ) { ?> selected <?php } ?>>Tanggal Disahkan</option>
              </select>
                <select name="c" id="c" onChange="rubah_data();">
                  <option value="0" <?php if( $c == 0 ) { ?> selected <?php } ?>>Menurun</option>
                  <option value="1" <?php if( $c == 1 ) { ?> selected <?php } ?>>Meningkat</option>
              </select></td>
              <td width="15%" align="right">Ke Halaman 
                <select name="d" id="d" onChange="rubah_data();">
<?php
		for( $iix = 1; $iix <= $maxhal; $iix++ ) {
?>
					<option value="<?php echo $iix; ?>" <?php if( $iix == $d ) { ?> selected <?php } ?>><?php echo $iix; ?></option>
<?php
		}
?>                
              </select></td>
            </tr>
          </table>
        </form>
        <br><br>
<?php
		$hsl = mysqli_query($conn, $fixquery);
		while( $B = mysqli_fetch_array($hsl) ) {
			$anid = $B["id"];
			$ada = $B["ada"];
			if( $ada == 1 ) {
				$berkas = "<td colspan=\"3\" onClick=\"unduh('$anid');\" onMouseOver=\"this.style.cursor='pointer';\"><u>Unduh</u></td>";
			} else {
				$berkas = "<td colspan=\"3\">Tidak ada</td>";
			}
			$judul = $B["judul"];
			$ringkasan = str_replace (array("\r\n", "\n", "\r"), '<br>', $B["ringkasan"]);
			$nomor = $B1["nomer"]." Tahun ".$B["tahun"];
			
			$hsl1 = mysqli_query($conn, "select nama from tbl_prov where kode='".$B["prov"]."'");
			$B1 = mysqli_fetch_array($hsl1);
			$wilayah = $B1[0];
			if( $B["kabkota"] != 0 ) {
				$hsl1 = mysqli_query($conn, "select nama from tbl_kab where kode_kab='".$B["kabkota"]."'");
				$B1 = mysqli_fetch_array($hsl1);
				$wilayah .= " / ".$B1[0];
			}
?>
 <table width="100%" border="0" cellspacing="0" cellpadding="3">
          <tr>
            <td><table class="sws_table" width="100%" border="0" cellspacing="0" cellpadding="10" style="border-top: 2px solid #000;">
              <tr valign="top">
                <td width="21%">Judul</td>
                <td width="2%">&nbsp;</td>
                <td colspan="3"><strong><?php echo $B["judul"]; ?></strong></td>
              </tr>
              <tr valign="top">
                <td>Nomor PERDA</td>
                <td>&nbsp;</td>
                <td colspan="3"><?php echo $nomor; ?></td>
              </tr>
              <tr valign="top">
                <td>Tanggal Disahkan</td>
                <td>&nbsp;</td>
                <td colspan="3"><?php echo get_waktu($B["tgl"]); ?></td>
              </tr>
              <tr valign="top">
                <td>Provinsi / Kabupaten/Kota</td>
                <td>&nbsp;</td>
                <td colspan="3"><?php echo $wilayah; ?></td>
              </tr>
              <tr valign="top">
                <td>Berkas PERDA</td>
                <td>&nbsp;</td>
                <?php echo $berkas; ?>
              </tr>
              <tr valign="top">
                <td>Ringkasan</td>
                <td>&nbsp;</td>
                <td colspan="3"><?php echo $ringkasan; ?></td>
              </tr>
              <tr valign="top">
              	<td onClick="setuju('<?php echo $anid; ?>');" onMouseOver="this.style.cursor='pointer';"><u>Setujui</u></td>
                <td>&nbsp;</td>
                <td align="right" onClick="tolak('<?php echo $anid; ?>');" onMouseOver="this.style.cursor='pointer';"><u>Tolak</u></td>
              </tr>
            </table></td>
          </tr>
        </table>              
<?php
		}
?>

        <p>&nbsp;</p>
<?php
	}
?>
        
      </div>
    </div>
    <?php generate_footer(); ?>
  </div>
  <p>&nbsp;</p>
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript" src="js/image_fade.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
    });
  </script>
  
<form action="pd_dreg.php" method="post" name="form_tampil">
<input name="a" type="hidden" value="<?php echo $a; ?>" />
<input name="b" type="hidden" value="" />
<input name="c" type="hidden" value="" />
<input name="d" type="hidden" value="" />
<input name="proses" type="hidden" value="1" />
</form>
<form action="pd_review.php" method="post" name="form_review">
<input name="id" type="hidden" value="" />
</form>
<form action="pd_man_perda_confirm.php" method="post" name="form_setuju">
<input name="id" type="hidden" value="" />
<input name="jns" type="hidden" value="" />
</form>

<form action="pd_history.php" method="post" name="form_history">
<input name="id" type="hidden" value="" />
<input name="token" type="hidden" value="" />
</form>
<form action="pd_unduh_perda.php" method="post" name="form_unduh">
<input name="id" type="hidden" value="" />
</form>  
  
</body>
</html>
