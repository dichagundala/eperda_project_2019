
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>YoxView demo #2 - customizing with options</title>
		<script type="text/javascript" src="yoxview/yoxview-init.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$("#yoxview").yoxview({
					lang: "ja",
					backgroundColor: "#000099",
					autoPlay: true
				});
			});
		</script>
	</head>

<body><div class="yoxview">
			<a href="images/1.jpg"><img src="images/1.jpg" alt="1" title="First image" width="200" /></a>
			<a href="images/2.jpg"><img src="images/2.jpg" alt="2" title="Second image" width="200" /></a>
            <a href="images/3.jpg"><img src="images/3.jpg" alt="3" title="First image" width="200" /></a>
			<a href="images/4.jpg"><img src="images/4.jpg" alt="4" title="Second image" width="200" /></a>
            <a href="images/5.jpg"><img src="images/5.jpg" alt="5" title="First image" width="200" /></a>
			<a href="images/6.jpg"><img src="images/6.jpg" alt="6" title="Second image" width="200" /></a>
            <a href="images/7.jpg"><img src="images/7.jpg" alt="7" title="First image" width="200" /></a>
            <a href="images/8.jpg"><img src="images/8.jpg" alt="8" title="Second image" width="200" /></a>
            <a href="images/9.jpg"><img src="images/9.jpg" alt="9" title="First image" width="200" /></a>
		  </div>
</body>
</html>