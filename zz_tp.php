<?php
	$a = $_GET["a"];
	$akml = "http://www.geostech.org/morena/tkml/".$a.".kml";
?>
<!DOCTYPE HTML>
<html>

<head>
  <title>.:: ePERDA - Kementerian Dalam Negeri ::.</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>
  
  <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyC61ki0hlbvrqI96_F2EdmcZoScN23xQF8&sensor=false"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript" src="js/tooltip.js"></script>
<script type="text/javascript" src="geoXML3.js"></script>
<script type="text/javascript" src="http://google-maps-utility-library-v3.googlecode.com/svn/trunk/infobubble/src/infobubble.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	initialize();
});

function initialize() {
	var geoXmlDoc = null;
	var map = null;
	jQuery(document).ready(function () {
		var mapOptions = {
    		center: new google.maps.LatLng( -4,118),
			zoom: 4,
			minZoom: 2,
			scrollwheel: false,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};
    	map = new google.maps.Map(document.getElementById("googleMap"), mapOptions);	
		
		

		//var geoXml = new geoXML3.parser({
        //	map: map,
        //	singleInfoWindow: true
      	//});
  		//geoXml.parseKmlString('<?php echo $akml; ?>');
		var ctaLayer = new google.maps.KmlLayer({
    		url: '<?php echo $akml; ?>'
  		});
  		ctaLayer.setMap(map);
	});
	

}
</script>

  
</head>

<body onload="initialize()">
  <div id="main">
    <div id="site_content">
      <div class="content_webgis_up">
	  <br>
		  <br>
      </div>      
      <div class="content_webgis">
      	<div id="googleMap" style="height:600px; color:#000000"></div> 
	  </div>
    </div>
    <?php generate_footer(); ?>
  </div>
  <p>&nbsp;</p>
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript" src="js/image_fade.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
    });
  </script>
</body>
</html>
