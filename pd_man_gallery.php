<?php include_once("zz_koneksi_db.php"); ?>
<?php include ("zz_generate_menu.php"); ?>
<?php
	session_start();
	$host  = $_SERVER['HTTP_HOST'];
	$uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
	$ip1 = $_SERVER['HTTP_X_FORWARDED_FOR'];
	$ip2 = $_SERVER['REMOTE_ADDR'];
	$waktu = date("Y-m-d H:i:s");
	$extra = "pd_login.php";
	if( $_SESSION["sws_id"] == "" || !isset($_SESSION["sws_id"]) || $_SESSION["sws_id"] == 0 ) {
		echo "<script>window.location.href=\"pd_login.php\";</script>";
		//header("Location: http://$host$uri/$extra");
		exit;
	}
	if( $_SESSION["sws_tingkat"] != 99 ) {
		echo "<script>window.location.href=\"pd_login.php\";</script>";
		//header("Location: http://$host$uri/$extra");
		exit;
	}
?>


<!DOCTYPE HTML>
<html>

<head>
  <title>.:: ePERDA - Kementerian Dalam Negeri ::.</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>

<script language="javascript">

function go_edit(a, b) {
	document.form_edit.id.value = a;
	document.form_edit.token.value = b;
	document.form_edit.submit();
}

function go_hapus(a) {
	var ans; 
	ans=window.confirm('Yakin akan menghapus image ini?');
	if( ans == true ) {
		document.form_hapus.id.value = a;
		document.form_hapus.submit();
	}
}


</script>  
  
</head>

<body>
  <div id="main">
    <header>
      <?php generate_logo(); ?>
      <?php generate_menu(5); ?>
    </header>
    <div id="site_content">
      <div class="gallery"><?php generate_gallery($conn, 950, 150); ?></div>
      <div class="content_webgis">
        <h1>Manajemen Gallery</h1><form action="pd_man_gallery_confirm.php" method="post" enctype="multipart/form-data" name="form1">
          <table width="100%" border="0" cellspacing="0" cellpadding="3">
            <tr>
              <td width="17%">Nama berkas</td>
              <td width="3%">:</td>
              <td width="80%"><input type="file" name="berkas" id="berkas">
              (pastikan ukuran image adalah 950 x 150 pixel)</td>
            </tr>
            <tr>
              <td colspan="3" align="center"><br>
              <br>
              <input type="submit" name="button" id="button" value="Proses"></td>
            </tr>
          </table>
        </form><br>
        <h1>Daftar Gallery</h1>
        <table width="100%" border="1" cellspacing="0" cellpadding="3" bordercolor="#000000">
          <tr align="center">
            <td width="4%" bgcolor="#666666"><strong>No</strong></td>
            <td width="20%" bgcolor="#666666"><strong>Aksi</strong></td>
            <td width="76%" bgcolor="#666666"><strong>File Gallery</strong></td>
          </tr>
<?php
	$ctr = 0;
	$hsl = mysqli_query($conn, "select * from tbl_gallery");
	while( $B = mysqli_fetch_array($hsl) ) {
		$ctr += 1;
		$id = $B[0];
		$nama = $B[1];
		$tampil= $B[2];
		if( $tampil == 1 ) { $otok = "Sembunyikan"; } else { $otok = "Tampilkan"; }
		$td_hapus = "<td width=\"50%\" onclick=\"go_hapus('$id');\">Hapus</td>";
		
?>		
          <tr valign="top">
            <td><?php echo $ctr; ?></td>
            <td><table width="100%" border="0" cellpadding="3" cellspacing="0" class="sws_table">
              <tr align="center">
                <td align="center" width="50%" onclick="go_edit('<?php echo $id; ?>', '<?php echo $tampil; ?>');"><?php echo $otok; ?></td>
                <td align="center" width="50%" onclick="go_hapus('<?php echo $id; ?>');">Hapus</td> 
              </tr>
            </table></td>
            <td align="center"><span class="sws_table"></span><img src="gallery/<?php echo $nama; ?>" width="425"></td>
          </tr>
<?php
	}
?>
        </table>
        <p>&nbsp;</p>
      </div>
    </div>
    <?php generate_footer(); ?>
  </div>
  <p>&nbsp;</p>
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript" src="js/image_fade.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
    });
  </script>
  
<form action="pd_man_gallery_edit.php" method="post" name="form_edit">
<input name="id" type="hidden" value="" />
<input name="token" type="hidden" value="" />
</form>
<form action="pd_man_gallery_del.php" method="post" name="form_hapus">
<input name="id" type="hidden" value="" />
</form>  
  
</body>
</html>
