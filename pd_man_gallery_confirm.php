<?php include_once("zz_koneksi_db.php"); ?>
<?php
	session_start();
	$host  = $_SERVER['HTTP_HOST'];
	$uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
	$ip1 = $_SERVER['HTTP_X_FORWARDED_FOR'];
	$ip2 = $_SERVER['REMOTE_ADDR'];
	$waktu = date("Y-m-d H:i:s");
	$extra = "pd_login.php";
	if( $_SESSION["sws_id"] == "" || !isset($_SESSION["sws_id"]) || $_SESSION["sws_id"] == 0 ) {
		echo "<script>window.location.href=\"pd_login.php\";</script>";
		//header("Location: http://$host$uri/$extra");
		exit;
	}
	if( $_SESSION["sws_tingkat"] != 99 ) {
		echo "<script>window.location.href=\"pd_login.php\";</script>";
		//header("Location: http://$host$uri/$extra");
		exit;
	}
	function uniqueFilename($strExt) {
		$arrIp = explode('.', $_SERVER['REMOTE_ADDR']);
		list($usec, $sec) = explode(' ', microtime());
		$usec = (integer) ($usec * 65536);
		$sec = ((integer) $sec) & 0xFFFF;
		$strUid = sprintf("%08x-%04x-%04x", ($arrIp[0] << 24) | ($arrIp[1] << 16) | ($arrIp[2] << 8) | $arrIp[3], $sec, $usec);
		// tack on the extension and return the filename
		return $strUid . $strExt;
	}
	if( empty($_FILES['berkas']) ) {
		echo "<script>window.location.href=\"pd_man_gallery.php\";</script>";
		//header("Location: http://$host$uri/$extra");
		exit;
	}
	$fname = $_FILES['berkas']['name'];
	$dum = explode(".",$fname);
	if( count($dum) > 1 ) {
		$fname_db = uniqueFilename(".".$dum[count($dum)-1]);
	} else {
		$fname_db = uniqueFilename("");
	}
	$uploaddir = "gallery/".$fname_db;
	if(move_uploaded_file($_FILES['berkas']['tmp_name'], $uploaddir)) {
		mysqli_query($conn, "insert into tbl_gallery VALUES (null, '$fname_db', 1)");
	}
?>

<form action="pd_man_gallery.php" method="post" name="form1"></form>
<script>document.form1.submit();</script>