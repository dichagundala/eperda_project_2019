<?php include_once("zz_koneksi_db.php"); ?>
<?php include ("zz_generate_menu.php"); ?>
<!DOCTYPE HTML>
<html>

<head>
  <title>.:: ePERDA - Kementerian Dalam Negeri ::.</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript" src="js/image_fade.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
    });
  </script>
</head>

<body>
	<div id="main">
		<header>
		  <?php generate_logo(); ?>
		  <?php generate_menu(2); ?>
		</header>
		<div id="site_content">
		  <div class="gallery"><?php generate_gallery($conn, 950, 150); ?></div>
			<div class="content">
				<h1>Layanan ePERDA</h1>
				<p><br>
					Layanan ePERDA merupakan layanan yang dikembangkan oleh Direktorat Produk Hukum Daerah - Direktorat Jenderal Otonomi Daerah Kementerian Dalam Negeri dan merupakan kesatuan yang terkait antara satu sama lain dari 3 sub-layanan, yaitu:<br>
					a. e-Register<br>
					b. e-Fasilitasi<br>
					c. e-Konsultasi Publik<br><br>
					Ketiga layanan di atas merupakan layanan tidak berbayar yang disediakan oleh Kementerian Dalam Negeri untuk mendukung dan memperlancar proses pengajuan Rancangan Peraturan Daerah. Walaupun demikian, layanan tersebut merupakan layanan untuk kalangan terbatas yang terkait dengan proses pengajuan itu sendiri. Untuk itu pengguna diharuskan untuk melakukan pendaftaran dan melalui proses persetujuan (<em>approval</em>) dari pengelola sistem ePERDA sebelum pengguna dapat mengakses layanan-layanan seperti yang telah disebut di atas. Pendaftaran dapat dilakukan <a href="daftar.php">disini</a>.<br>
					Seluruh data dan informasi Rancangan Peraturan Daerah dan Nomor Register Rancangan Peraturan Daerah yang terdapat dalam basis data ePERDA hanya digunakan untuk kepentingan kalangan terbatas dan bukan merupakan konsumsi publik.<br>
					Hubungan antara ketiga sub-layanan tersebut dapat dilihat pada gambar di bawah ini:
				  <br>
				</p>
				<table width="100%" border="0" cellspacing="0" cellpadding="3">
					<tr>
					  <td align="center"><img src="images/sublayanan.PNG" width="653" height="426"></td>
					</tr>
				</table>
				
				<p><strong>eRegister</strong><br><br>
					Merupakan layanan untuk mendapatkan nomor Register ketika sebuah Rancangan Peraturan Daerah sudah diajukan oleh sebuah Provinsi atau Kabupaten/Kota, dan setelah dilakukan fasilitasi oleh Kementerian Dalam Negeri.<br>
					Diharapkan dengan diimplementasikannya sub-layanan eRegister ini maka Pemerintah Daerah (Provinsi atau Kabupaten/Kota) sudah mendapatkan informasi pendahuluan dari Rancangan Peraturan Daerah yang tengah diajukan.<br>
					Di lain pihak, di sisi Kementerian Dalam Negeri, pembuatan nomor Register dan penyimpanan dokumen pendukung secara sistem mempunyai beberapa keuntungan yaitu:
				</p>
			
				<table width="100%" border="0" cellspacing="0" cellpadding="3">
				  <tr valign="top">
					<td width="4%">a</td>
					<td width="96%">Terdata dan tersimpannya nomor Register dan Rancangan Peraturan Daerah secara terstruktur dalam sebuah basis data, dimana dengan tersimpannya dokumen tersebut secara terrstruktur maka proses pencarian dan <em>retrieval</em> dari dokumen tersebut, ketika dibutuhkan, dapat dilakukan secara cepat dan mudah.</td>
				  </tr>
				  <tr valign="top">
					<td>b</td>
					<td>Mengeliminasi kemungkinan terjadinya masalah yang timbul pada manusia (<em>human error</em>) pada saat pendefinisian nomor Register dan penyimpanan Rancangan Peraturan Daerah.</td>
				  </tr>
				  <tr valign="top">
					<td>c</td>
					<td>Aksesibilitas terhadap dokumen-dokumen yang lebih luas dalam artian fasilitator dapat mengakses dan mengunduh dokumen-dokumen tersebut tanpa dibatasi kendala ruang dan waktu. Hal ini disebabkan karena dokumen-dokumen tersebut disimpan dalam bentuk digital dan dapat diakses melalui media jaringan komunikasi global</td>
				  </tr>
				</table>
				<p> 
					eRegister merupakan modul antar muka (<em>interface</em>) antara Pemerintah Daerah Tingkat Provinsi atau Kabupaten/Kota dengan Kementerian Dalam Negeri dalam konteks pengajuan Rancangan Peraturan Daerah.<br>
					<br>
					<strong>eFasilitasi</strong><br><br>
					Sesuai dengan ketentuan dalam Pasal 87 dan 88, Peraturan Menteri Dalam Negeri Nomor 80 Tahun 2015 tentang Pembentukan Produk Hukum Daerah mengatakan bahwa "Pembinaan terhadap Produk Hukum Daerah berbentuk peraturan di Provinsi dilakukan oleh Menteri Dalam Negeri melalui Direktur Jenderal Otonomi Daerah" dan "pembinaan dilakukan Fasilitasi terhadap rancangan peraturan daerah sebelum mendapat persetujuan bersama antara pemerintah daerah dengan DPRD".<br>

					Fasilitasi yang dilakukan Menteri Dalam Negeri melalui Direktorat Jenderal Otonomi Daerah sesuai dengan ketentuan dalam Pasal 89 ayat (1) Peraturan Menteri Dalam Negeri Nomor 80 Tahun 2015 tentang Pembentukan Produk Hukum Daerah, antara lain adalah Rancangan Perda, Rancangan Perkada, Rancangan PB KDH atau Rancangan Peraturan DPRD dan dilakukan paling lama 15 (lima belas) hari setelah diterima.<br><br>

					<strong>e-Konsultasi Publik</strong><br><br>
					e-Konsultasi Publik merupakan layanan dari Kementerian Dalam Negeri untuk berinteraksi dengan pengguna akhir dalam konteks prosedur pelaksanaan pengajuan Rancangan Peraturan Daerah dan proses-proses lainnya yang berhubungan dengan ePERDA.<br>
					Modul antar muka (interface) dari e-Konsultasi Publik ini dibangun dengan mengadopsi antar muka fasilitas chat online yang sudah banyak diimplementasikan di pasaran. Pengguna dapat melakukan percakapan secara online, baik dengan pengelola ePERDA maupun dengan pengguna lain. Walaupun sistem ini beroperasi secara nonstop 24 jam, tetapi pada umumnya jam operasional dari operator pengelola ePERDA adalah setiap hari kerja antara pukul 08.00 WIB sampai dengan pukul 16.00 WIB. Jika pengguna menginginkan layanan di luar jam operasional tersebut, pengguna dapat membuat janji dengan operator pengelola ePERDA secara mandiri.<br><br>
					Untuk informasi lebih  lanjut silakan hubungi e-mail: <a href="mailto:phd.otda@kemendagri.go.id">phd.otda@kemendagri.go.id</a><br>
				</p>
			</div>
			<br>
		</div>
		
    <?php generate_footer(); ?>
  <p>&nbsp;</p>
	</div>
 
</body>
 
</html>
