<?php
function generate_menu($a) {
	$sws_tingkat ="";
	$sws_id ="";
	if(isset($_SESSION["sws_tingkat"]))
		$sws_tingkat = 	  $_SESSION["sws_tingkat"];
	if(isset($_SESSION["sws_id"]))
		$sws_id = 	  $_SESSION["sws_id"];

	echo "
	<nav>
	<ul id=\"nav\">";
	if( $a == 1 ) { 
		echo "<li><a href=\"index.php\">Beranda</a></li>";
	} 
	if( $a == 2 ) {
		echo "<li class=\"dropdown\"><a href=\"#\">Pengantar</a>";
	} 
	echo "
	<ul class=\"dropdown-menu\">
	<li><a href=\"tentang.php\">Tentang ePERDA</a></li>
	<li><a href=\"layanan.php\">Pelayanan ePERDA</a></li>
	<li><a href=\"fgal.php\">Galeri Foto</a></li>
	<li><a href=\"disclaimer.php\">Disclaimer</a></li>
	</ul>
	</li>";

	if( $sws_tingkat == 2 || $sws_tingkat== 99 ) {
		echo "<li class=\"dropdown\"><a href=\"#\">Pengunggahan Data</a>";
		echo "  <ul class=\"dropdown-menu\">";
		echo "		<li><a href=\"pd_dreg.php\">Hasil Fasilitasi</a></li>";
		echo "		<li><a href=\"pd_dnreg.php\">Hasil Register</a></li>";
		echo "  </ul>";
		echo "</li>";
	} else {
		if( $a == 3 ) {
			echo "<li class=\"dropdown\"><a href=\"#\">Pelayanan</a>";
		}
		echo "
		<ul class=\"dropdown-menu\">
		<li><a href=\"live_chat.php\">e-Konsultasi Publik</a></li>
		<li class=\"dropdown-submenu\"><a href=\"#\">e-Fasilitasi</a>
		<ul class=\"dropdown-menu\">
		<li><a href=\"reg_ranperda.php\">Pengunggahan Ranperda</a>
		<li><a href=\"tl_ranperda.php\">Pemantauan Ranperda</a></li>
		</ul>
		</li>
		<li class=\"dropdown-submenu\"><a href=\"#\">e-Register</a>			
		<ul class=\"dropdown-menu\">
		<li><a href=\"eregister.php\">Permohonan Register</a>
		<li><a href=\"tl_eregister.php\">Pemantauan Register</a></li>
		</ul>				  
		</li>
		";
		echo "
		</ul>          
		</li>";
	}

	if( $sws_tingkat == 10 || $sws_tingkat == 11 ) {
		echo "<li class=\"dropdown\"><a href=\"#\">Pengunggahan</a>";
		echo "<ul class=\"dropdown-menu\">";
		echo "<li><a href=\"upload_perda.php\">Pengunggahan PERDA</a></li>";
		echo "</ul>";
		echo "</li>";
	}
	if( $a == 4 ) {
		echo "<li class=\"dropdown\"><a href=\"#\">Produk Hukum</a>";
	}
	echo "
	<ul class=\"dropdown-menu\">
	<li><a href=\"ph_uu.php\">Undang-Undang (UU)</a></li>
	<li><a href=\"ph_perpu.php\">PP Pengganti UU</a></li>
	<li><a href=\"ph_pp.php\">Peraturan Pemerintah (PP)</a></li>
	<li><a href=\"ph_perpres.php\">Peraturan Presiden</a></li>
	<li><a href=\"ph_inpres.php\">Instruksi Presiden</a></li>
	<li><a href=\"ph_kepres.php\">Keputusan Presiden</a></li>
	<li><a href=\"ph_permen.php\">Peraturan Menteri</a></li>
	<li><a href=\"ph_inmen.php\">Instruksi Menteri</a></li>
	<li><a href=\"ph_kepmen.php\">Keputusan Menteri</a></li>
	<li><a href=\"ph_perda.php\">Peraturan Daerah</a></li>
	<li class=\"dropdown-submenu\"><a href=\"#\">Pembatalan/Revisi</a>
	<ul class=\"dropdown-menu\">
	<li><a href=\"pd_pmbatal.php\">Peraturan Menteri</a></li>
	<li><a href=\"pd_kmbatal.php\">Keputusan Menteri</a></li>
	<li><a href=\"pd_dbatal.php\">Peraturan Daerah</a></li>
	</ul>
	</li>
	</ul>
	</li>";
	if( $a == 5 ) {
		echo "<li class=\"dropdown\"><a href=\"#\">Halaman Internal</a>";
	}
	echo "
	<ul class=\"dropdown-menu\">";
	if( $sws_id == "" || $sws_id == 0 ) {			
		echo "<li><a href=\"pd_login.php\">Login</a></li>";
	} else {
		if( $sws_tingkat == 10 || $sws_tingkat == 11 ) {

			echo "<li><a href=\"pd_login.php\">Login</a></li>";
			echo "<li><a href=\"rubah_profil.php\">Rubah Data Pengguna</a></li>";
		} else {
			echo "
			<li><a href=\"pd_logout.php\">Logout</a></li>
			<li><a href=\"pd_rubah_pwd.php\">Rubah Profil</a></li>
			<li class=\"dropdown-submenu\"><a href=\"#\">e-Fasilitasi</a>
			<ul class=\"dropdown-menu\">
			<li><a href=\"pd_dn.php\">Daftar Notifikasi</a></li>
			<li><a href=\"pd_dreg.php\">Daftar Fasilitasi</a></li>
			</ul>
			</li>
			<li class=\"dropdown-submenu\"><a href=\"#\">e-Register</a>
			<ul class=\"dropdown-menu\">
			<li><a href=\"pd_dnreg.php\">Daftar Pengajuan Register</a></li>
			</ul>
			</li>
			<li class=\"dropdown-submenu\"><a href=\"#\">Pembatalan/Revisi</a>
			<ul class=\"dropdown-menu\">
			<li class=\"dropdown-submenu\"><a href=\"#\">Daftar Pembatalan/Revisi</a>
			<ul class=\"dropdown-menu\">
			<li><a href=\"pd_pmbatal.php\">Peraturan Menteri</a></li>
			<li><a href=\"pd_kmbatal.php\">Keputusan Menteri</a></li>
			<li><a href=\"pd_dbatal.php\">Perda</a></li>
			</ul>
			</li>
			<li><a href=\"pd_ebatal.php\">Proses Pembatalan Perda</a></li>
			</ul>
			</li>
			<li class=\"dropdown-submenu\"><a href=\"#\">Tinjauan Kinerja</a>
			<ul class=\"dropdown-menu\">
			<li class=\"dropdown-submenu\"><a href=\"#\">Jumlah</a>
			<ul class=\"dropdown-menu\">
			<li><a href=\"pd_jl_fas.php\">Fasilitasi RANPERDA</a></li>
			<li><a href=\"pd_jl_reg.php\">Pengajuan Register</a></li>
			<li><a href=\"pd_jl_bat.php\">Pengajuan Pembatalan PERDA</a></li>
			</ul>						
			</li>
			<li class=\"dropdown-submenu\"><a href=\"#\">Lama Respond</a>
			<ul class=\"dropdown-menu\">
			<li><a href=\"pd_wr_fas.php\">Fasilitasi RANPERDA</a></li>
			<li><a href=\"pd_wr_reg.php\">Pengajuan Register</a></li>
			<li><a href=\"pd_wr_bat.php\">Pengajuan Pembatalan PERDA</a></li>
			</ul>
			</li>
			</ul>
			</li>";
			if($sws_tingkat == 99 ) {
				echo " 
				<li class=\"dropdown-submenu\"><a href=\"#\">Manajemen Sistem</a>
				<ul class=\"dropdown-menu\">
				<li><a href=\"pd_man_user.php\">Pengguna</a></li>
				<li><a href=\"pd_man_daftar.php\">Pendaftaran</a></li>
				<li><a href=\"pd_man_fb.php\">Feedback</a></li>
				<li><a href=\"pd_man_perda.php\">Pengunggahan PERDA</a></li>
				<li><a href=\"pd_man_gallery.php\">Gallery</a></li>
				</ul>
				</li>";
			}
		}
	}	
	echo "
	</ul>
	</li>";
	if( $a == 6 ) {
		echo "<li class=\"dropdown\"><a href=\"#\">Kontak Kami</a>";
	}
	echo "
	<ul class=\"dropdown-menu\">
	<li><a href=\"contact.php\">Kirim Masukan</a></li>
	<li><a href=\"panduan.php\">Panduan</a></li>
	</ul>
	</li>";
	echo "
	</ul>
	</nav>
	";
	if(!isset($_SESSION["sws_islogin"]) ) {
		echo "<div id=\"txt_register\" align=\"right\"><a style='text-decoration: none;'>Sudah terdaftar ?  </a><a href=\"pd_login.php\"><span class=\"font_merah\">Klik disini</span></a> | <a href=\"daftar.php\">Daftar</a></div>";
	} else {
		echo "<div id=\"txt_register\" align=\"right\">Selamat datang <span class=\"font_merah\">".$_SESSION["sws_nama_pengguna"]."</span> | <a href=\"logout.php\">Logout</a></div>";
	}
}

function generate_logo() {
	echo "<div id=\"logo\">
	<div id=\"logo_text\">
	<div id=\"Shape3\">
	<img src=\"images/logo.png\" align=\"right\">
	<div id=\"Shape4\" align=\"right\">Direktorat Produk Hukum Daerah<br>Direktorat Jenderal Otonomi Daerah<br>Kementerian Dalam Negeri</div>
	</div>
	<img src=\"images/logo_eperda.png\">
	</div>
	</div>";
}


function generate_logo_old() {
	echo "  	<div id=\"Shape3\"><img src=\"images/logo.png\"></div>
	<div id=\"Shape4\" align=\"right\">
	Direktorat Produk Hukum Daerah<br>
	Direktorat Jenderal Otonomi Daerah<br>
	</div>";
	if( Isset($_SESSION["sws_islogin"]) == "" || !isset($_SESSION["sws_islogin"]) ) {
		echo "<div id=\"txt_register\" align=\"right\">Sudah terdaftar? <a href=\"pd_login.php\"><span class=\"font_merah\">Klik disini</span></a> | <a href=\"daftar.php\">Daftar</a></div>";
	} else {
		echo "<div id=\"txt_register\" align=\"right\">Selamat datang <span class=\"font_merah\">".$_SESSION["sws_nama_pengguna"]."</span> | <a href=\"logout.php\">Logout</a></div>";
	}
	echo "      <div id=\"logo\">
	<div id=\"logo_text\">
	<h1><a href=\"index.php\">e<span class=\"font_merah\"><strong>PERDA</strong></span></a></h1>
	<h2>Kementerian Dalam Negeri</h2>
	</div>
	</div>";
}

// function generate_footer() {
// 	echo "    <footer>
// 	<p>© Copyright DIREKTORAT PRODUK HUKUM DAERAH All Right Reserved<br>Jl. Medan Merdeka Utara No. 7, Jakarta Pusat - INDONESIA<br>
// 	Telp. (021) 3453627 Fax. (021) 3453627<br>e-mail: <a href=\"mailto:phd.otda@kemendagri.go.id\">phd.otda@kemendagri.go.id</a><br>
// 	</footer>";
// }

function generate_berita($B) {
	echo "        <div class=\"sidebar\">
	<h3>Update Berita</h3>
	<h4>".$B["judul"]."</h4>
	<h5>".$B["oleh"].": ".sws_get_tgl($B["tgl"])."</h5>
	<p>".sws_potong($B["isi"],150)."<br /><a href=\"berita_satu.php?a=".$B["id"]."\">Baca selengkapnya</a></p>
	</div>";
}

function generate_tautan($conn) {
	echo "<ul class=\"list-icon list-icon-caret\">    
	<li><a href=\"http://www.kemendagri.go.id\" target=\"_blank\">Kementerian Dalam Negeri</a></li>";
	$hsl = mysqli_query($conn, "select * from tbl_tautan where tampil=1");
	while( $B = mysqli_fetch_array($hsl) ) {
		echo "<li><a href=\"".$B["tautan"]."\" target=\"_blank\">".$B["nama"]."</a></li>";
	}
	echo "</ul>";
}

function generate_gallery($conn, $w, $h) {
	echo "<ul class=\"images\">";
	$pertama = true;
	$hsl = mysqli_query($conn, "select * from tbl_gallery where tampil=1");
	while( $B = mysqli_fetch_array($hsl) ) {
		$nf = $B["nama"];
		if( $pertama ) {
			$pertama = false;
			echo "<li class=\"show\"><img width=\"$w\" height=\"$h\" src=\"gallery/$nf\" alt=\"\" /></li>";
		} else {
			echo "<li><img width=\"$w\" height=\"$h\" src=\"gallery/$nf\" alt=\"\" /></li>";
		}
	}
	echo "</ul>";
}

function sws_get_tgl($a1) {
	$d = explode(" ",$a1);
	$T = explode("-",$d[0]);
	$a = $T[2];
	if( $T[1] == 1 ) { $a .= " Januari "; }
	elseif( $T[1] == 2 ) { $a .= " Februari "; }
	elseif( $T[1] == 3 ) { $a .= " Maret "; }
	elseif( $T[1] == 4 ) { $a .= " April "; }
	elseif( $T[1] == 5 ) { $a .= " Mei "; }
	elseif( $T[1] == 6 ) { $a .= " Juni "; }
	elseif( $T[1] == 7 ) { $a .= " Juli "; }
	elseif( $T[1] == 8 ) { $a .= " Agustus "; }
	elseif( $T[1] == 9 ) { $a .= " September "; }
	elseif( $T[1] == 10 ) { $a .= " Oktober "; }
	elseif( $T[1] == 11 ) { $a .= " November "; }
	elseif( $T[1] == 12 ) { $a .= " Desember "; }
	$a .= $T[0];
	return $a;
}

function sws_potong($a1, $b) {
	$a = substr($a1, 0, $b)." ...";
	return $a;
}
?>	  