<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyC61ki0hlbvrqI96_F2EdmcZoScN23xQF8&sensor=false"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript" src="geoXML3.js"></script>
<script>

var geoXml = [];
var countgeoXML = 0;
var map = null;

$(document).ready(function(){
	initialize();
});

function initialize() {
	var geoXmlDoc = null;
	jQuery(document).ready(function () {
		var mapOptions = {
    		center: new google.maps.LatLng(-6.184517,106.82249),
			zoom: 6,
			minZoom: 2,
			scrollwheel: true,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};
    	map = new google.maps.Map(document.getElementById("googleMap"), mapOptions);	
	});
	
	url = "zz_peta_analisis.php";
	$.getJSON(url, function(data){ 
		for( i = 0; i < countgeoXML; i++ ) {
			geoXml[i].hideDocument(geoXml[i].docs[0]);
			geoXml[i] = null;
		}
		countgeoXML = 0;
		$.each(data, function(index, d){
			geoXml[countgeoXML] = new geoXML3.parser({
				map: map,
				singleInfoWindow: true
			});
			geoXml[countgeoXML].parseKmlString(d.kml);
			countgeoXML += 1;
		});
	}).error(function(jqXHR, textStatus, errorThrown){ /* assign handler */});
}

</script>

</head>

<body>
<div id="googleMap" style="height:300px;width:100%">
</body>
</html>