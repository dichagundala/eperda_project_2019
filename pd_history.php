<?php include_once("zz_koneksi_db.php"); ?>
<?php include ("zz_generate_menu.php"); ?>
<?php
	session_start();
	$host  = $_SERVER['HTTP_HOST'];
	$uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
	$ip1 = $_SERVER['HTTP_X_FORWARDED_FOR'];
	$ip2 = $_SERVER['REMOTE_ADDR'];
	$waktu = date("Y-m-d H:i:s");
	$extra = "pd_login.php";
	if( $_SESSION["sws_id"] == "" || !isset($_SESSION["sws_id"]) || $_SESSION["sws_id"] == 0 ) {
		echo "<script>window.location.href=\"pd_login.php\";</script>";
		//header("Location: http://$host$uri/$extra");
		exit;
	}
	if( $_SESSION["sws_tingkat"] == 99 || $_SESSION["sws_tingkat"] == 1 || $_SESSION["sws_tingkat"] == 2 ) {
	} else {
		echo "<script>window.location.href=\"pd_login.php\";</script>";
		//header("Location: http://$host$uri/$extra");
		exit;
	}
	$anid = $_POST["id"];
	$token = $_POST["token"];
	
	function get_waktu($a) {
		$dum = explode(" ",$a);
		$dum1 = explode("-", $dum[0]);
		$ostr = $dum1[2];
		if( $dum1[1] == "01" ) { $ostr .= " Januari ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "02" ) { $ostr .= " Februari ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "03" ) { $ostr .= " Maret ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "04" ) { $ostr .= " April ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "05" ) { $ostr .= " Mei ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "06" ) { $ostr .= " Juni ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "07" ) { $ostr .= " Juli ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "08" ) { $ostr .= " Agustus ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "09" ) { $ostr .= " September ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "10" ) { $ostr .= " Oktober ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "11" ) { $ostr .= " November ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "12" ) { $ostr .= " Desember ".$dum1[0]." ".$dum[1]; }
		
		$tnow = time();
		$tunduh = strtotime($a);
		$time = $tnow - $tunduh;
		$time = ($time < 1) ? 1 : $time;
		$tokens = array(
			31536000 => 'tahun',
			2592000  => 'bulan',
			604800   => 'minggu',
			86400    => 'hari',
			3600     => 'jam',
			60		 => 'menit',
			1		 => 'detik');
			
		foreach($tokens as $unit => $text ) {
			if( $time < $unit ) continue;
			$nou = floor($time / $unit);
			$et = $nou." ".$text;
			break;
		}
		return $ostr." (".$et." yang lalu)";
	}
?>


<!DOCTYPE HTML>
<html>

<head>
  <title>.:: ePERDA - Kementerian Dalam Negeri ::.</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>

<script language="javascript">
function unduh(b, a, c) {
	document.form_unduh.id.value = a;
	document.form_unduh.jenis.value = b;
	document.form_unduh.tabel.value = c;
	document.form_unduh.submit();
}

function go_edit(a) {
	document.form_edit.id.value = a;
	document.form_edit.submit();
}

function kirim_email(a) {
	document.form_email.id.value = a;
	document.form_email.submit();
}

function go_hapus(a) {
	var ans; 
	ans=window.confirm('Yakin akan menon-aktifkan pengguna ini?');
	if( ans == true ) {
		document.form_hapus.id.value = a;
		document.form_hapus.submit();
	}
}

function review(a) {
	document.form_review.id.value = a;
	document.form_review.submit();
}
</script>  

<style>
.sws_font_kecil {
	font-size: x-small;
}
</style>
</head>

<body>
  <div id="main">
    <header>
      <?php generate_logo(); ?>
      <?php generate_menu(5); ?>
    </header>
    <div id="site_content">
      <div class="gallery"><?php generate_gallery($conn, 950, 150); ?></div>
      <div class="content">
        <h1>Riwayat Pengajuan Rancangan PERDA</h1>
        <table width="100%" border="0" cellspacing="0" cellpadding="3">
<?php
	$hsl = mysqli_query($conn, "select * from tbl_ranperda where id='$anid'");
	while( $B = mysqli_fetch_array($hsl) ) {
		$judul = $B["judul"];
		$ringkasan = str_replace (array("\r\n", "\n", "\r"), '<br>', $B["ringkasan"]);
		$wkirim = get_waktu($B["wkirim"]);
		$okirim = $B["okirim_nama"];
		$ada1 = 1;
		$hsl1 = mysqli_query($conn, "select id, fa from tbl_review where token='$token' and adaberkas=1 ORDER BY id DESC LIMIT 1");
		if( mysqli_num_rows($hsl1) == 0 ) {
			//tidak pernah revisi
			$bj1 = 1;
			$bid1 = $anid;
			$nf1 = $B["nfile_asli"];
		} else {
			$bj1 = 2;
			$B1 = mysqli_fetch_array($hsl1);
			$bid1 = $B1[0];
			$nf1 = $B1[1];
		}
		$hsl1 = mysqli_query($conn, "select id, fa1 from tbl_review where token='$token' and ada1=1 ORDER BY id DESC LIMIT 1");
		if( mysqli_num_rows($hsl1) == 0 ) {
			if( $B["ada_kepdprd"] == 1 ) {
				$ada2 = 1;
				$bj2 = 1;
				$bid2 = $anid;
				$nf2 = $B["kepdprd_asli"];
			} else {
				$ada2 = 0;
			}
		} else {
			$ada2 = 1;
			$bj2 = 2;
			$B1 = mysqli_fetch_array($hsl1);
			$bid2 = $B1[0];
			$nf2 = $B1[1];
		}
		$hsl1 = mysqli_query($conn, "select id, fa2 from tbl_review where token='$token' and ada2=1 ORDER BY id DESC LIMIT 1");
		if( mysqli_num_rows($hsl1) == 0 ) {
			if( $B["ada_sp"] == 1 ) {
				$ada3 = 1;
				$bj3 = 1;
				$bid3 = $anid;
				$nf3 = $B["sp_asli"];
			} else {
				$ada3 = 0;
			}
		} else {
			$ada3 = 1;
			$bj3 = 2;
			$B1 = mysqli_fetch_array($hsl1);
			$bid3 = $B1[0];
			$nf3 = $B1[1];
		}
		$fas = "<td onClick=\"review('$anid');\" onMouseOver=\"this.style.cursor='pointer';\">Lakukan fasilitasi</td>";
		if( $B["sproses"] == 1 ) { $sproses = "<font color=\"#FF0000\"><strong>Menunggu fasilitasi</strong></font>"; }
		if( $B["sproses"] == 2 ) { $sproses = "<font color=\"#000033\">Tindak lanjut</font>"; }
		if( $B["sproses"] == 3 ) { 
			$acuan = $B["acuan"];
			$sproses = "<font color=\"#00CC00\">Sudah disetujui</font>"; 
			$fas = "<td onClick=\"unduh(1, '$acuan', '3');\" onMouseOver=\"this.style.cursor='pointer';\" class=\"sws_font_kecil\"><u>Unduh Kepmen</u></td>";
			$bid1 = $acuan;
			$bid2 = $acuan;
			$bid3 = $acuan;
			$bj1 = 3;
			$bj2 = 3;
			$bj3 = 3;
		}
		
		$hsl1 = mysqli_query($conn, "select nama from tbl_prov where kode='".$B["prov"]."'");
		$B1 = mysqli_fetch_array($hsl1);
		$wilayah = $B1[0];
		if( $B["kabkota"] != 0 ) {
			$hsl1 = mysqli_query($conn, "select nama from tbl_kab where kode_kab='".$B["kabkota"]."'");
			$B1 = mysqli_fetch_array($hsl1);
			$wilayah .= " / ".$B1[0];
		}
		
		$brp = "<td width=\"25%\" onClick=\"unduh(1, '$bid1', '$bj1');\" onMouseOver=\"this.style.cursor='pointer';\" class=\"sws_font_kecil\"><u>Unduh Ranperda</u></td>";
		if( $ada2 == 1 ) {
			$bsk = "<td width=\"25%\" onClick=\"unduh(2, '$bid2', '$bj2');\" onMouseOver=\"this.style.cursor='pointer';\" class=\"sws_font_kecil\"><u>Unduh Berkas Keputusan DPRD</u></td>";
		} else {
			$bsk = "<td width=\"27%\" class=\"sws_font_kecil\">Berkas Keputusan DPRD belum ada</td>";
		}
		if( $ada3 == 1 ) {
			$bsp = "<td width=\"27%\" onClick=\"unduh(3, '$bid3', '$bj3');\" onMouseOver=\"this.style.cursor='pointer';\" class=\"sws_font_kecil\"><u>Unduh Surat Permohonan</u></td>";
		} else {
			$bsp = "<td width=\"26%\" class=\"sws_font_kecil\">Surat Permohonan belum ada</td>";
		}
?>		
          <tr>
            <td><h2><?php echo $judul; ?></h2></td>
          </tr>
          <tr>
            <td><table class="sws_table" width="100%" border="0" cellspacing="0" cellpadding="3" style="border-top: 4px solid #000000;">
              <tr>
                <td>Provinsi / KabKota</td>
                <td colspan="3"><?php echo $wilayah; ?></td>
              </tr>
              <tr valign="top">
                <td width="22%">Ringkasan</td>
                <td colspan="3"><?php echo $ringkasan; ?></td>
              </tr>
              <tr>
                <td>Diunggah Pada</td>
                <td colspan="3"><?php echo $wkirim; ?></td>
              </tr>
              <tr>
                <td>Status Terkini</td>
                <td colspan="3"><?php echo $sproses; ?></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <?php echo $fas; ?><?php echo $brp; ?><?php echo $bsk; ?><?php echo $bsp; ?>
              </tr>
            </table></td>
          </tr>
<?php
	}
?>
        </table>
<?php
	$hsl = mysqli_query($conn, "select * from tbl_review where token='$token' order by id DESC");
	while( $B = mysqli_fetch_array($hsl) ) {
		$trev = get_waktu($B["waktu"]);
		if( $B["sproses"] == 1 ) { $sproses = "<font color=\"#FF0000\"><strong>Menunggu fasilitasi</strong></font>"; }
		if( $B["sproses"] == 2 ) { $sproses = "<font color=\"#000033\">Tindak lanjut</font>"; }
		if( $B["sproses"] == 3 ) { $sproses = "<font color=\"#00CC00\">Sudah disetujui</font>"; }
		$isi = str_replace (array("\r\n", "\n", "\r"), '<br>', $B["isi"]);
		$revnama = $B["reviewer_nama"];
		$id = $B["id"];
		$bj2 = 2;
		if( $B["adaberkas"] == 1 ) {
			$brp = "<td width=\"78%\" onClick=\"unduh(1, '$id', '$bj2');\" onMouseOver=\"this.style.cursor='pointer';\"><u>Ada</u></td>";
		} else {
			$brp = "<td width=\"78%\">Tidak diunggah</td>";
		}
		if( $B["ada1"] == 1 ) {
			$bsk = "<td width=\"78%\" onClick=\"unduh(2, '$id', '$bj2');\" onMouseOver=\"this.style.cursor='pointer';\"><u>Ada</u></td>";
		} else {
			$bsk = "<td width=\"78%\">Tidak diunggah</td>";
		}
		if( $B["ada2"] == 1 ) {
			$bsp = "<td width=\"78%\" onClick=\"unduh(3, '$id', '$bj2');\" onMouseOver=\"this.style.cursor='pointer';\"><u>Ada</u></td>";
		} else {
			$bsp = "<td width=\"78%\">Tidak diunggah</td>";
		}
?>		        
        <table width="100%" border="0" cellspacing="0" cellpadding="3">
          <tr>
            <td>
<?php
		if( $B["sproses"] == 2 || $B["sproses"] == 3 ) {
			// aksi dari Pemerintah Pusat
?>			            
            <table class="sws_table" width="100%" border="0" cellspacing="0" cellpadding="3" style="border-top: 2px solid #FF0000; border-bottom: 1px solid #000"">
              <tr>
                <td width="22%">Difasilitasi Pada</td>
                <td width="78%"><?php echo $trev; ?></td>
              </tr>
              <tr>
                <td>Status</td>
                <td><?php echo $sproses; ?></td>
              </tr>
              <tr valign="top">
                <td valign="top">Isi Masukan</td>
                <td valign="top"><?php echo $isi; ?></td>
              </tr>
              <tr>
                <td>Fasilitator</td>
                <td><?php echo $revnama; ?></td>
              </tr>
              <tr>
                <td onClick="kirim_email('<?php echo $id; ?>');" onMouseOver="this.style.cursor='pointer';">Kirim Ulang email</td>
                <td>&nbsp;</td>
              </tr>
            </table>

<?php
		} else {
			//aksi dari Daerah
?>
            <table class="sws_table" width="100%" border="0" cellspacing="0" cellpadding="3" style="border-top: 2px solid #FF0000; border-bottom: 1px solid #000"">
              <tr>
                <td width="22%">Dilengkapi Pada</td>
                <td width="78%"><?php echo $trev; ?></td>
              </tr>
              <tr>
                <td width="22%">RANPERDA</td>
                <?php echo $brp; ?>
              </tr>
              <tr>
                <td width="22%">Surat Keputusan</td>
                <?php echo $bsk; ?>
              </tr>
              <tr>
                <td width="22%">Surat Permohonan</td>
                <?php echo $bsp; ?>
              </tr>
              <tr valign="top">
                <td valign="top">Keterangan</td>
                <td valign="top"><?php echo $isi; ?></td>
              </tr>
              <tr>
                <td>Pengunggah</td>
                <td><?php echo $revnama; ?></td>
              </tr>
            </table>

<?php
		}
?>
            </td>
          </tr>
        </table>
<?php
	}
?>
        <br>
        <p>&nbsp;</p>
      </div>
    </div>
    <?php generate_footer(); ?>
  </div>
  <p>&nbsp;</p>
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript" src="js/image_fade.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
    });
  </script>
  
<form action="pd_man_user_edit.php" method="post" name="form_edit">
<input name="id" type="hidden" value="" />
</form>
<form action="pd_man_user_del.php" method="post" name="form_hapus">
<input name="id" type="hidden" value="" />
</form>
<form action="pd_kirim_email.php" method="post" name="form_email">
<input name="id" type="hidden" value="" />
<input name="id_induk" type="hidden" value="<?php echo $anid; ?>" />
<input name="token" type="hidden" value="<?php echo $token; ?>" />
</form>  
<form action="pd_unduh_01.php" method="post" name="form_unduh">
<input name="id" type="hidden" value="" />
<input name="jenis" type="hidden" value="" />
<input name="tabel" type="hidden" value="" />
</form>
<form action="pd_review.php" method="post" name="form_review">
<input name="id" type="hidden" value="" />
</form>
  
</body>
</html>
