<?php include_once("zz_koneksi_db.php"); ?>
<?php include ("zz_generate_menu.php"); ?>
<?php
	require('calendar/tc_calendar.php');
	if( $_SESSION["sws_daerah"] == "" || !isset($_SESSION["sws_daerah"]) ) {
		echo "<script>window.location.href = \"reg_login.php\";</script>";
	}
?>
<!DOCTYPE HTML>
<html>

<head>
  <title>.:: ePERDA - Kementerian Dalam Negeri ::.</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>
  
  <script>
  	function checkit() {
		var errorMsg = "";
	
		//Check for a username
		if (document.form1.judul.value==""){ errorMsg += "\n\t - Isi nama judul PERDA dahulu";}
		if (document.form1.nomor.value==""){ errorMsg += "\n\t - Isi nomor PERDA yang akan diunggah";}
		if (document.form1.tahun.value==""){ errorMsg += "\n\t - Isi tahun PERDA disahkan";}
		if( isNaN(document.form1.tahun.value) ) {
			errorMsg += "\n\t - Tahun PERDA harus numerik";
		}
		if (document.form1.answer.value!=document.form1.user_answer.value){ errorMsg += "\n\t - Tidak dapat melakukan verifikasi";}
		if (errorMsg != ""){
			msg  = "___________________________________________________________________\n\n";
			msg += "Update data belum bisa dilakukan dikarenakan kesalahan berikut ini.\n";
			msg += "___________________________________________________________________\n\n";
			errorMsg += alert(msg + errorMsg + "\n\n");
			return false;
		}
		
		return true;
	}
  </script>
</head>

<body>
  <div id="main">
    <header>
	  <?php generate_logo(); ?>
      <?php generate_menu(7); ?>
    </header>
    <div id="site_content">
      <div class="gallery"><?php generate_gallery($conn, 950, 150); ?></div>
      <div class="content">
        <h1>Pengunggahan Perda</h1>
        <p>Pengunggahan PERDA berhasil dilakukan.<br>
          <br>
        Patut diketahui bahwa keberhasilan pengunggahan Perda <strong>tidak </strong>serta merta akan disimpan dalam database PERDA Kementerian Dalam Negeri, tetapi akan menunggu persetujuan dari pengelola Kementerian Dalam Negeri. Langkah ini dilakukan untuk menghindari kesalahan pada saat pengunggahan data.. <br>
        <br>
        Klik <a href="upload_perda.php">disini</a> untuk memulai pengunggahan PERDA
        yang lain.<br>
        <br>
        Terima kasih atas partisipasinya.
        </p>
        <?phpphp
          $number_1 = rand(1, 9);
          $number_2 = rand(1, 9);
          $answer = $number_1+$number_2;
        ?><br><br><br><br>
      </div>
    </div>
    <?php generate_footer(); ?>
  </div>
  <p>&nbsp;</p>
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript" src="js/image_fade.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
    });
  </script>
</body>
</html>
