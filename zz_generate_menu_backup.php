<?php
	function generate_menu($a) {
		echo "
		<nav>
			<ul class=\"sf-menu\" id=\"nav\">";
		if( $a == 1 ) { 
			echo "<li class=\"selected\"><a href=\"index.php\">Beranda</a></li>";
		} else {
			echo "<li><a href=\"index.php\">Beranda</a></li>";
		}
		if( $a == 2 ) {
			echo "<li class=\"selected\"><a href=\"#\">Pengantar</a>";
		} else {
			echo "<li><a href=\"#\">Pengantar</a>";
		}
		echo "
				<ul>
				  <li><a href=\"tentang.php\">Tentang ePERDA</a></li>
				  <li><a href=\"layanan.php\">Pelayanan ePERDA</a></li>
				  <li><a href=\"disclaimer.php\">Disclaimer</a></li>
				</ul>
			  </li>";
		if( $a == 3 ) {
			echo "<li class=\"selected\"><a href=\"#\">Pelayanan</a>";
		} else {
			echo "<li><a href=\"#\">Pelayanan</a>";
		}
		echo "
				<ul>
				  <li><a href=\"#\">e-Fasilitasi</a>
				  	<ul>
						<li><a href=\"reg_ranperda.php\">Pengunggahan Ranperda</a>
				  		<li><a href=\"tl_ranperda.php\">Pemantauan Ranperda</a></li>
					</ul>
				  </li>
				  <li><a href=\"lengkap_ranperda.php\">e-Register</a></li>
				  <li><a href=\"live_chat.php\">e-Konsultasi Publik</a></li>
				  ";
		if( $_SESSION["sws_tingkat"] == 10 || $_SESSION["sws_tingkat"] == 11 ) {
			echo "<li><a href=\"upload_perda.php\">Pengunggahan PERDA</a></li>";
			echo "<li><a href=\"rubah_profil.php\">Rubah Data Pengguna</a></li>";
		}
		echo "
				</ul>          
			  </li>";
		if( $a == 4 ) {
			echo "<li class=\"selected\"><a href=\"#\">Produk Hukum</a>";
		} else {
			echo "<li><a href=\"#\">Produk Hukum</a>";
		}
		echo "
				<ul>
				  <li><a href=\"ph_uu.php\">Undang-Undang (UU)</a></li>
				  <li><a href=\"ph_perpu.php\">PP Pengganti UU</a></li>
				  <li><a href=\"ph_pp.php\">Peraturan Pemerintah (PP)</a></li>
				  <li><a href=\"ph_perpres.php\">Peraturan Presiden</a></li>
				  <li><a href=\"ph_inpres.php\">Instruksi Presiden</a></li>
				  <li><a href=\"ph_kepres.php\">Keputusan Presiden</a></li>
				  <li><a href=\"ph_permen.php\">Peraturan Menteri</a></li>
				  <li><a href=\"ph_inmen.php\">Instruksi Menteri</a></li>
				  <li><a href=\"ph_kepmen.php\">Keputusan Menteri</a></li>
				  <li><a href=\"ph_perda.php\">Peraturan Daerah</a></li>
				</ul>
			  </li>";
		if( $a == 5 ) {
			echo "<li class=\"selected\"><a href=\"#\">Halaman Internal</a>";
		} else {
			echo "<li><a href=\"#\">Halaman Internal</a>";
		}
		echo "
				<ul>";
		if( $_SESSION["sws_id"] == "" || !isset($_SESSION["sws_id"]) || $_SESSION["sws_id"] == 0 ) {			
			echo "    <li><a href=\"pd_login.php\">Login</a></li>";
		} else {
			if( $_SESSION["sws_tingkat"] == 10 || $_SESSION["sws_tingkat"] == 11 ) {
				echo "    <li><a href=\"pd_login.php\">Login</a></li>";
			} else {
				echo "
				  <li><a href=\"pd_logout.php\">Logout</a></li>
				  <li><a href=\"pd_rubah_pwd.php\">Rubah Profil</a></li>
				  <li><a href=\"#\">e-Register</a>
				  	<ul>
						<li><a href=\"pd_dn.php\">Daftar Notifikasi</a></li>
						<li><a href=\"pd_dreg.php\">Daftar Pengajuan Ranperda</a></li>
					</ul>
				  </li>
				  <li><a href=\"#\">Pembatalan Perda</a>
				  	<ul>
				  		<li><a href=\"pd_dbatal.php\">Daftar Pembatalan Perda</a></li>
						<li><a href=\"pd_ebatal.php\">Proses Pembatalan Perda</a></li>
					</ul>
				  </li>
				  <li><a href=\"#\">Tinjauan Kinerja</a>
				  	<ul>
						<li><a href=\"pd_jranperda.php\">Jumlah RANPERDA</a></li>
						<li><a href=\"pd_wproses.php\">Waktu Proses</a></li>
					</ul>
				  </li>";
				if($_SESSION["sws_tingkat"] == 99 ) {
					echo " 
					  <li><a href=\"#\">Manajemen Sistem</a>
						<ul>
						  <li><a href=\"pd_man_user.php\">Pengguna</a></li>
						  <li><a href=\"pd_man_daftar.php\">Pendaftaran</a></li>
						  <li><a href=\"pd_man_fb.php\">Feedback</a></li>
						  <li><a href=\"pd_man_perda.php\">Pengunggahan PERDA</a></li>
						  <li><a href=\"pd_man_gallery.php\">Gallery</a></li>
						</ul>
					  </li>";
				}
			}
		}	
		echo "
				</ul>
			  </li>";
		if( $a == 6 ) {
			echo "<li class=\"selected\"><a href=\"#\">Kontak Kami</a>";
		} else {
			echo "<li><a href=\"#\">Kontak Kami</a>";
		}
		echo "
				<ul>
				  <li><a href=\"contact.php\">Kirim Masukan</a></li>
				  <li><a href=\"panduan.php\">Panduan</a></li>
				</ul>
			  </li>";
		echo "
			</ul>
		  </nav>
		";
		if( $_SESSION["sws_islogin"] == "" || !isset($_SESSION["sws_islogin"]) ) {
			echo "<div id=\"txt_register\" align=\"right\">Sudah terdaftar? <a href=\"pd_login.php\"><span class=\"font_merah\">Klik disini</span></a> | <a href=\"daftar.php\">Daftar</a></div>";
		} else {
			echo "<div id=\"txt_register\" align=\"right\">Selamat datang <span class=\"font_merah\">".$_SESSION["sws_nama_pengguna"]."</span> | <a href=\"logout.php\">Logout</a></div>";
		}
	}
	
	function generate_logo() {
		echo "<div id=\"logo\">
		<div id=\"logo_text\">
			<div id=\"Shape3\">
				<img src=\"images/logo.png\" align=\"right\">
				<div id=\"Shape4\" align=\"right\">Direktorat Produk Hukum Daerah<br>Direktorat Jenderal Otonomi Daerah<br>Kementerian Dalam Negeri</div>
			</div>
			<img src=\"images/logo_eperda.png\">
		</div>
	</div>";
	}
		
	
	function generate_logo_old() {
		echo "  	<div id=\"Shape3\"><img src=\"images/logo.png\"></div>
    <div id=\"Shape4\" align=\"right\">
    Direktorat Produk Hukum Daerah<br>
    Direktorat Jenderal Otonomi Daerah<br>
    </div>";
		if( $_SESSION["sws_islogin"] == "" || !isset($_SESSION["sws_islogin"]) ) {
			echo "<div id=\"txt_register\" align=\"right\">Sudah terdaftar? <a href=\"pd_login.php\"><span class=\"font_merah\">Klik disini</span></a> | <a href=\"daftar.php\">Daftar</a></div>";
		} else {
			echo "<div id=\"txt_register\" align=\"right\">Selamat datang <span class=\"font_merah\">".$_SESSION["sws_nama_pengguna"]."</span> | <a href=\"logout.php\">Logout</a></div>";
		}
		echo "      <div id=\"logo\">
        <div id=\"logo_text\">
          <h1><a href=\"index.php\">e<span class=\"font_merah\"><strong>PERDA</strong></span></a></h1>
          <h2>Kementerian Dalam Negeri</h2>
        </div>
      </div>";
	}
	
	function generate_footer() {
		echo "    <footer>
      <p>© Copyright PUSDATINKOMTEL 2013. All Right Reserved<br>
Jl. Medan Merdeka Utara No. 7, Jakarta Pusat - INDONESIA<br>Telp. (021) 3450038, Fax (021) 3851193, 34830261,3846430<br>
      e-mail: <a href=\"mailto:pusdatinkomtel@kemendagri.go.id\">pusdatinkomtel@kemendagri.go.id</a>
	  <br>
    </footer>";
	}
	
	function generate_berita($B) {
		echo "        <div class=\"sidebar\">
          <h3>Pemberitahuan</h3>
          <h4>".$B["judul"]."</h4>
          <h5>".$B["oleh"].": ".sws_get_tgl($B["tgl"])."</h5>
          <p>".sws_potong($B["isi"],150)."<br /><a href=\"berita_satu.php?a=".$B["id"]."\">Baca selengkapnya</a></p>
        </div>";
	}
	
	function generate_tautan($conn) {
		echo "<div class=\"sidebar\">
          <h3>Tautan</h3>
          <ul>
            <li><a href=\"http://www.kemendagri.go.id\" target=\"_blank\">Kementerian Dalam Negeri</a></li>";
		$hsl = mysqli_query($conn, "select * from tbl_tautan where tampil=1");
		while( $B = mysqli_fetch_array($hsl) ) {
			echo "<li><a href=\"".$B["tautan"]."\" target=\"_blank\">".$B["nama"]."</a></li>";
		}
		echo "</ul>
        </div>";
	}
	
	function generate_gallery($conn, $w, $h) {
		echo "<ul class=\"images\">";
		$pertama = true;
		$hsl = mysqli_query($conn, "select * from tbl_gallery where tampil=1",$conn);
		while( $B = mysqli_fetch_array($hsl) ) {
			$nf = $B["nama"];
			if( $pertama ) {
				$pertama = false;
				echo "<li class=\"show\"><img width=\"$w\" height=\"$h\" src=\"gallery/$nf\" alt=\"\" /></li>";
			} else {
          		echo "<li><img width=\"$w\" height=\"$h\" src=\"gallery/$nf\" alt=\"\" /></li>";
			}
		}
		echo "</ul>";
	}
	
	function sws_get_tgl($a1) {
		$d = explode(" ",$a1);
		$T = explode("-",$d[0]);
		$a = $T[2];
		if( $T[1] == 1 ) { $a .= " Januari "; }
		elseif( $T[1] == 2 ) { $a .= " Februari "; }
		elseif( $T[1] == 3 ) { $a .= " Maret "; }
		elseif( $T[1] == 4 ) { $a .= " April "; }
		elseif( $T[1] == 5 ) { $a .= " Mei "; }
		elseif( $T[1] == 6 ) { $a .= " Juni "; }
		elseif( $T[1] == 7 ) { $a .= " Juli "; }
		elseif( $T[1] == 8 ) { $a .= " Agustus "; }
		elseif( $T[1] == 9 ) { $a .= " September "; }
		elseif( $T[1] == 10 ) { $a .= " Oktober "; }
		elseif( $T[1] == 11 ) { $a .= " November "; }
		elseif( $T[1] == 12 ) { $a .= " Desember "; }
		$a .= $T[0];
		return $a;
	}
	
	function sws_potong($a1, $b) {
		$a = substr($a1, 0, $b)." ...";
		return $a;
	}
?>	  