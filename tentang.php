<?php include_once("zz_koneksi_db.php"); ?>
<?php include ("zz_generate_menu.php"); ?>
<!DOCTYPE HTML>
<html>

<head>
  <title>.:: ePERDA - Kementerian Dalam Negeri ::.</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>
<style type="text/css">
.sws_caption {
	font-size: small;
}
</style>
</head>

<body>
  <div id="main">
    <header>
      <?php generate_logo(); ?>
      <?php generate_menu(2); ?>
    </header>
    <div id="site_content">
      <div class="gallery"><?php generate_gallery($conn, 950, 150); ?></div>
      <div class="content">
        <h1>Sekilas Tentang ePERDA</h1><p><br>
          <strong>LATAR BELAKANG</strong><br>
        <br>
        Untuk  memaknai Negara hadir sebagai salah satu visi pemerintahan kabinet kerja  Presiden Jokowi membutuhkan energi  yang kuat dalam pelaksanaan mengawal Perda dan Perkada sebagai <em>output</em> dalam penyelenggaraan  pemerintahan daerah dalam desentrasisasi dan otonomi daerah pada Negara Kesatuan  Republik  Indonesia.<br>
        <br>
Pesatnya  kemajuan dunia informatika membuat Negara tanpa batas efisiensi, efektivitas  dan akuntabilitas menjadi satu kesatuan kata yang harus menjadi bagian dalam  memberikan pelayanan kepada masyarakat. <br>
Berdasarkan  Pasal 89 Permendagri 80 Tahun 2015  Tentang Pedoman Penyusunan Produk Hukum Daerah<strong></strong>diberikan  wewenang untuk melakukan pembinaan terhadap Perda dan Perkada, Peraturan Bersama Kepala  Daerah dan  Tatib DPRD, fenomena pembatalan Perda dan  Perkada sejak tahun 2000 s.d  2014 menjadi hal yang lumrah tanpa solusi yang optimal.<br>
<br>
Insinkronisasi  tidak hanya terjadi di daerah namun terjadi di regulasi tingkat pusat yang pada  satu titik terakumulasi menjadi penghambat bagi dunia investasi.<br>
<br>
Permasalahan  inefisiensi dalam penyusunan perda dan perkada dimulai dari tahap perencanaan,  penyusunan, pembahasan hingga penetapan serta pengundangan demikian dan selalu  terjadi pengulangan, sehingga permasalahan inefisiensi pada saat penyusunan Perda  dan Perkada menjadi prioritas Direktorat  Jenderal Otonomi Daerah untuk membangun sistem e-perda dengan tujuan:</p>
        <ul>
          <li>Negara hadir artinya melalui sistem  e-perda Kemendagri selaku Pembina umum melakukan komunikasi intensif di dunia  maya pada saat proses penyusunan produk hukum daerah dimulai hingga pada saat  diimplementasikan sehingga meminimalisir pembatalan.</li>
          <li>Effisiensi, effektivitas, akuntabilitas  dan transparansi dalam mengawal produk hukum daerah.</li>
          <li>Mewujudkan kebebasan dalam berkomunikasi  pada suatu <em>decision</em> secara formal  (wujud fasilitasi).</li>
        </ul>
        <p>e-PERDA  sebagai suatu sistem komunikasi berbasis elektronik <em>&ldquo;perda in my hand&rdquo;</em> terhubung dengan wujud fasilitasi yang diberikan  oleh Direktorat Jenderal Otonomi Daerah Kemendagri  dalam wujud:</p>
        <ul>
          <li>e-register</li>
          <li>e-fasilitasi <em>(live chat)</em></li>
          <li>e-konsultasi <em>(live chat)</em></li>
        </ul>
        <p>Direktorat  Jenderal Otonomi Daerah yang semula memberikan pelayanan kepada pemda pada  penyusunan perda sampai pada pemberian nomor register terhadap perda yang sudah  ditetapkan dengan e-PERDA:</p>
        <ul>
          <li>Nomor  register secara elektronik; </li>
          <li>Memberikan  kemudahan fasilitasi; </li>
          <li>Stakeholder  memonitor setiap proses pemberian nomor  register. </li>
        </ul>
        <p>Dengan  demikian melalui e-perda Direktorat Jenderal Otonomi Daerah menjawab tantangan  pembinaan perda/perkada berbasis sistem  elektronik yang mudah, murah, transparan dan akuntabel serta yang terpenting  adalah mewujudkan &ldquo;Negara hadir pada setiap sendi penyelenggaraan pemerintahan  daerah dengan mewujudkan perda dan perkada yang aspiratif, akuntabel dan  implementatif. Selanjutnya Buku  Panduan dapat menjadi <em>Guidance</em>/petunjuk bagi stakeholders daerah dalam  komunikasi Perda dan Perkada berbasis elektronik. Bagi masyarakat dapat  memonitor dan menyampaikan masukan terhadap kebijakan yang ditetapkan dalam  koridor masukan membangun.<br>
        Demikian,  semoga buku ini bermanfaat bagi kita semua dalam memajukan bangsa dan Negara.<br>
  <br>
          Secara garis besar, proses ePERDA dapat dilihat dari gambar di bawah ini:<br>
  <br>
        </p>
        <table width="100%" border="0" cellspacing="0" cellpadding="3">
          <tr>
            <td><img src="images/Aplikasi e perda.png" width="684" height="385"></td>
          </tr>
          <tr>
            <td align="center"><em class="sws_caption">Konsep global ePERDA</em></td>
          </tr>
        </table>
        <p>Pengguna Pemerintah Daerah dapat melakukan registrasi secara online melalui komputer/laptop dan bisa juga melalui smartphone di Website e-Perda, data registrasi user akan tersimpan pada server sistem E-Perda untuk kemudian Tim Verifikasi Registrasi E-Perda melakukan proses verifikasi registrasi sistem E-Perda, setelah Tim Verifikasi Registrasi menerima data registrasi maka user akan menerima notifikasi pada email yang terdaftar pada aplikasi dan bersama dengan diterima notifikasi oleh user artinya user sudah dapat melakukan login ke sistem E-Perda untuk melakukan input dokumen/berkas Perda dan Rancangan Perda. Dokumen/berkas yang sudah di inputkan oleh user akan tersimpan dalam server E-Perda dan akan langsung diterima oleh masing-masing Tim Persetujuan Substansi untuk ke-4 wilayah, proses persetujuan substansi dilakukan selama 2 hari kerja dan setelah itu Tim Persetujuan Substansi akan mengeluarkan persetujuan yang kemudian akan dilanjutkan kepada Tim Penyerasian Kebijakan, dari Tim Penyerasian Kebijakan akan ada proses pemberian nomor registrasi yang dilakukan selama 2 hari kerja, setelah Perda mendapatkan nomor registrasi maka nomor registrasi berkas dokumen Perda akan tersampaikan melalui sistem aplikasi E-Perda.<br>
  <br>
  <strong>ALUR PROSES REGISTER RANCANGAN PERATURAN DAERAH</strong><br>
  <br>
Alur proses register Rancangan Peraturan Daerah dapat dilihat pada gambar di bawah ini:  <br>
        </p>
        <table width="100%" border="0" cellspacing="0" cellpadding="3">
          <tr>
            <td><img src="images/Aplikasi e perda2.png" width="684" height="385"></td>
          </tr>
          <tr>
            <td align="center"><em class="sws_caption">Alur Kerja aplikasi ePERDA</em></td>
          </tr>
        </table>
        <p>Alur Kerja Aplikasi E-Perda, Pemerintah Daerah yang sudah melakukan proses registrasi akun dan telah disetujui oleh administrator dari pemerintah pusat maka pemerintah daerah mendapatkan notifikasi via email dan kemudian dapat melakukan login pada aplikasi E-Perda, terdapat 3 hal pokok dari aplikasi E-Perda yaitu E-Register untuk mendapatkan nomor register atau No. Reg, e-Konsultasi Publik untuk melakukan konsultasi secara online atau live chat, E-Fasilitasi untuk pembinaan berupa pemberian pedoman dan petunjuk teknis, arahan, bimbingan teknis, supervisi, asistensi dan kerja sama serta monitoring dan evaluasi. Pada E-Register yang harus dilakukan oleh pemerintah daerah untuk mendapatkan No. Reg yaitu pertama kali adalah pengajuan nomor register disertai dengan pengunggahan dokumen, setelah pengajuan nomor register dan pengunggahan dokumen tersebut pemerintah daerah akan menerima notifikasi dari masing-masing langkah yang dilakukan, pengajuan nomor register dan pengunggahan dokumen akan tersimpan pada Database E-Perda dan secara langsung pemerintah pusat akan menerima notifikasi tentang aktifitas tersebut, setelah pengunggahan dokumen akan dilakukan pemantauan proses oleh pemerintah pusat, apabila terdapat revisi atau perbaikan terhadap dokumen maka notifikasi revisi akan terkirim pada emai pemerintah daerah, tentunya agar pemerintah daerah bisa mendapatkan No.Reg harus melakukan perbaikan dokumen dan melakukan pengunggahan dokumen hasil revisi hingga dokumen disetujui oleh pemerintah pusat dan pemerintah daerah menerima hasil dari proses E-Register yaitu No.Reg.<br>
          E-Fasilitasi, pemerintah pusat akan memeriksa setiap dokumen yang dikirimkan oleh pemerintah daerah dengan melakukan pengunduhan dokumen dari database E-Perda, agar kemudian pemerintah pusat bisa melakukan fasilitasi terhadap pemerintah daerah hingga pemerintah pusat mendapatkan ranperda yang sesuai dengan kriteria maka persetujuan oleh pemerintah pusat bisa diberikan dan pengarsipan dokumen final bisa dilakukan. Sama sepertihalnya E-Register, sistem E-Fasilitasi juga akan mengirimkan notifikasi via email tentang kegiatan yang telah dilakukan Antara pemerintah daerah dan pemerintah pusat.<br>
          e-Konsultasi Publik, melalui e-Konsultasi Publik Pemerintah Daerah dapat melakukan bimbingan, asistensi dan konsultasi mengenai segala hal tentang Ranperda hingga Perda kepada pemerintah pusat, setiap komunikasi yang terjadi pada live chat e-Konsultasi Publik akan tersimpan juga didalam database e-Perda. <br>
        </p>
        <table width="100%" border="0" cellspacing="0" cellpadding="3">
          <tr>
            <td><img src="images/Aplikasi e perda3.png" width="684" height="385"></td>
          </tr>
          <tr>
            <td align="center"><em class="sws_caption">Alur Kerja aplikasi ePERDA</em></td>
          </tr>
        </table>
        <p>  <br>
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
        </p>
      </div>
    </div>
    <?php generate_footer(); ?>
  </div>
  <p>&nbsp;</p>
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript" src="js/image_fade.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
    });
  </script>
</body>
</html>
