<?php
	//baca kml
	//selama belum eof
	//	pos1 dan 2 = baca <PolyStyle><fill>0</fill></PolyStyle>
	//  pos3 dan 4 = baca <ExtendedData> dan </ExtendedData>
	//  pos5 dan 6 = baca <SimpleData name="GRIDCODE">4</SimpleData>
	//  cari grid code
	//  kalau grid code = 2 --> <fill>1</fill><color>d0d0fe</color></PolyStyle>
	//  kalau grid code = 3 --> <fill>1</fill><color>c3ffc3</color></PolyStyle>
	//  kalau grid code = 4 --> <fill>1</fill><color>6eff6e</color></PolyStyle>
	//  kalau grid code = 5 --> <fill>1</fill><color>00ff00</color></PolyStyle>
	//  bikin tambahan: 
	//		grid code = 2 --> <name>ZONA 2</name><description><![CDATA[<table width="200" border="1" cellspacing="0" cellpadding="3"><tr><td width="50" style="font-size: x-small">Produktivitas</td><td width="132" style="font-size: x-small">Kurang dari 4.64 Ton/Ha</td></tr><tr><td style="font-size: x-small">Kecamatan</td><td style="font-size: x-small">Sukra</td></tr></table>]]></description>
	//		grid code = 3 --> <name>ZONA 2</name><description><![CDATA[<table width="200" border="1" cellspacing="0" cellpadding="3"><tr><td width="50" style="font-size: x-small">Produktivitas</td><td width="132" style="font-size: x-small">4.64 - 6.11 Ton/Ha</td></tr><tr><td style="font-size: x-small">Kecamatan</td><td style="font-size: x-small">Sukra</td></tr></table>]]></description>
	//		grid code = 4 --> <name>ZONA 2</name><description><![CDATA[<table width="200" border="1" cellspacing="0" cellpadding="3"><tr><td width="50" style="font-size: x-small">Produktivitas</td><td width="132" style="font-size: x-small">6.11 - 7.52 Ton/Ha</td></tr><tr><td style="font-size: x-small">Kecamatan</td><td style="font-size: x-small">Sukra</td></tr></table>]]></description>
	//		grid code = 5 --> <name>ZONA 2</name><description><![CDATA[<table width="200" border="1" cellspacing="0" cellpadding="3"><tr><td width="50" style="font-size: x-small">Produktivitas</td><td width="132" style="font-size: x-small">Di atas 7.52 Ton/Ha</td></tr><tr><td style="font-size: x-small">Kecamatan</td><td style="font-size: x-small">Sukra</td></tr></table>]]></description>
	
	$f1 = file_get_contents("fz_kml.kml");
	$f2 = fopen("fz_jadi.kml", "w");
	
	$pstr = strlen($f1);
	$pos1 = 0;
	$pos3 = 0;
	$pos4 = 0;
	$pos5_1 = 0;
	$pos6_1 = 0;
	$pos5_2 = 0;
	$pos6_2 = 0;
	$pos5_3 = 0;
	$pos6_3 = 0;
	$pos5_4 = 0;
	$pos6_4 = 0;
	$terakhir = 0;
	$pertama = true;
	
	do {
		$pos1 = strpos($f1, "<Style><LineStyle><color>ff0000ff</color></LineStyle><PolyStyle><fill>0</fill></PolyStyle></Style>",$pos1+1);
		if( $pos1 === false ) {
			//udah gak ketemu lagi
			//tulis trailing
			$terakhir = $pos4 + 15;
			fwrite($f2, substr($f1,$terakhir));
			break;
		} else {
			//tulis dulu sampai point sebelum style
			//acuan
			if( $pos4 == 0 ) { $terakhir = 0; } else { $terakhir = $pos4 + 15; }
			fwrite($f2, substr($f1,$terakhir,$pos1 - $terakhir-1));
		}
		$pos3 = strpos($f1, "<ExtendedData>", $pos3 + 1);
		$pos4 = strpos($f1, "</ExtendedData>", $pos4 + 1);
		$pos5_1 = strpos($f1, "<SimpleData", $pos5_1 + 1);
		$pos6_1 = strpos($f1, "</SimpleData>", $pos6_1 + 1);
		$pos5_2 = strpos($f1, "<SimpleData", $pos5_1 + 1);
		$pos6_2 = strpos($f1, "</SimpleData>", $pos6_1 + 1);
		$pos5_3 = strpos($f1, "<SimpleData", $pos5_2 + 1);
		$pos6_3 = strpos($f1, "</SimpleData>", $pos6_2 + 1);
		$pos5_4 = strpos($f1, "<SimpleData", $pos5_3 + 1);
		$pos6_4 = strpos($f1, "</SimpleData>", $pos6_3 + 1);
		
		$ss_gc = substr($f1, $pos5_1 + 28, 1);
		//<SimpleData name="KECAMATAN">SUKRA</SimpleData>
		$ss_kc = substr($f1, $pos5_2 + 29, $pos6_2 - $pos5_2 - 29);
		if( $ss_gc == "2" ) { 
			$ns1 = "<Style><LineStyle><color>00000000</color><width>0.01</width></LineStyle><PolyStyle><fill>1</fill><color>ffd0d0fe</color></PolyStyle></Style>";
			$ns2 = '<name>ZONA 2</name><description><![CDATA[<table width="200" border="1" cellspacing="0" cellpadding="3"><tr><td width="50" style="font-size: x-small; color:#000000">Produktivitas</td><td width="132" style="font-size: x-small; color:#000000">Kurang dari 4.64 Ton/Ha</td></tr><tr><td style="font-size: x-small; color:#000000">Kecamatan</td>';
		 }
		if( $ss_gc == "3" ) { 
			$ns1 = "<Style><LineStyle><color>00000000</color><width>0.01</width></LineStyle><PolyStyle><fill>1</fill><color>ffc3ffc3</color></PolyStyle></Style>"; 
			$ns2 = '<name>ZONA 3</name><description><![CDATA[<table width="200" border="1" cellspacing="0" cellpadding="3"><tr><td width="50" style="font-size: x-small; color:#000000">Produktivitas</td><td width="132" style="font-size: x-small; color:#000000">4.64 - 6.11 Ton/Ha</td></tr><tr><td style="font-size: x-small; color:#000000">Kecamatan</td>';
		}
		if( $ss_gc == "4" ) { 
			$ns1 = "<Style><LineStyle><color>00000000</color><width>0.01</width></LineStyle><PolyStyle><fill>1</fill><color>ff6eff6e</color></PolyStyle></Style>"; 
			$ns2 = '<name>ZONA 4</name><description><![CDATA[<table width="200" border="1" cellspacing="0" cellpadding="3"><tr><td width="50" style="font-size: x-small; color:#000000">Produktivitas</td><td width="132" style="font-size: x-small; color:#000000">6.11 - 7.52 Ton/Ha</td></tr><tr><td style="font-size: x-small; color:#000000">Kecamatan</td>';
		}
		if( $ss_gc == "5" ) { 
			$ns1 = "<Style><LineStyle><color>00000000</color><width>0.01</width></LineStyle><PolyStyle><fill>1</fill><color>ff00ff00</color></PolyStyle></Style>"; 
			$ns2 = '<name>ZONA 5</name><description><![CDATA[<table width="200" border="1" cellspacing="0" cellpadding="3"><tr><td width="50" style="font-size: x-small; color:#000000">Produktivitas</td><td width="132" style="font-size: x-small; color:#000000">Di atas 7.52 Ton/Ha</td></tr><tr><td style="font-size: x-small; color:#000000">Kecamatan</td>';
		}
		$ns2 .= '<td style="font-size: x-small; color:#000000">'.$ss_kc;
		$ns2 .= '</td></tr></table>]]></description>';
		
		fwrite($f2, $ns1);
		fwrite($f2, $ns2);
		
		$pos5_1 = $pos5_4 + 1;
		$pos6_1 = $pos6_4 + 1;
	} while ($pos1 != 0 );
	fclose($f2);
	echo "Selesai";
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
</body>
</html>