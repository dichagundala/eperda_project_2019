<?php include_once("zz_koneksi_db.php"); ?>
<?php include ("zz_generate_menu.php"); ?>
<?php
	//session_start();
	//$host  = $_SERVER['HTTP_HOST'];
	//$uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
	//$ip1 = $_SERVER['HTTP_X_FORWARDED_FOR'];
	//$ip2 = $_SERVER['REMOTE_ADDR'];
	$waktu = date("Y-m-d H:i:s");
	//$extra = "pd_login.php";
	if( $_SESSION["sws_id"] == "" || !isset($_SESSION["sws_id"]) || $_SESSION["sws_id"] == 0 ) {
		echo "<script>window.location.href=\"pd_login.php\";</script>";
		//header("Location: http://$host$uri/$extra");
		exit;
	}
	if( $_SESSION["sws_tingkat"] != 99 ) {
		echo "<script>window.location.href=\"pd_login.php\";</script>";
		//header("Location: http://$host$uri/$extra");
		exit;
	}
?>


<!DOCTYPE HTML>
<html>

<head>
  <title>.:: ePERDA - Kementerian Dalam Negeri ::.</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>

<script language="javascript">

function go_edit(a) {
	document.form_edit.id.value = a;
	document.form_edit.submit();
}

function go_hapus(a) {
	var ans; 
	ans=window.confirm('Yakin akan menghapus pendaftaran pengguna ini?');
	if( ans == true ) {
		document.form_hapus.id.value = a;
		document.form_hapus.submit();
	}
}

var kab_arr = new Array();
var kab_opt = new Array();
var acounter = 0;
kab_arr[0] = new Array(" --- Pilih --- ");
kab_opt[0] = new Array('0');
<?php
	$hsl = mysqli_query($conn, "select kode from tbl_prov");
	while( $B = mysqli_fetch_array($hsl) ) {
		$kdprop = $B[0];
		$hsl1 = mysqli_query($conn, "select kode_kab, nama from tbl_kab where kode_prov='$kdprop' order by kode_kab");
		$calar = "\" --- Pilih --- \"";
		$oplar = "\"0\"";
		while( $B1 = mysqli_fetch_array($hsl1) ) {
			$kdkab = $B1[0];
			if( $calar == "" ) { $calar = "\"".$B1[1]."\""; } else { $calar .= ",\"".$B1[1]."\""; }
			if( $oplar == "" ) { $oplar = "\"".$B1[0]."\""; } else { $oplar .= ",\"".$B1[0]."\""; }
		}
?>
		kab_arr[<?php echo $kdprop; ?>] = new Array(<?php echo $calar; ?>);
		kab_opt[<?php echo $kdprop; ?>] = new Array(<?php echo $oplar; ?>);
<?php
	}
?>

function change_kab(combo1){
	var comboValue = combo1.value;
	document.forms["form1"].elements["kab"].options.length=0;
	for (var i=0;i<kab_arr[comboValue].length;i++){
		var option = document.createElement("option");
		option.setAttribute('value',kab_opt[comboValue][i]);
		option.innerHTML = kab_arr[comboValue][i];
		document.forms["form1"].elements["kab"].appendChild(option);
	}
}


function change_01_kab(combo1, indikator){
	var selin = 0;
	var comboValue = document.forms["form1"].elements[combo1].value;
	document.forms["form1"].elements["kab"].options.length=0;
	for (var i=0;i<kab_arr[comboValue].length;i++){
		var option = document.createElement("option");
		option.setAttribute('value',kab_opt[comboValue][i]);
		option.innerHTML = kab_arr[comboValue][i];
		document.forms["form1"].elements["kab"].appendChild(option);
		if( kab_opt[comboValue][i] == indikator ) { selin = i; }
	}
	document.forms["form1"].elements["kab"].options.selectedIndex=selin;
}


</script>  
  
</head>

<body>
  <div id="main">
    <header>
      <?php generate_logo(); ?>
      <?php generate_menu(5); ?>
    </header>
    <div id="site_content">
      <div class="gallery"><?php generate_gallery($conn, 950, 150); ?></div>
      <div class="content_webgis">
        <h1><strong>Manajemen Pendaftaran</strong>        </h1>
        <table width="100%" border="0" cellspacing="0" cellpadding="3">
          <tr align="center">
            <td width="4%"><strong>No</strong></td>
            <td width="11%"><strong>Aksi</strong></td>
            <td width="50%"><strong>Nama</strong></td>
            <td width="28%"><strong>Otoritas</strong></td>
            <td width="7%"><strong>Status</strong></td>
          </tr>
<?php
	$ctr = 0;
	$hsl = mysqli_query($conn, "select * from tbl_pendaftaran");
	while( $B = mysqli_fetch_array($hsl) ) {
		$ctr += 1;
		$id = $B[0];
		$nlog = $B[1];
		$nlengkap = $B[2];
		$tingkat = $B[4];
		$prov = $B[5];
		$kab = $B[6];
		$oto = $B[7];
		if( $oto == 1 ) { 
			$otok = "Aktif"; 
			$td_edit = "<td width=\"50%\">&nbsp;</td>";
		} else { 
			$otok = "Belum disetujui"; 
			$td_edit = "<td width=\"50%\" onclick=\"go_edit('$id');\">Edit</td>";
		}
		$td_hapus = "<td width=\"50%\" onclick=\"go_hapus('$id');\">Hapus</td>";
?>		
          <tr valign="top">
            <td><?php echo $ctr; ?></td>
            <td><table width="100%" border="0" cellpadding="3" cellspacing="0" class="sws_table">
              <tr>
                <?php echo $td_edit; ?>
                <?php echo $td_hapus; ?> 
              </tr>
            </table></td>
            <td><table width="100%" border="0" cellpadding="3" cellspacing="0" class="sws_table">
              <tr>
                <td width="26%">Nama Login</td>
                <td width="4%">:</td>
                <td width="70%"><?php echo $nlog; ?></td>
              </tr>
              <tr>
                <td>Nama Lengkap</td>
                <td>:</td>
                <td><?php echo $nlengkap; ?></td>
              </tr>
            </table>
            <span class="sws_table"></span></td>
            <td><table width="100%" border="0" cellpadding="3" cellspacing="0" class="sws_table">
<?php
		if( $tingkat == 99 ) {
?>			            
              <tr>
                <td>Administrator</td>
              </tr>
<?php
		} elseif( $tingkat == 1 ) {
?>			            
              <tr>
                <td>Supervisor</td>
              </tr>
<?php
		} elseif( $tingkat == 2 ) {
?>			            
              <tr>
                <td>Seksi Wilayah <?php echo $prov; ?></td>
              </tr>
<?php
		} elseif( $tingkat == 10 ) {
			$hsl1 = mysqli_query($conn, "select nama from tbl_prov where kode='$prov'");
			$B1 = mysqli_fetch_array($hsl1);
			$np = $B1[0];
?>			            
              <tr>
                <td>Pengguna tingkat Provinsi</td>
              </tr>
              <tr>
                <td><?php echo $np; ?></td>
              </tr>              
<?php
		} elseif( $tingkat == 11 ) {
			$hsl1 = mysqli_query($conn, "select nama from tbl_prov where kode='$prov'");
			$B1 = mysqli_fetch_array($hsl1);
			$np = $B1[0];
			$hsl1 = mysqli_query($conn, "select nama from tbl_kab where kode_kab='$kab'");
			$B1 = mysqli_fetch_array($hsl1);
			$nk = $B1[0];
?>			            
              <tr>
                <td>Pengguna tingkat Kabupaten/Kota</td>
              </tr>
              <tr>
                <td><?php echo $nk; ?></td>
              </tr>
              <tr>
                <td><?php echo $np; ?></td>
              </tr>              
<?php
		}
?>
            </table></td>
            <td><?php echo $otok; ?></td>
          </tr>
<?php
	}
?>
        </table>
        <p>&nbsp;</p>
      </div>
    </div>
    <?php generate_footer(); ?>
  </div>
  <p>&nbsp;</p>
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript" src="js/image_fade.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
    });
  </script>
  
<form action="pd_man_daftar_edit.php" method="post" name="form_edit">
<input name="id" type="hidden" value="" />
</form>
<form action="pd_man_daftar_del.php" method="post" name="form_hapus">
<input name="id" type="hidden" value="" />
</form>  
  
</body>
</html>
