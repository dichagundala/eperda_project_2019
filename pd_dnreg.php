
<?php
	include_once("zz_koneksi_db.php");
	include ("zz_generate_menu.php"); 
	//session_start();
	//$host  = $_SERVER['HTTP_HOST'];
	$uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
	//$ip1 = $_SERVER['HTTP_X_FORWARDED_FOR'];
	$ip2 = $_SERVER['REMOTE_ADDR'];
	$waktu = date("Y-m-d H:i:s");
	$extra = "pd_login.php";
	if( $_SESSION["sws_id"] == "" || !isset($_SESSION["sws_id"]) || $_SESSION["sws_id"] == 0 ) {
		echo "<script>window.location.href=\"pd_login.php\";</script>";
		//header("Location: http://$host$uri/$extra");
		exit;
	}
	if( $_SESSION["sws_tingkat"] == 99 || $_SESSION["sws_tingkat"] == 1 || $_SESSION["sws_tingkat"] == 2 ) {
	} else {
		echo "<script>window.location.href=\"pd_login.php\";</script>";
		//header("Location: http://$host$uri/$extra");
		exit;
	}
	if( isset($_POST["proses"] )) {
		$a = $_POST["a"];
		$b = $_POST["b"];
		$c = $_POST["c"];
		$d = $_POST["d"];
	} else {
		if( isset($_GET["a"] )== "" || !isset($_GET["a"]) ) { $a = 0; } else { $a = $_GET["a"]; }
		$b = 0;
		$c = 0;
		$d = 1;
	}
	if( $a == 0 ) { $rstr = " 1=1 "; } 
	else {
		$rstr = "";
		if( $_SESSION["sws_tingkat"] == 2 ) {
			$hsl = mysqli_query($conn, "select distinct reg from tbl_region");
			while( $B = mysqli_fetch_array($hsl) ) {
				$areg = $B[0];
				$hsl1 = mysqli_query($conn, "select prov from tbl_region where reg='$areg'");
				while( $B1 = mysqli_fetch_array($hsl1) ) {
					if( $rstr == "" ) { $rstr = " prov IN ('".$B1[0]."'"; } else { $rstr .= ",'".$B1[0]."'"; }
				}
				$rstr .= ") ";
			}
		}
		if( $rstr == "" ) { $rstr = " 1=1 "; }
	}
	if( $b == 0 ) { $ostr = " ORDER BY id"; }
	if( $b == 1 ) { $ostr = " ORDER BY prov"; }
	if( $b == 2 ) { $ostr = " ORDER BY judul"; }
	if( $b == 3 ) { $ostr = " ORDER BY cnoreg"; }
	if( $b == 4 ) { $ostr = " ORDER BY tkt"; }
	if( $b == 5 ) { $ostr = " ORDER BY wkirim"; }
	if( $c == 0 ) { $ostr .= " DESC"; } else { $ostr .= " ASC"; }
	
	$hsl = mysqli_query($conn, "select count(id) from tbl_reg_ranperda where $rstr");
	if( mysqli_num_rows($hsl) == 0 ) { $jumlah_record = 0; }
	else {
		$B = mysqli_fetch_array($hsl);
		if( $B[0] == "" || !isset($B[0]) ) { $jumlah_record = 0; } else { $jumlah_record = $B[0]; }
	}
	$perhal = 10;
	$maxhal = floor($jumlah_record / $perhal);
	if( $maxhal * $perhal != $jumlah_record ) { $maxhal += 1; }
	$str_limit = " LIMIT ".(($d - 1)*$perhal).", ".$perhal;
	$fixquery = "select * from tbl_reg_ranperda where $rstr $ostr $str_limit";
	//echo "FIXQUERY --> ".$fixquery;
	
	
	function get_waktu($a) {
		$dum = explode(" ",$a);
		$dum1 = explode("-", $dum[0]);
		$ostr = $dum1[2];
		if( $dum1[1] == "01" ) { $ostr .= " Januari ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "02" ) { $ostr .= " Februari ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "03" ) { $ostr .= " Maret ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "04" ) { $ostr .= " April ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "05" ) { $ostr .= " Mei ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "06" ) { $ostr .= " Juni ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "07" ) { $ostr .= " Juli ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "08" ) { $ostr .= " Agustus ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "09" ) { $ostr .= " September ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "10" ) { $ostr .= " Oktober ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "11" ) { $ostr .= " November ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "12" ) { $ostr .= " Desember ".$dum1[0]." ".$dum[1]; }
		
		$tnow = time();
		$tunduh = strtotime($a);
		$time = $tnow - $tunduh;
		$time = ($time < 1) ? 1 : $time;
		$tokens = array(
			31536000 => 'tahun',
			2592000  => 'bulan',
			604800   => 'minggu',
			86400    => 'hari',
			3600     => 'jam',
			60		 => 'menit',
			1		 => 'detik');
			
		foreach($tokens as $unit => $text ) {
			if( $time < $unit ) continue;
			$nou = floor($time / $unit);
			$et = $nou." ".$text;
			break;
		}
		return $ostr." (".$et." yang lalu)";
	}
	
	function get_waktu1($a) {
		if( $a == "0000-00-00" ) { return "Tidak diketahui"; }
		$dum = explode(" ",$a);
		$dum1 = explode("-", $dum[0]);
		$ostr = $dum1[2];
		$dum[1] = "";
		if( $dum1[1] == "01" ) { $ostr .= " Januari ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "02" ) { $ostr .= " Februari ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "03" ) { $ostr .= " Maret ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "04" ) { $ostr .= " April ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "05" ) { $ostr .= " Mei ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "06" ) { $ostr .= " Juni ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "07" ) { $ostr .= " Juli ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "08" ) { $ostr .= " Agustus ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "09" ) { $ostr .= " September ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "10" ) { $ostr .= " Oktober ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "11" ) { $ostr .= " November ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "12" ) { $ostr .= " Desember ".$dum1[0]." ".$dum[1]; }
		
		return $ostr;
	}
?>


<!DOCTYPE HTML>
<html>

<head>
  <title>.:: ePERDA - Kementerian Dalam Negeri ::.</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>

<script language="javascript">

function rubah_data() {
	document.form_tampil.b.value = document.form1.b.value;
	document.form_tampil.c.value = document.form1.c.value;
	document.form_tampil.d.value = document.form1.d.value;
	document.form_tampil.submit();
}

function go_edit(a) {
	document.form_edit.id.value = a;
	document.form_edit.submit();
}

function go_history(a, b) {
	document.form_history.id.value = a;
	document.form_history.token.value = b;
	document.form_history.submit();
}

function unduh(b, a, c) {
	document.form_unduh.id.value = a;
	document.form_unduh.jenis.value = b;
	document.form_unduh.tabel.value = c;
	document.form_unduh.submit();
}

function review(a) {
	document.form_review.id.value = a;
	document.form_review.submit();
}

function disposisi(a) {
	document.form_disposisi.id.value = a;
	document.form_disposisi.submit();
}

function go_hapus(a) {
	var ans; 
	ans=window.confirm('Yakin akan menon-aktifkan pengguna ini?');
	if( ans == true ) {
		document.form_hapus.id.value = a;
		document.form_hapus.submit();
	}
}



</script>  
  
</head>

<body>
  <div id="main">
    <header>
      <?php generate_logo(); ?>
      <?php generate_menu(5); ?>
    </header>
    <div id="site_content">
      <div class="gallery"><?php generate_gallery($conn, 950, 150); ?></div>
      <div class="content_webgis_up">
        <h1>Daftar Pengajuan Register PERDA</h1>
<?php
	if( $jumlah_record == 0 ) {
?>		     
	<strong><br>
<br>
        TIDAK ADA DATA</strong><br>
        <br>
        <br>
<br>
        <br>
        <br>
<br><br><br><br>
<?php
	} else {
?>		
        <br>
        <br>
        <form name="form1" method="post" action="">
          <table width="100%" border="0" cellspacing="0" cellpadding="3">
            <tr>
              <td width="44%">Ditemukan <?php echo number_format($jumlah_record); ?> Pengajuan Register PERDA</td>
              <td width="41%" align="right">Urut berdasar
                <select name="b" id="b" onChange="rubah_data();">
                  <option value="0" <?php if( $b == 0 ) { ?> selected <?php } ?>>ID</option>
                  <option value="1" <?php if( $b == 1 ) { ?> selected <?php } ?>>Provinsi</option>
                  <option value="2" <?php if( $b == 2 ) { ?> selected <?php } ?>>Judul Ranperda</option>
                  <option value="3" <?php if( $b == 3 ) { ?> selected <?php } ?>>No Register</option>
                  <option value="4" <?php if( $b == 4 ) { ?> selected <?php } ?>>Tingkat Wilayah</option>
                  <option value="5" <?php if( $b == 5 ) { ?> selected <?php } ?>>Waktu Pengunggahan</option>
              </select>
                <select name="c" id="c" onChange="rubah_data();">
                  <option value="0" <?php if( $c == 0 ) { ?> selected <?php } ?>>Menurun</option>
                  <option value="1" <?php if( $c == 1 ) { ?> selected <?php } ?>>Meningkat</option>
              </select></td>
              <td width="15%" align="right">Ke Halaman 
                <select name="d" id="d" onChange="rubah_data();">
<?php
		for( $iix = 1; $iix <= $maxhal; $iix++ ) {
?>
					<option value="<?php echo $iix; ?>" <?php if( $iix == $d ) { ?> selected <?php } ?>><?php echo $iix; ?></option>
<?php
		}
?>                
              </select></td>
            </tr>
          </table>
        </form>
        <br><br>
<?php
		$hsl = mysqli_query($conn, $fixquery);
		while( $B = mysqli_fetch_array($hsl) ) {
			$token = $B["utoken"];
			$anid = $B["id"];
			$ada1 = 1;
			if( $B["nfile_asli"] == "" || !isset($B["nfile_asli"]) ) {
				$brp = "<td width=\"25%\" style=\"font-size:small\">Berkas Ranperda tidak ada</td>";
			} else {
				$brp = "<td width=\"25%\" style=\"font-size:small\" onClick=\"unduh(1, '$anid', '1');\" onMouseOver=\"this.style.cursor='pointer';\"><u>Unduh Berkas Ranperda</u></td>";
			}
			if( $B["kepdprd_asli"] == "" || !isset($B["kepdprd_asli"]) ) {
				$bsk = "<td width=\"27%\" style=\"font-size:small\">Berkas Keputusan DPRD belum ada</td>";
			} else {
				$bsk = "<td width=\"27%\" style=\"font-size:small\" onClick=\"unduh(2, '$anid', '1');\" onMouseOver=\"this.style.cursor='pointer';\"><u>Unduh Berkas Keputusan DPRD</u></td>";
			}
			if( $B["sp_asli"] == "" || !isset($B["sp_asli"]) ) {
				$bsp = "<td width=\"30%\" style=\"font-size:small\">Surat Permohonan belum ada</td>";
			} else {
				$bsp = "<td width=\"30%\" style=\"font-size:small\" onClick=\"unduh(3, '$anid', '1');\" onMouseOver=\"this.style.cursor='pointer';\"><u>Unduh Surat Permohonan</u></td>";
			}
			if( $B["kepmen_asli"] == "" || !isset($B["kepmen_asli"]) ) {
				$bkm = "<td width=\"25%\" style=\"font-size:small\">Surat Keputusan Menteri tidak ada</td>";
			} else {
				$bkm = "<td width=\"25%\" style=\"font-size:small\" onClick=\"unduh(4, '$anid', '1');\" onMouseOver=\"this.style.cursor='pointer';\"><u>Unduh Surat Keputusan Menteri</u></td>";
			}
			if( $B["skreg_asli"] == "" || !isset($B["skreg_asli"]) ) {
				$bsr = "<td width=\"27%\" style=\"font-size:small\">Surat Keputusan Register belum ada</td>";
			} else {
				$bsr = "<td width=\"27%\" style=\"font-size:small\" onClick=\"unduh(5, '$anid', '1');\" onMouseOver=\"this.style.cursor='pointer';\"><u>Unduh Surat Keputusan Register</u></td>";
			}
			if( $B["btamb_asli"] == "" || !isset($B["btamb_asli"]) ) {
				$bbt = "<td width=\"30%\" style=\"font-size:small\">&nbsp;</td>";
			} else {
				$bbt = "<td width=\"30%\" style=\"font-size:small\" onClick=\"unduh(6, '$anid', '1');\" onMouseOver=\"this.style.cursor='pointer';\"><u>Unduh Berkas Penunjang</u></td>";
			}
			if( $B["jenis"] == 1 ) { $jeval = "Evaluasi"; } else { $jeval = "Non evaluasi"; }
			$hsl1 = mysqli_query($conn, "select nama from tbl_prov where kode='".$B["prov"]."'");
			$B1 = mysqli_fetch_array($hsl1);
			$wilayah = $B1[0];
			if( $B["kabkota"] != 0 ) {
				$hsl1 = mysqli_query($conn, "select nama from tbl_kab where kode_kab='".$B["kabkota"]."'");
				$B1 = mysqli_fetch_array($hsl1);
				$wilayah .= " / ".$B1[0];
			}
			if( $B["sproses"] == 1 ) { $astat = "<font color=\"#FF0000\"><strong>Menunggu diproses</strong></font>"; }
			if( $B["sproses"] == 2 ) { $astat = "<font color=\"#000033\">Sedang diproses</font>"; }
			if( $B["sproses"] == 3 ) { $astat = "<font color=\"#00CC00\">Sudah disetujui</font>"; }
			$ringkasan = str_replace (array("\r\n", "\n", "\r"), '<br>', $B["ringkasan"]);
			
?>
 <table width="100%" border="0" cellspacing="0" cellpadding="3">
          <tr>
            <td><table class="sws_table" width="100%" border="0" cellspacing="0" cellpadding="10" style="border-top: 2px solid #000;">
              <tr valign="top">
                <td width="21%">Judul</td>
                <td width="2%">&nbsp;</td>
                <td colspan="3"><strong><?php echo $B["judul"]; ?></strong></td>
              </tr>
<?php
	if( $B["sproses"] == 3 ) {
?>		              
              <tr valign="top">
                <td>No Register</td>
                <td>&nbsp;</td>
                <td colspan="3"><?php echo $B["noreg"]; ?></td>
              </tr>
              <tr valign="top">
                <td>Tgl Dikeluarkan</td>
                <td>&nbsp;</td>
                <td colspan="3"><?php echo get_waktu1($B["tgl_keluar"]); ?></td>
              </tr>
<?php
	}
?>
              <tr valign="top">
                <td width="21%">Jenis</td>
                <td width="2%">&nbsp;</td>
                <td colspan="3"><?php echo $jeval; ?></td>
              </tr>
              <tr valign="top">
                <td width="21%">Klasifikasi</td>
                <td width="2%">&nbsp;</td>
                <td colspan="3"><?php echo $B["klasifikasi"]; ?></td>
              </tr>
              <tr valign="top">
                <td>Ringkasan</td>
                <td>&nbsp;</td>
                <td colspan="3"><?php echo $ringkasan; ?></td>
              </tr>
              <tr valign="top">
                <td>Waktu Pengajuan</td>
                <td>&nbsp;</td>
                <td colspan="3"><?php echo get_waktu($B["wkirim"]); ?></td>
              </tr>
              <tr valign="top">
                <td>Provinsi / Kabupaten/Kota</td>
                <td>&nbsp;</td>
                <td colspan="3"><?php echo $wilayah; ?></td>
              </tr>
              <tr valign="top">
                <td>Status Terakhir</td>
                <td>&nbsp;</td>
                <td colspan="3"><?php echo $astat; ?></td>
              </tr>
              <tr valign="top">
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr valign="top">
<?php
	if( $B["sproses"] == 1 ) {
?>		
		<td onClick="disposisi('<?php echo $anid; ?>');" onMouseOver="this.style.cursor='pointer';"><u>Disposisi</u></td>
<?php
	} elseif( $B["sproses"] == 2 ) {
?>		              
		<td onClick="review('<?php echo $anid; ?>');" onMouseOver="this.style.cursor='pointer';"><u>SK Register</u></td>
<?php
	} else {
?>
				<td>&nbsp;</td>
<?php
	}
?>
                <td>&nbsp;</td>
                <?php echo $brp; ?><?php echo $bsk; ?><?php echo $bsp; ?>
              </tr>
<?php
	if( $B["sproses"] == 3 ) {
?>
              <tr valign="top">
				<td>&nbsp;</td>
                <td>&nbsp;</td>
                <?php echo $bkm; ?><?php echo $bsr; ?><?php echo $bbt; ?>
              </tr>
<?php
	}
?>
            </table></td>
          </tr>
        </table>              
<?php
		}
?>

        <p>&nbsp;</p>
<?php
	}
?>
        
      </div>
    </div>
    <?php generate_footer(); ?>
  </div>
  <p>&nbsp;</p>
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript" src="js/image_fade.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
    });
  </script>
  
<form action="pd_dnreg.php" method="post" name="form_tampil">
<input name="a" type="hidden" value="<?php echo $a; ?>" />
<input name="b" type="hidden" value="" />
<input name="c" type="hidden" value="" />
<input name="d" type="hidden" value="" />
<input name="proses" type="hidden" value="1" />
</form>
<form action="pd_ereview.php" method="post" name="form_review">
<input name="id" type="hidden" value="" />
</form>
<form action="pd_edisposisi.php" method="post" name="form_disposisi">
<input name="id" type="hidden" value="" />
</form>
<form action="pd_history.php" method="post" name="form_history">
<input name="id" type="hidden" value="" />
<input name="token" type="hidden" value="" />
</form>
<form action="pd_unduh_01.php" method="post" name="form_unduh">
<input name="id" type="hidden" value="" />
<input name="jenis" type="hidden" value="" />
<input name="tabel" type="hidden" value="" />
</form>  
  
</body>
</html>
