<?php include_once("zz_koneksi_db.php"); ?>
<?php include ("zz_generate_menu.php"); ?>
<?php
	session_start();
	$host  = $_SERVER['HTTP_HOST'];
	$uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
	$ip1 = $_SERVER['HTTP_X_FORWARDED_FOR'];
	$ip2 = $_SERVER['REMOTE_ADDR'];
	$waktu = date("Y-m-d H:i:s");
	$extra = "pd_login.php";
	if( $_SESSION["sws_id"] == "" || !isset($_SESSION["sws_id"]) || $_SESSION["sws_id"] == 0 ) {
		echo "<script>window.location.href=\"pd_login.php\";</script>";
		//header("Location: http://$host$uri/$extra");
		exit;
	}
	if( $_SESSION["sws_tingkat"] == 99 || $_SESSION["sws_tingkat"] == 1 || $_SESSION["sws_tingkat"] == 2 ) {
	} else {
		echo "<script>window.location.href=\"pd_login.php\";</script>";
		//header("Location: http://$host$uri/$extra");
		exit;
	}
	
	function get_waktu($a) {
		$dum = explode(" ",$a);
		$dum1 = explode("-", $dum[0]);
		$dum[1] = "";
		$ostr = $dum1[2];
		if( $dum1[1] == "01" ) { $ostr .= " Januari ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "02" ) { $ostr .= " Februari ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "03" ) { $ostr .= " Maret ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "04" ) { $ostr .= " April ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "05" ) { $ostr .= " Mei ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "06" ) { $ostr .= " Juni ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "07" ) { $ostr .= " Juli ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "08" ) { $ostr .= " Agustus ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "09" ) { $ostr .= " September ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "10" ) { $ostr .= " Oktober ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "11" ) { $ostr .= " November ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "12" ) { $ostr .= " Desember ".$dum1[0]." ".$dum[1]; }
		return $ostr;
	}
	
?>


<!DOCTYPE HTML>
<html>

<head>
  <title>.:: ePERDA - Kementerian Dalam Negeri ::.</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>
  <script type="text/javascript" src="amcharts/amcharts.js"></script>
  <script type="text/javascript" src="amcharts/serial.js"></script>
  <script type="text/javascript" src="amcharts/themes/dark.js"></script>
  <script src="amcharts/plugins/dataloader/dataloader.min.js"></script>

<script language="javascript">
function unduh(b, a, c) {
	document.form_unduh.id.value = a;
	document.form_unduh.jenis.value = b;
	document.form_unduh.tabel.value = c;
	document.form_unduh.submit();
}

function go_history(a, b) {
	document.form_hist.id.value = a;
	document.form_hist.token.value = b;
	document.form_hist.submit();
}

function toggle_kepmen() {
	var aval = document.form1.sproses.value;
	if( aval == 3 ) {
		document.getElementById("kepmen1").style.display = "";
		document.getElementById("kepmen2").style.display = "";
		document.getElementById("kepmen3").style.display = "";
	} else {
		document.getElementById("kepmen1").style.display = "none";
		document.getElementById("kepmen2").style.display = "none";
		document.getElementById("kepmen3").style.display = "none";
	}
}

function checkit() {
	var errorMsg = "";
	var aval = document.form1.sproses.value;
	if( aval == 3 ) {
		if( document.form1.bkepmen.value == "" ) { errorMsg += "\n\t - Masukkan berkas Surat Keputusan Menteri dahulu";}
		if (document.form1.nkepmen.value =="" ){ errorMsg += "\n\t - Nomor Surat Keputusan Menteri belum diisi";}
	}
	if (errorMsg != ""){
		msg  = "___________________________________________________________________\n\n";
		msg += "Fasilitasi belum bisa dilakukan dikarenakan kesalahan berikut ini.\n";
		msg += "___________________________________________________________________\n\n";
		errorMsg += alert(msg + errorMsg + "\n\n");
		return false;
	}
		
	return true;
}

</script>  
  
</head>

<body>
  <div id="main">
    <header>
      <?php generate_logo(); ?>
      <?php generate_menu(5); ?>
    </header>
    <div id="site_content">
      <div class="gallery"><?php generate_gallery($conn, 950, 150); ?></div>
      <div class="content">
        <h1>Waktu Respond Fasilitasi Rancangan PERDA</h1>
        <br>
        <p>Data hari ini (<?php echo get_waktu(date("Y-m-d H:i:s")); ?> )<br>
          <br>
          <br><div id="chart1div" style="width: 100%; height: 350px;"></div>
          <br>
          Data sampai dengan hari ini (<?php echo get_waktu(date("Y-m-d H:i:s")); ?> )<br>
          <br>
          <br><div id="chart2div" style="width: 100%; height: 350px;"></div>
        </p></div>
    </div>
    <?php generate_footer(); ?>
  </div>
  <p>&nbsp;</p>
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript" src="js/image_fade.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
    });
	
	
	AmCharts.loadJSON = function(url) {
	if (window.XMLHttpRequest) {
    // IE7+, Firefox, Chrome, Opera, Safari
    	var request = new XMLHttpRequest();
  	} else {
    // code for IE6, IE5
    	var request = new ActiveXObject('Microsoft.XMLHTTP');
  }
  request.open('GET', url, false);
  request.send();
  // parse adn return the output
  return eval(request.responseText);
  
	};
	
	var chartData1 = AmCharts.loadJSON('zz_tk_jl_fas.php?a=1');
	var chart1 = AmCharts.makeChart("chart1div", {
	"type": "serial",
	"dataProvider": chartData1,
    "pathToImages": "http://cdn.amcharts.com/lib/3/images/",
    "categoryField": "Wilayah",
	"categoryAxis": { 
		"gridPosition": "start",
		"fontSize": 10 },
	"valueAxis": {
		"labelsEnabled": true
	},
	"chartCursor": {},
	"trendLines": [],
    "graphs": [ {
	  "balloonText": "Jml Permohonan:[[jml1]]",
      "valueField": "jml1",
	  "fillAlphas": 0.8,
	  "fontSize": 8,
	  "labelText": "[[jml1]]",
	  "fillColor": "#8d0404",
	  "lineColor": "#8d0404",
	  "type": "column",
      "lineThickness ": 4
    }, {
	  "balloonText": "Jml Tindak Lanjut:[[jml2]]",
      "valueField": "jml2",
	  "fillAlphas": 0.8,
	  "fontSize": 8,
	  "labelText": "[[jml2]]",
	  "fillColor": "#00FF00",
	  "lineColor": "#00FF00",
	  "type": "column",
      "lineThickness ": 4
    }]
  } );
  
	var chartData2 = AmCharts.loadJSON('zz_tk_jl_fas.php?a=2');
	var chart2 = AmCharts.makeChart("chart2div", {
	"type": "serial",
	"dataProvider": chartData2,
    "pathToImages": "http://cdn.amcharts.com/lib/3/images/",
    "categoryField": "Wilayah",
	"categoryAxis": { 
		"gridPosition": "start",
		"fontSize": 10 },
	"valueAxis": {
		"labelsEnabled": true
	},
	"chartCursor": {},
	"trendLines": [],
    "graphs": [ {
	  "balloonText": "Jml Permohonan:[[jml1]]",
      "valueField": "jml1",
	  "fillAlphas": 0.8,
	  "fontSize": 8,
	  "labelText": "[[jml1]]",
	  "fillColor": "#8d0404",
	  "lineColor": "#8d0404",
	  "type": "column",
      "lineThickness ": 4
    }, {
	  "balloonText": "Jml Fasilitasi:[[jml2]]",
      "valueField": "jml2",
	  "fillAlphas": 0.8,
	  "fontSize": 8,
	  "labelText": "[[jml2]]",
	  "fillColor": "#00FF00",
	  "lineColor": "#00FF00",
	  "type": "column",
      "lineThickness ": 4
    }]
  } );  
  </script>

<form action="pd_man_user_edit.php" method="post" name="form_edit">
<input name="id" type="hidden" value="" />
</form>
<form action="pd_man_user_del.php" method="post" name="form_hapus">
<input name="id" type="hidden" value="" />
</form>
<form action="pd_unduh_01.php" method="post" name="form_unduh">
<input name="id" type="hidden" value="" />
<input name="jenis" type="hidden" value="" />
<input name="tabel" type="hidden" value="" />
</form>
<form action="pd_history.php" method="post" name="form_hist">
<input name="id" type="hidden" value="" />
<input name="token" type="hidden" value="" />
</form>
  
</body>
</html>
