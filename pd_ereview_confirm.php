<?php include_once("zz_koneksi_db.php"); ?>
<?php
	require 'PHPMailer/PHPMailerAutoload.php';
	session_start();
	$host  = $_SERVER['HTTP_HOST'];
	$uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
	$ip1 = $_SERVER['HTTP_X_FORWARDED_FOR'];
	$ip2 = $_SERVER['REMOTE_ADDR'];
	$waktu = date("Y-m-d H:i:s");
	$extra = "pd_login.php";
	if( $_SESSION["sws_id"] == "" || !isset($_SESSION["sws_id"]) || $_SESSION["sws_id"] == 0 ) {
		header("Location: http://$host$uri/$extra");
		exit;
	}
	if( $_SESSION["sws_tingkat"] == 99 || $_SESSION["sws_tingkat"] == 1 || $_SESSION["sws_tingkat"] == 2 ) {
	} else {
		header("Location: http://$host$uri/$extra");
		exit;
	}
	
	function get_waktu($a) {
		$dum = explode(" ",$a);
		$dum1 = explode("-", $dum[0]);
		$ostr = $dum1[2];
		$dum[1] = "";
		if( $dum1[1] == "01" ) { $ostr .= " Januari ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "02" ) { $ostr .= " Februari ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "03" ) { $ostr .= " Maret ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "04" ) { $ostr .= " April ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "05" ) { $ostr .= " Mei ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "06" ) { $ostr .= " Juni ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "07" ) { $ostr .= " Juli ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "08" ) { $ostr .= " Agustus ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "09" ) { $ostr .= " September ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "10" ) { $ostr .= " Oktober ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "11" ) { $ostr .= " November ".$dum1[0]." ".$dum[1]; }
		if( $dum1[1] == "12" ) { $ostr .= " Desember ".$dum1[0]." ".$dum[1]; }
		
		return $ostr;
	}
	
	function uniqueFilename($strExt) {
		$arrIp = explode('.', $_SERVER['REMOTE_ADDR']);
		list($usec, $sec) = explode(' ', microtime());
		$usec = (integer) ($usec * 65536);
		$sec = ((integer) $sec) & 0xFFFF;
		$strUid = sprintf("%08x-%04x-%04x", ($arrIp[0] << 24) | ($arrIp[1] << 16) | ($arrIp[2] << 8) | $arrIp[3], $sec, $usec);
		// tack on the extension and return the filename
		return $strUid . $strExt;
	}
	
	
	
	$anid = $_POST["id"];
	$token = $_POST["token"];
	$sproses = $_POST["sproses"];
	$nosk = addslashes($_POST["nosk"]);
	$tglsk = $_POST["tglsk"];
	$tglskel = $_POST["tglskel"];
	$athn = substr($tglsk,0,4);
	$noreg = addslashes($_POST["noreg"]);
	$lanjut = true;
	
	if( empty($_FILES['ska']) ) {
		$adafb1 = 0;
		$fba1 = "";
		$fbdb1 = "";
	} else {
		$adafb1 = 1;
		$fba1 = $_FILES['ska']['name'];
		$dum = explode(".",$fba1);
		if( count($dum) > 1 ) { $fbdb1 = uniqueFilename(".".$dum[count($dum)-1]); } else { $fbdb1 = uniqueFilename(""); }
		$uploaddir = "upload/".$fbdb1;
		if(move_uploaded_file($_FILES['ska']['tmp_name'], $uploaddir)) {
		} else {
			$adafb1 = 0;
			$fba1 = "";
			$fbdb1 = "";
		}
	}
	
	if( empty($_FILES['btamb']) ) {
		$adafb2 = 0;
		$fba2 = "";
		$fbdb2 = "";
	} else {
		$adafb2 = 1;
		$fba2 = $_FILES['btamb']['name'];
		$dum = explode(".",$fba2);
		if( count($dum) > 1 ) { $fbdb2 = uniqueFilename(".".$dum[count($dum)-1]); } else { $fbdb2 = uniqueFilename(""); }
		$uploaddir = "upload/".$fbdb2;
		if(move_uploaded_file($_FILES['btamb']['tmp_name'], $uploaddir)) {
		} else {
			$adafb2 = 0;
			$fba2 = "";
			$fbdb2 = "";
		}
	}
	$rev = addslashes($_POST["elm1"]);
	
	$hsl = mysqli_query($conn, "select * from tbl_reg_ranperda where id='$anid'");
	if( mysqli_num_rows($hsl) == 0 ) { $lanjut = false; }
	else {
		$B = mysqli_fetch_array($hsl);
		$email = $B["kirimke"];
		$noreg = $B["cnoreg"];
		$okirim = $B["okirim_nama"];
		$judul = $B["judul"];
		$tgl_aju = get_waktu($B["wkirim"]);
		$prov = $B["prov"];
		$nprov= $B["nprov"];
	}
	
	$rev = addslashes($_POST["elm1"]);
	$waktu = date("Y-m-d H:i:s");
	$revnama = addslashes($_SESSION["sws_nama_pengguna"]);
	$revid = $_SESSION["sws_id"];
	
	
	$tgl_now = get_waktu($waktu);
	
	if( $lanjut ) {
		//bikin dulu noregister
		$hsl = mysqli_query($conn, "select max(nreg) from tbl_reg_ranperda where prov='$prov' and tahun='$athn' and sproses=3");
		if( mysqli_num_rows($hsl) == 0 ) {
			$noreg = 1;
		} else {
			$B = mysqli_fetch_array($hsl);
			if( $B[0] == "" || !isset($B[0]) ) { $noreg = 1; } else { $noreg = $B[0] + 1; }
		}
		$noregkomplit = $noreg."/".$nprov."/".$athn;
		
		$hsl = mysqli_query($conn, "update tbl_reg_ranperda SET sproses='$sproses', cnoreg='$nprov', ada_skreg='$adafb1', no_skreg='$nosk', tgl_skreg='$tglsk', skreg_asli='$fba1', skreg_db='$fbdb1', approved='1', tgl_keluar = '$tglskel', noreg='$noregkomplit', nreg='$noreg', tahun='$athn', ada_btamb='$adafb2', btamb_asli='$fba2', btamb_db='$fbdb2', pengantar='$rev' where id='$anid'");
	}
?>
<form action="pd_ereview_fin.php" method="post" name="form1">
<input name="id" type="hidden" value="<?php echo $anid; ?>" />
</form>
<script>document.form1.submit();</script>
