<?php include_once("zz_koneksi_db.php"); ?>
<?php include ("zz_generate_menu.php"); ?>
<?php
	require('calendar/tc_calendar.php');
	if( isset($_SESSION["sws_daerah"]) == "" || !isset($_SESSION["sws_daerah"]) ) {
		echo "<script>window.location.href = \"ereg_login.php\";</script>";
	}
?>
<!DOCTYPE HTML>
<html>

<head>
  <title>.:: ePERDA - Kementerian Dalam Negeri ::.</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>
  
  <script>
  	function checkit() {
		var errorMsg = "";
	
		//Check for a username
		if (document.form1.judul.value==""){ errorMsg += "\n\t - Isi judul dahulu";}
		if (document.form1.berkas.value==""){ errorMsg += "\n\t - Pilih berkas Ranperda yang akan diunggah";}
		if (document.form1.berkas1.value==""){ errorMsg += "\n\t - Pilih berkas Keputusan DPRD yang akan diunggah";}
		if (document.form1.berkas2.value==""){ errorMsg += "\n\t - Pilih berkas Surat Permohonan yang akan diunggah";}
		if( document.form1.jenis.value == 1 ) {
			if (document.form1.berkas3.value==""){ errorMsg += "\n\t - Pilih berkas Keputusan Menteri yang akan diunggah";}
		}
		//if (document.form1.ringkasan.value==""){ errorMsg += "\n\t - Isi keterangan tambahan dahulu";}
		if (document.form1.answer.value!=document.form1.user_answer.value){ errorMsg += "\n\t - Tidak dapat melakukan verifikasi";}
		if (errorMsg != ""){
			msg  = "___________________________________________________________________\n\n";
			msg += "Update data belum bisa dilakukan dikarenakan kesalahan berikut ini.\n";
			msg += "___________________________________________________________________\n\n";
			errorMsg += alert(msg + errorMsg + "\n\n");
			return false;
		}
		
		return true;
	}
  </script>
</head>

<body>
  <div id="main">
    <header>
	  <?php generate_logo(); ?>
      <?php generate_menu(7); ?>
    </header>
    <div id="site_content">
      <div class="gallery"><?php generate_gallery($conn, 950, 150); ?></div>
      <div class="content">
        <h1>e-Register</h1>
        <p>Silakan mengisikan informasi di bawah ini dan mengunggah draft Rancangan Peraturan Daerah yang dimaksud. Draft Rancangan Peraturan Daerah harus dalam format MS  Word dan besaran berkas tidak melebihi 5 MB.<br>
          <br>
          Untuk RANPERDA Non Evaluasi, Surat Keputusan Menteri tidak dibutuhkan. Harap dikosongkan saja.<br>
          <br>
        Jika proses pengunggahan berhasil, maka sebuah email yang berisi informasi pengajuan Rancangan Peraturan Daerah akan dilayangkan. Untuk itu pastikan alamat e-mail yang terdaftar masih aktif dan dapat diakses. </p>
        <?phpphp
          $number_1 = rand(1, 9);
          $number_2 = rand(1, 9);
          $answer = $number_1+$number_2;
        ?>
        <form id="contact" name="form1" action="eregister_confirm.php" method="post" enctype="multipart/form-data" onsubmit="return checkit();">
          <div class="form_settings">
			<p><span>Judul RANPERDA</span>
              <input class="contact" type="text" name="judul" value="" /></p>
            <p><span>Jenis RANPERDA</span>
              <select name="jenis" id="jenis" onChange="toggle_kepmen();">
                <option value="1">Evaluasi</option>
                <option value="2">Non Evaluasi</option>
            </select></p><br>
            <p><span>Klasifikasi</span>
              <select name="klas" id="klas">
              	<option value="999">LAIN-LAIN</option>
<?php
	$hsl = mysqli_query($conn, "select * from tbl_klasifikasi where id <> 999 order by nama ");
	while( $B = mysqli_fetch_array($hsl) ) {
?>				
				<option value="<?php echo $B[0]; ?>"><?php echo $B[1]; ?></option>	
<?php
	}
?>
            </select></p><br>
            <p><em style="color: #FF0000">pilihlah jenis perda yang anda akan ajukan, apabila tidak terdaftar silahkan pilih menu lain2<br>
            </em><br></p>
            <p><span>Berkas RANPERDA</span>
              <input class="contact" type="file" name="berkas" value="" accept=".pdf" /></p><hr>
            <p style="line-height: 1em;">&nbsp;</p>
            <p><span>No. Surat Permohonan</span>
              <input class="contact" type="text" name="no_sp" value="" /></p>
            <p><span>Tanggal Surat</span>
<?php
		$dnow = date("Y-m-d");
		$tl = explode("-",$dnow);
		$jam = date("H");
		$menit = date("i");
	  $myCalendar = new tc_calendar("tgl_sp", true, false);
	  $myCalendar->setPicture("calendar/images/iconCalendar.gif");
	  $myCalendar->setDate($tl[2],$tl[1],$tl[0]);
	  $myCalendar->setPath("calendar/");
	  $myCalendar->setYearSelect(1930, date('Y'));
	  $myCalendar->dateAllow('1930-01-01', date('Y-m-d',mktime(0, 0, 0, 31, 12, date("Y"))));
	  // VERY IMPORTANT: next line REQUIRED only for localized version!
	  $myCalendar->setDateFormat(str_replace("%","",str_replace("B","F",str_replace("d","j",L_CAL_FORMAT))));
	  $myCalendar->setAlignment('left', 'bottom');
	  //$myCalendar->autoSubmit(true, "form1");
	  $myCalendar->writeScript(101);        
?>            
            <br></p><br>
            <p><span>Berkas Surat Permohonan</span>
              <input class="contact" type="file" name="berkas2" value="" accept=".pdf" /></p><hr>
            <p style="line-height: 1em;">&nbsp;</p>            
            <p><span>No. Keputusan DPRD</span>
            <input class="contact" type="text" name="no_kepdprd" value="" /></p>
            <p><span>Tanggal</span>
<?php
		$dnow = date("Y-m-d");
		$tl = explode("-",$dnow);
		$jam = date("H");
		$menit = date("i");
	  $myCalendar = new tc_calendar("tgl_kepdprd", true, false);
	  $myCalendar->setPicture("calendar/images/iconCalendar.gif");
	  $myCalendar->setDate($tl[2],$tl[1],$tl[0]);
	  $myCalendar->setPath("calendar/");
	  $myCalendar->setYearSelect(1930, date('Y'));
	  $myCalendar->dateAllow('1930-01-01', date('Y-m-d',mktime(0, 0, 0, 31, 12, date("Y"))));
	  // VERY IMPORTANT: next line REQUIRED only for localized version!
	  $myCalendar->setDateFormat(str_replace("%","",str_replace("B","F",str_replace("d","j",L_CAL_FORMAT))));
	  $myCalendar->setAlignment('left', 'bottom');
	  //$myCalendar->autoSubmit(true, "form1");
	  $myCalendar->writeScript(102);        
?>              
              
            <br></p><br>
            <p><span>Berkas Keputusan DPRD</span>
              <input class="contact" type="file" name="berkas1" value="" accept=".pdf" /></p><hr>
            <p style="line-height: 1em;">&nbsp;</p>


<div id="sws_kepmen" style="display:none">
            <p><span>No. Keputusan Menteri</span>
              <input class="contact" type="text" name="no_kepmen" value="" /></p>
            <p><span>Tanggal Surat</span>
<?php
		$dnow = date("Y-m-d");
		$tl = explode("-",$dnow);
		$jam = date("H");
		$menit = date("i");
	  $myCalendar = new tc_calendar("tgl_kepmen", true, false);
	  $myCalendar->setPicture("calendar/images/iconCalendar.gif");
	  $myCalendar->setDate($tl[2],$tl[1],$tl[0]);
	  $myCalendar->setPath("calendar/");
	  $myCalendar->setYearSelect(1930, date('Y'));
	  $myCalendar->dateAllow('1930-01-01', date('Y-m-d',mktime(0, 0, 0, 31, 12, date("Y"))));
	  // VERY IMPORTANT: next line REQUIRED only for localized version!
	  $myCalendar->setDateFormat(str_replace("%","",str_replace("B","F",str_replace("d","j",L_CAL_FORMAT))));
	  $myCalendar->setAlignment('left', 'bottom');
	  //$myCalendar->autoSubmit(true, "form1");
	  $myCalendar->writeScript(100);        
?>            
            <br></p><br>
            <p><span>Surat Keputusan Menteri</span>
              <input class="contact" type="file" name="berkas3" value="" accept=".pdf" /></p><hr>        
            <p style="line-height: 1em;">&nbsp;</p>
</div>            
            <p><span>Keterangan Tambahan</span>
              <textarea class="contact textarea" rows="5" cols="50" name="ringkasan" placeholder="Isikan ringkasan Ranperda"></textarea></p>
            <p><em style="color: #FF0000">Catatan: sistem hanya menerima dokumen dalam format PDF saja<br><br></em></p>
            <p style="line-height: 1.7em;">Untuk mencegah email spam, harap isikan jawaban dari pertanyaan di bawah ini:</p>
            <p><span><?phpphp echo $number_1; ?> + <?phpphp echo $number_2; ?> = ?</span><input type="text" name="user_answer" /><input type="hidden" name="answer" value="<?phpphp echo $answer; ?>" /><input type="hidden" name="sc" value="reg_ranperda" /></p>
            <p style="padding-top: 15px"><span>&nbsp;</span><input class="submit" type="submit" name="contact_submitted" value="Kirim" /></p>
          </div>
        </form><br><br><br><br>
      </div>
    </div>
    <?php generate_footer(); ?>
  </div>
  <p>&nbsp;</p>
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript" src="js/image_fade.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
    });
	
	function toggle_kepmen() {
		var a = document.getElementById("jenis").value;
		var b = document.getElementById("sws_kepmen");
		if( a == "2" ) {
			b.style.display = "none";
		} else {
			b.style.display = "";
		}
	}
	
	toggle_kepmen();
  </script>
</body>
</html>
