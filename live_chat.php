<?php include_once("zz_koneksi_db.php"); ?>
<?php include ("zz_generate_menu.php"); ?>
<?php require('includes/init.php'); ?>
<?php
	if( !isset($_SESSION["sws_islogin"]) || isset( $_SESSION["sws_islogin"] )!= 1 ) {
		echo "<script>window.location.href = \"live_chat_login.php\";</script>";
	}
?>
<!DOCTYPE HTML>
<html>

<head>
  <title>.:: ePERDA - Kementerian Dalam Negeri ::.</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <link rel="stylesheet" type="text/css" href="css/style.css" />
  <!-- modernizr enables HTML5 elements and feature detects -->
  <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>
</head>

<body>
  <div id="main">
    <header>
	  <?php generate_logo(); ?>
      <?php generate_menu(7); ?>
    </header>
    <div id="site_content">
      <div class="gallery"><?php generate_gallery($conn, 950, 150); ?></div>
      <div class="content_webgis">
        <h1>e-Konsultasi</h1>
        <p>Selamat datang pada Layanan Konsultasi On-Line.<br>
          Untuk peningkatan mutu layanan Kami, semua percakapan dalam Layanan ini akan direkam dalam sistem Kami.
<br>
          <br>
        </p>
        <iframe src="chat/chat/chat_sws.php" width="900" height="430" frameborder="0"></iframe><br><br><br><br>
      </div>
    </div>
    <?php generate_footer(); ?>
  </div>
  <p>&nbsp;</p>
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="js/jquery.js"></script>
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript" src="js/image_fade.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('ul.sf-menu').sooperfish();
    });
  </script>
</body>
</html>
